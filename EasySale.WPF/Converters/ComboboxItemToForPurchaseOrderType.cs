﻿using EasySale.WPF.Comman;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace EasySale.WPF.Converters
{
    public class ComboboxItemToForPurchaseOrderType : IValueConverter
    {
        internal NewPurchaseOrderPage page;
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //if (page == null)
            //    page = (NewPurchaseOrderPage)Activator.CreateInstance(typeof(NewPurchaseOrderPage));

            //if (value == null) return null;

            //PurchaseOrderType poType = (PurchaseOrderType)value;
            //return page.cbxPoType.Items.Cast<ComboBoxItem>().First(x => x.Content == (object)poType);
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var item = (ComboBoxItem)value;
            var type = Enum.Parse(typeof(PurchaseOrderType), (string)item.Tag);
            return type;
        }
    }
}
