﻿using EasySale.WPF.Models;
using EasySale.WPF.Views.Pages;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace EasySale.WPF.Converters
{
    public class StyleConverterForMinQty : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ProductModel data = (ProductModel)value;

            if (data == null)
                return null;

            if (data.Stock.Qty == 0)
                return 0;
            if (data.MinQty.HasValue && data.MinQty >= data.Stock.Qty)
                return "low-stock";
            if (data.MaxQty.HasValue && data.MaxQty < data.Stock.Qty)
                return "serplus";

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
