﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ResponseModel
    {
        public List<CategoryModel> Categories { get; set; }
        public List<ManufacturerModel> Manufacturers { get; set; }
    }
}
