﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class CartModel
    {
        private double discount;
        private double subTotal;
        private double total;

        public ObjectId Id { get; set; }
        public string InvoiceNo { get; set; } 
        public List<CartItemModel> CartItems { get; set; }
        public double CartDiscount { get; set; }

        /// <summary>
        ///  Total cart discount
        /// </summary>
        public double Discount {
            get => CartItems.Sum(x => x.DiscountAmount) + CartDiscount;
            set => discount = value;
        }
        [BsonIgnore]
        public int TotalItems { get => CartItems.Sum(x => x.Qty); }
        [BsonIgnore]
        public double SubTotal {
            get => CartItems.Sum(x => x.DiscountedLineTotal);
            set => subTotal = value;
        }

        [BsonIgnore]
        public double SubTotalWitoutDiscount
        {
            get => CartItems.Sum(x => x.LineTotal);
            set => subTotal = value;
        }

        public string CustomerName { get; set; }
        [BsonIgnore]
        public UserModel User { get; set; }
        public double Total {
            get => SubTotal - CartDiscount;
            set => total = value;
        }
        public ObjectId? CustomerId { get; set; }
        public ObjectId UserId { get; set; }
        public int PaymentMode { get; set; }
        public string CardNo { get; set; }
        public string Note { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Date { get; set; }
        public int Status { get; internal set; }
        public string RefNote { get; set; }
        public double Paying { get; set; }
    }
}
