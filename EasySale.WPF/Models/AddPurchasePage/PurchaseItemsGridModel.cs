﻿using EasySale.WPF.Comman;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models.AddPurchasePage
{
    public class PurchaseItemsGridModel : PurchaseItems
    {
        public PurchaseItemsGridModel()
        {
            base.SelectedMultiplier = (int)QtyMultiplier.Cost;
            base.SelectedDiscountType = (int)DiscountType.Percentage;
        }

        public override int Qty
        {
            get => base.Qty;
            set
            {
                base.Qty = value;
                OnPropertyChanged(nameof(Qty));
                OnPropertyChanged(nameof(Subtotal));
                OnPropertyChanged(nameof(Total));

            }
        }

        public override double Price
        {
            get => base.Price;
            set
            {
                base.Price = value;
                OnPropertyChanged(nameof(Price));
                OnPropertyChanged(nameof(Subtotal));
                OnPropertyChanged(nameof(Total));
            }
        }

        public override double? Cost
        {
            get
            {
                return base.Cost;
            }
            set
            {
                base.Cost = value;
                OnPropertyChanged(nameof(Cost));
                OnPropertyChanged(nameof(Subtotal));
                OnPropertyChanged(nameof(Total));
            }
        }

        public ProductModel Product { get; set; }

        public override int? SelectedDiscountType
        {
            get => base.SelectedDiscountType;
            set
            {
                base.SelectedDiscountType = value;
                OnPropertyChanged(nameof(Total));
                OnPropertyChanged(nameof(DiscountString));
            }
        }

        public override int? SelectedMultiplier
        {
            get => base.SelectedMultiplier;
            set
            {
                base.SelectedMultiplier = value;
                OnPropertyChanged(nameof(Subtotal));
                OnPropertyChanged(nameof(Total));
                OnPropertyChanged(nameof(DiscountString));
            }
        }

        public override double Discount
        {
            get => base.Discount;
            set
            {
                base.Discount = value;

                if (GetParsedQtyMultiplier() == QtyMultiplier.Price)
                    Cost = Price - GetDiscountAmount(Price);

                OnPropertyChanged(nameof(Total));
                OnPropertyChanged(nameof(DiscountString));
            }
        }

        public string DiscountString
            => $"{GetDiscountPercentage()}% ({GetDiscountAmount()})";


        public double Subtotal
        {
            get
            {
                if (GetParsedQtyMultiplier() == QtyMultiplier.Cost)
                    return (Cost ?? 0) * Convert.ToDouble(Qty);
                return Price * Convert.ToDouble(Qty);
            }
            set
            {
                double unitValue = value / Convert.ToDouble(Qty);
                if (GetParsedQtyMultiplier() == QtyMultiplier.Cost)
                {
                    Cost = unitValue;
                }
                else
                {
                    Price = unitValue;
                    Cost = unitValue - GetDiscountAmount(unitValue);
                }

                OnPropertyChanged(nameof(DiscountString));
                OnPropertyChanged(nameof(Total));

            }
        }

        public double Total
            => Subtotal - GetDiscountAmount();



        /******** PRIVATE HELPERS ********/

        private DiscountType GetParsedDiscountType()
            => (DiscountType)SelectedDiscountType;

        private QtyMultiplier GetParsedQtyMultiplier()
            => (QtyMultiplier)SelectedMultiplier;

        public double GetDiscountAmount()
            => CartDiscountModel.GetDiscountAmount(GetParsedDiscountType(), Discount, Subtotal);

        private double GetDiscountAmount(double amount)
            => CartDiscountModel.GetDiscountAmount(GetParsedDiscountType(), Discount, amount);

        public double GetDiscountPercentage()
            => CartDiscountModel.GetDiscountPercentage(GetParsedDiscountType(), Discount, Subtotal);
    }
}
