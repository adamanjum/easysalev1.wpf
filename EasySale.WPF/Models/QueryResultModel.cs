﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class QueryResultModel
    {
        public QueryResultModel()
        {
            Records = new Dictionary<string, object>();
        }

        public Dictionary<string, object> Records { get; set; }
        public long Total { get; set; }
    }
}
