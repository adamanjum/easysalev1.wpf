﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class SubCartItemModel
    {
        public string Name { get; set; }
        public int Qty { get; set; }
    }
}
