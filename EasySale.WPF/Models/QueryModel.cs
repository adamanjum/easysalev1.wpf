﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class QueryModel: ICloneable
    {
        public QueryModel()
        {
            PageNo = 1;
            NoOfRecord = 20;
        }

        private string searchText;

        public int PageNo { get; set; }
        public int NoOfRecord { get; set; }
        public bool ApplyPagination { get; set; } = true;
        public string SearchText
        {
            set => searchText = value;
            get => searchText?
                .Replace("(", "\\(")
                .Replace(")", "\\)")
                .Replace("*", "\\*")
                .Replace("+", "\\+");
        }
        public DateTime? Date { get; set; }
        public DateTime? DateTo { get; set; }
        public int? Status { get; set; }
        public ObjectId? UserId { get; set; }
        public ObjectId? CategoryId { get; set; }
        public ObjectId? ManufacturerId { get; set; }

        public Dictionary<string, object> Filters { get; set; } = new Dictionary<string, object>();

        public int? Skip
        {
            get
            {
                if (!ApplyPagination) return null;
                return NoOfRecord * (PageNo - 1);
            }
        }

        public int? Limit
        {
            get
            {
                if (!ApplyPagination) return null;
                return NoOfRecord;
            }

        }

        /// @fuctions
        public void SetFilter(string key, object value)
        {
            bool stringCheck = value is string && string.IsNullOrWhiteSpace((string)value);

            if (value == null || stringCheck)
            {
                if (Filters.ContainsKey(key))
                    Filters.Remove(key);
            }
            else if (Filters.ContainsKey(key))
                Filters[key] = ApplyFilterConfiguration(key, value);
            else
                Filters.Add(key, ApplyFilterConfiguration(key, value));
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        private object ApplyFilterConfiguration(string key, object value)
        {
            object v = value;
            if (key == "Search")
            {
                string search = (string)v;
                search = "^" + Regex.Escape(search);

                if (search.StartsWith("^>"))
                    search = search.Replace("^>", "");

                v = $"/{search.Replace("\\ ", ".+")}/i";
            }

            return v;
        }
    }
}
