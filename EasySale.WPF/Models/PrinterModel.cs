﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class PrinterModel
    {
        public ObjectId Id { get; set; }
        public string DefaultInvoicePrinter { get; set; }
        public string DefaultLablePrinter { get; set; }
        public bool AutoInvoicePrint { get; set; }
        public bool AutoLablePrint { get; set; }
        public ObjectId Addedby { get; internal set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; internal set; }
    }
}
