﻿using FileHelpers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ProductReportModel
    {
        public ObjectId Id { get; set; }
        public string Code { get; set; }
        public string ProductName { get; set; }
        public int QtySold { get; set; }
        public decimal TotalPrice { get; set; }
        public int RemainingStock { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
    }
}
