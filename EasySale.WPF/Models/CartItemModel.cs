﻿using EasySale.WPF.Comman;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class CartItemModel
    {
        private double lineTotal;
        public CartItemModel()
        {
            Qty = 1;
        }

        public ObjectId ProductId { get; set; }
        public string Name { get; set; }
        public int Qty { get; set; }
        public double Price { get; set; }
        public string Note { get; set; }
        public ObjectId CategoryId { get; set; }
        public ObjectId? ManufacturerId { get; set; }
        public string CategoryName { get; set; }
        public string ManufacturerName { get; set; }

        [BsonIgnore]    
        public int Type { get; set; }

        [BsonIgnore]
        public ProductModel Product { get; set; }

        [BsonIgnore]
        public Stock Stock { get; set; }

        public CartDiscountModel Discount { get; set; }
        public List<SubCartItemModel> SubCartItems { get; set; }

        public double LineTotal
        {
            get => Convert.ToDouble(Qty * Price);
            set => lineTotal = value;
        }

        public double DiscountedLineTotal
        {
            get => LineTotal - DiscountAmount;
        }

        public double DiscountAmount
        {
            get
            {
                if (Discount == null)
                    return 0;

                return CartDiscountModel.GetDiscountAmount(Discount.DiscountType, Discount.DiscountValue, LineTotal);
            }
        }

        public double DiscountPercentage
        {
            get
            {
                if (Discount == null)
                    return 0;

                return CartDiscountModel.GetDiscountPercentage(Discount.DiscountType, Discount.DiscountValue, LineTotal);
            }
        }
    }
}
