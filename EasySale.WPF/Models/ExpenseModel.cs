﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ExpenseModel
    {
        public ObjectId Id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Date { get; set; } = DateTime.Today;
        public string Reference { get; set; }
        public double Amount { get; set; }
        public List<string> Attachments { get; set; }
        public string Note { get; set; }
        public ObjectId AddedBy { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; set; }

        [BsonIgnore]
        public UserModel User { get; set; }
    }
}
