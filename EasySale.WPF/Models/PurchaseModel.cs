﻿using EasySale.WPF.ViewModels;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class PurchaseModel
    {
        public ObjectId Id { get; set; }

        public DateTime Date { get; set; } = DateTime.Today;
        public string Ref { get; set; }
        public ObjectId SupplierId { get; set; }
        public List<string> Attachments { get; set; }
        public List<PurchaseItems> PurchaseItems { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }

        public DateTime CreatedAt { get; set; }

        [BsonIgnore]
        public SupplierModel Supplier { get; set; }

        [BsonIgnore]
        public int TotalQty { get => PurchaseItems.Sum(x => x.Qty); }
        public ObjectId Addedby { get; internal set; }
    }

    public class PurchaseItems : ViewModelBase
    {
        public ObjectId ProductId { get; set; }
        public string Name { get; set; }
        public ObjectId? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ObjectId? ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public virtual double? Cost { get; set; }
        public virtual double Price { get; set; }
        public virtual int Qty { get; set; }
        public virtual double Discount { get; set; }
        public virtual int? SelectedMultiplier { get; set; }
        public virtual int? SelectedDiscountType { get; set; }
    }
}
