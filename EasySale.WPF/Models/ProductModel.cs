﻿using EasySale.WPF.ViewModels;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EasySale.WPF.Models
{
    public class ProductModel : ViewModelBase
    {
        private int? minQty;
        private Stock stock;


        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string Code { get; set; }
        public ObjectId CategoryId { get; set; }
        public virtual double Price { get; set; }
        public virtual double? Cost { get; set; }
        public virtual int? MinQty
        {
            get => minQty;
            set
            {
                minQty = value;
                OnPropertyChanged(nameof(MinQty));
                OnPropertyChanged(nameof(Stock));
            }
        }
        public virtual int? MaxQty { get; set; }
        public string ImgPath { get; set; }
        public virtual Stock Stock
        {
            get => stock;
            set
            {
                stock = value;
                OnPropertyChanged(nameof(Stock));
            }
        }
        public string Description { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreatedAt { get; set; }
        public string SKU { get; set; }

        public List<ComboProductModel> Combo { get; set; }
        public StandardProductModel Standard { get; set; }
        public ObjectId Addedby { get; set; }
        [BsonIgnore]
        public CategoryModel Category { get; set; }
        public ObjectId? ManufacturerId { get; internal set; }
        [BsonIgnore]
        public ManufacturerModel Manufacturer { get; set; }
    }

    public class ComboProductModel
    {
        public string Name { get; set; }
        public int Qty { get; set; }
    }

    public class StandardProductModel
    {
        public int Qty { get; set; }
        public decimal Price { get; set; }
    }

    public class Stock
    {
        public int Qty { get; set; }
        public DateTime UpdatedAt { get; set; }
        public ObjectId UpdatedBy { get; set; }
        public ObjectId Addedby { get; set; }
    }
}
