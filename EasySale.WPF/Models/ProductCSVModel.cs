﻿using FileHelpers;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    [IgnoreFirst]
    [DelimitedRecord(",")]
    public class ProductCSVModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
        [FieldConverter(ConverterKind.Double)]
        public double? Cost { get; set; }
        [FieldConverter(ConverterKind.Double)]
        public double Price { get; set; }
        public int? MinQty { get; set; }
        public string Description { get; set; }
        public int? StockQty { get; set; }
    }
}
