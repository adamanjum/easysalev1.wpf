﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class StockAdjustmentModel
    {
        public ObjectId Id { get; set; }
        public string ProductName { get; set; }

        public string CategoryName { get; set; }
        public ObjectId CategoryId { get; set; }

        public string ManufacturerName { get; set; }
        public ObjectId ManufacturerId { get; set; }

        public string Reason { get; set; }
        public double NewStockValue { get; set; }
        public double CurrentStockValue { get; set; }

        public string StockChangedBy { get; set; }
        public ObjectId StockChangedById { get; set; }

        public DateTime ChangedAt { get; set; }

        [BsonIgnore]
        public double StockDifference { get => NewStockValue - CurrentStockValue; }
    }
}
