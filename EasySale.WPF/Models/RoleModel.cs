﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class RoleModel
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public List<Resourse> Resourses { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime UpdatedAt { get; set; }

        [BsonIgnore]
        public static List<string> ResourseName
        {
            get => new List<string>
            {
                "Dashboard",
                "POS",
                "Sale",
                "Purchase",
                "Products",
                "Category",
                "Person",
                "Reports",
                "Settings"
            };
        }

        [BsonIgnore]
        public static List<string> GrantsName
        {
            get => new List<string>
            {
                "Can View",
                "Can Modify",
                "Can Delete",
            };
        }
    }

    public class Resourse
    {
        public string Name { get; set; }
        public List<string> Grants { get; set; }
    }
}
