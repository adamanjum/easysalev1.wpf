﻿using EasySale.WPF.Comman;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ReasonModel
    {
        public ReasonModel(ReasonType type)
        {
            Type = type;
        }

        public ObjectId Id { get; set; }

        private string title;
        public string Title { get => title?.Trim(); set => title = value; }

        public ReasonType Type { get; set; }
        public string Description { get; set; }
    }
}
