﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ManufacturerModel
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string logoPath { get; set; }
        public string Code { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public ObjectId Addedby { get; set; }
    }
}
