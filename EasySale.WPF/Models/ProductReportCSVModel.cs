﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    [IgnoreFirst]
    [DelimitedRecord(",")]
    public class ProductReportCSVModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
       
        public int QuantitySold { get; set; }
        public int RemainingStock { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
