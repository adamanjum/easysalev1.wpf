﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.ViewModels;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class PurchaseOrderModel : ViewModelBase, IDataErrorInfo
    {
        public ObjectId Id { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Today;
        public string OrderNo { get; set; } = Utils.GetUID("ES-PO-");

        [Required(ErrorMessage = "Description cannot be null")]
        public string Description { get; set; }
        public PurchaseOrderType? PoType { get; set; }
        public ObservableCollection<PurchaseOrderItemModel> Items { get; set; }
        public PurchaseOrderStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }

        [BsonIgnore]
        public int TotalQtyOrdered { get => Items.Sum(x => x.QtyToBeOrdered); }



        [Bindable(false)]
        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string propertyName]
        {
            get
            {
                if (propertyName == nameof(Description))
                {
                    if (string.IsNullOrEmpty(Description))
                    {
                        return "Description is required";
                    }
                }
                return null;
            }
        }
    }
}
