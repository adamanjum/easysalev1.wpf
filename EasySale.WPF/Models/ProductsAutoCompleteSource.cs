﻿using MaterialDesignExtensions.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class ProductsAutoCompleteSource : IAutocompleteSource
    {
        private List<ProductModel> products;
        public ProductsAutoCompleteSource(List<ProductModel> products)
        {
            this.products = products;
        }

        public IEnumerable Search(string searchTerm)
        {
            return this.products;
        }
    }
}
