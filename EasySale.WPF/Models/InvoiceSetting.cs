﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class InvoiceSetting
    {
        public ObjectId Id { get; set; }
        public bool ShowLogo { get; set; }
        public bool ShowAddress { get; set; }
        public bool ShowCity { get; set; }
        public bool ShowUserId { get; set; }
        public bool ShowUserName { get; set; }
        public bool ShowCustomerName { get; set; }
        public bool ShowCustomerContact { get; set; }
        public bool ShowCustomerAddress { get; set; }
        public bool ShowItemNote { get; set; }
        public bool ShowCustomerBillSummary { get; set; }
        public bool ShowOrderNote { get; set; }
        public bool ShowTermCondition { get; set; }
        public string TermConditionText { get; set; }
        public bool ShowThankYou { get; set; }
        public string ThankYouText { get; set; }
        public ObjectId StoreId { get; set; }
        public ObjectId Addedby { get; internal set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; internal set; }
    }
}
