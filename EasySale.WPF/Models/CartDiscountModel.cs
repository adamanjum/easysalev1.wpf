﻿using EasySale.WPF.Comman;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class CartDiscountModel
    {
        public string Reason { get; set; }
        public double DiscountValue { get; set; }
        public DiscountType DiscountType { get; set; }

        public static double GetDiscountAmount(DiscountType discountType, double discountValue, double total)
            => discountType == DiscountType.Flate ? discountValue : discountValue / 100 * total;

        public static double GetDiscountPercentage(DiscountType discountType, double discountValue, double total)
            => discountType == DiscountType.Percentage ? discountValue : Math.Round(discountValue / total * 100, 1);
    }
}
