﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class RegisterDetailModel
    {
        public RegisterDetailModel(RegisterModel register)
        {
            Register = register;
        }

        public double CashInHand { get => Register.CashInHand; }
        public double TotalCashSale { get; set; }
        public double TotalCardSale { get; set; }
        public double TotalExprenses { get; set; }
        public RegisterModel Register { get; set; }

        public double Subtotal { get => CashInHand + TotalCardSale + TotalCashSale; }
        public double TotalCash { get => Subtotal - TotalExprenses; }
    }
}
