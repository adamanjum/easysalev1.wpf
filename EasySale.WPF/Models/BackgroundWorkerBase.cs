﻿using EasySale.WPF.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public abstract class BackgroundWorkerBase
    {
        internal readonly BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };

        /// <summary>
        /// This event is invoked when background job is completed
        /// </summary>
        public event Action<QueryResultModel> onInitialized;

        /// <summary>
        /// This event is invoked just before starting background job
        /// </summary>
        public event Action onInitializing;

        public bool IsBusy { get => bw.IsBusy; }


        public BackgroundWorkerBase()
        {
            bw.DoWork += BackgroundworkerDoWork;
            bw.RunWorkerCompleted += BackgroundworkerRunWorkerCompleted;
        }


        public void Initialize(QueryModel query)
        {
            bw.RunWorkerAsync(query);
            onInitializing?.Invoke();
        }

        private void BackgroundworkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }

        internal abstract void BackgroundworkerDoWork(object sender, DoWorkEventArgs e);
    }
}
