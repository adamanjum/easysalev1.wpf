﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class RegisterModel
    {
        public ObjectId Id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime OpenAt { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? CloseAt { get; set; }
        public double CashInHand { get; set; }
        public int? CreditCardSlips { get; set; }
        public double? TotalCash { get; set; }
        public ObjectId UserId { get; set; }
        public string Note { get; set; }
    }
}
