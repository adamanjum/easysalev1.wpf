﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models
{
    public class PurchaseOrderItemModel
    {
        public ObjectId ProductId { get; set; }
        public string ProductName { get; set; }
        public ObjectId CategoryId { get; set; }
        public ObjectId? ManufacrurerId { get; set; }
        public int QtyToBeOrdered { get; set; }

        [BsonIgnore]
        public ProductModel Product { get; set; }
        [BsonIgnore]
        public CategoryModel Category { get; set; }
        [BsonIgnore]
        public ManufacturerModel Manufacturer { get; set; }
        [BsonIgnore]
        public bool AdditionalRecordFetched { get; set; }
    }
}
