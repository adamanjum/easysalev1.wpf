﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Models.PurchaseOrder
{
    public class PurchaseOrderItemGridModel : ProductModel
    {
        private int? maxQty;
        private int qtyToBeOrdered;

        public override int? MaxQty
        {
            get => maxQty;
            set
            {
                maxQty = value;
                OnPropertyChanged(nameof(MaxQty));

                if (MaxQty.HasValue)
                    SetOrderQty();
            }
        }

        public int QtyToBeOrdered
        {
            get => qtyToBeOrdered;
            set
            {
                qtyToBeOrdered = value;
                OnPropertyChanged(nameof(QtyToBeOrdered));
            }
        }

        public void SetOrderQty()
        {
            if (MaxQty.HasValue)
            {
                QtyToBeOrdered = MaxQty.Value - (Stock?.Qty ?? 0);
                if (QtyToBeOrdered < 0)
                    QtyToBeOrdered = 0;
            }
        }
    }
}
