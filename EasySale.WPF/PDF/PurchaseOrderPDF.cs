﻿using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using EasySale.WPF.Models;
using System.Data;
using Syncfusion.Pdf.Barcode;
using EasySale.WPF.Helper;

namespace EasySale.WPF.PDF
{
    internal class PurchaseOrderPDF: PDFBase
    {
        public PurchaseOrderPDF(PurchaseOrderModel po)
        {
            DrawHeader();
            DrawPoDetails(po);
            DrawTable(ConstructGridData(po.Items.ToList()));

        }

        private void DrawPoDetails(PurchaseOrderModel po)
        {
            float pageWidth = GetClient().Width;
            var graphics = GetPage().Graphics;

            // distance from start of the label and value
            float gutter = 60;

            Y = Margin;

            string poTitle = "Purchase Order";
            SizeF poTitleSize = SubtitleFont.MeasureString(poTitle);

            graphics.DrawString(
                poTitle,
                SubtitleFont,
                PdfBrushes.Black,
                new RectangleF(X, Y, pageWidth, poTitleSize.Height),
                new PdfStringFormat(PdfTextAlignment.Center));

            Y += poTitleSize.Height + LineSpace;
            graphics.DrawRectangle(PdfBrushes.Black, new RectangleF(X, Y, pageWidth, 1));

            float poTitleEndAt = Y;

            //////////////// SECOND COLUMN ////////////////

            // second column starts at 25% of the page from right side
            float secondColumn = (float)(pageWidth - (pageWidth * 0.25));
            X = secondColumn;
            Y = poTitleEndAt + Margin;

            PdfCode39Barcode barcode = new PdfCode39Barcode();
            barcode.Text = po.OrderNo;
            barcode.Size = new SizeF(pageWidth - secondColumn, 40);
            barcode.Draw(CurrentPage, new PointF(X, Y));


            //////////////// FIRST COLUMN /////////////////////
            string label;
            SizeF size;

            Y = poTitleEndAt + Margin;
            X = 0;

            label = "Order To:";
            size = NormalTextFontBold.MeasureString(label);
            graphics.DrawString(label, NormalTextFontBold, PdfBrushes.Black, new PointF(X, Y));

            X += size.Width + (gutter - size.Width);
            graphics.DrawString("Abraham Swearegin", NormalTextFont, PdfBrushes.Black, new PointF(X, Y));

            X = 0;
            Y += NormalTextFont.Height + LineSpace;

            label = "Address:";
            size = NormalTextFontBold.MeasureString(label);
            graphics.DrawString(label, NormalTextFontBold, PdfBrushes.Black, new PointF(X, Y));


            X += size.Width + (gutter - size.Width);

            graphics.DrawString("1/A Habib park Lahore", NormalTextFont, PdfBrushes.Black, new PointF(X, Y));

            X = 0;
            Y += NormalTextFont.Height + LineSpace;

            label = "Date:";
            size = NormalTextFontBold.MeasureString(label);
            graphics.DrawString(label, NormalTextFontBold, PdfBrushes.Black, new PointF(X, Y));

            X += size.Width + (gutter - size.Width);

            graphics.DrawString(po.OrderDate.ToLocalTime().ToString("dd MMMM, yyyy"), NormalTextFont, PdfBrushes.Black, new PointF(X, Y));

            X = 0;
            Y += NormalTextFont.Height + Margin;
        }

        private DataTable ConstructGridData(List<PurchaseOrderItemModel> items)
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("Sr. #"));
            table.Columns.Add(new DataColumn("Product Name"));
            table.Columns.Add(new DataColumn("Category"));
            table.Columns.Add(new DataColumn("Manufacturer"));
            table.Columns.Add(new DataColumn("Exp. Price"));
            table.Columns.Add(new DataColumn("Qty"));
            table.Columns.Add(new DataColumn("Total"));
            

            int sr = 1;
            items.ForEach((item) =>
            {
                string cost = item.Product?.Cost?.ToString("C0", CultureHelper.CustomCulture());
                if (string.IsNullOrEmpty(cost)) cost = "N/A";

                //string total = string.Format("Rs {0}", item.Product.Price * (item.Product.Cost ?? 0));

                table.Rows.Add(sr, item.Product.Name, item.Category.Name, item.Manufacturer.Name, cost, item.QtyToBeOrdered);
                sr++;
            });

            return table;
        }

        private void DrawSummary()
        {

        }
    }
}
