﻿using EasySale.WPF.Comman;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Pdf.Lists;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using FontStyle = System.Drawing.FontStyle;

namespace EasySale.WPF.PDF
{
    internal abstract class PDFBase
    {
        public PDFBase()
        {
            Document = new PdfDocument();
            Document.PageSettings.Margins.All = 15;
            CurrentPage = Document.Pages.Add();
        }


        public PdfDocument Document { get; set; }

        public virtual PdfColor LightBlue { get; set; } = Color.FromArgb(255, 91, 126, 215);
        public virtual PdfBrush LightBlueBrush { get => new PdfSolidBrush(LightBlue); }

        public virtual PdfColor DarkBlue { get; set; } = Color.FromArgb(255, 65, 104, 209);
        public virtual PdfBrush DarkBlueBrush { get => new PdfSolidBrush(DarkBlue); }

        public virtual PdfBrush WhiteBrush { get => new PdfSolidBrush(Color.White); }



        public PdfTrueTypeFont TitleFont
        {
            get => new PdfTrueTypeFont(new Font("Roboto Light", 18, FontStyle.Bold), true);
        }

        public PdfTrueTypeFont SubtitleFont
        {
            get => new PdfTrueTypeFont(new Font("Roboto Light", 12, FontStyle.Bold), true);
        }

        public PdfTrueTypeFont NormalTextFont
        {
            get => new PdfTrueTypeFont(new Font("Arial", 8, FontStyle.Regular), true);
        }

        public PdfTrueTypeFont NormalTextFontBold
        {
            get => new PdfTrueTypeFont(new Font("Arial", 8, FontStyle.Bold), true);
        }

        public float X { get; set; } = 0;
        public float Y { get; set; } = 0;
        public virtual float Margin { get; set; } = 10;
        public virtual float LineSpace { get; set; } = 5;

        internal PdfPage CurrentPage;



        public SizeF GetClient(int? pageNo = null)
        {
            int page = pageNo ?? Document.Pages.Count - 1;
            return Document.Pages[page].GetClientSize();
        }

        public PdfPage GetPage(int? pageNo = null)
        {
            int page = pageNo ?? Document.Pages.Count - 1;
            return Document.Pages[page];
        }

        public virtual PdfPageTemplateElement DrawHeader()
        {
            RectangleF bounds = new RectangleF(0, 0, GetClient(0).Width, 50);
            PdfPageTemplateElement header = new PdfPageTemplateElement(bounds);

            float pageWidth = GetClient().Width;
            float pageHeight = GetClient().Height;

            float x = 0;
            float y = 0;

            PdfGraphics graphics = header.Graphics;

            string title = Session.Store.Name;
            SizeF titleSize = TitleFont.MeasureString(title);

            graphics.DrawString(
                title,
                TitleFont,
                PdfBrushes.Black,
                new RectangleF(x, y, pageWidth, titleSize.Height),
                new PdfStringFormat(PdfTextAlignment.Center));

            y += titleSize.Height + LineSpace;

            string description = $"{Session.Store.Address} ({Session.Store.Phone})";
            SizeF descriptionSize = NormalTextFont.MeasureString(description);

            graphics.DrawString(
                description,
                NormalTextFont,
                PdfBrushes.Black,
                new RectangleF(x, y, pageWidth, descriptionSize .Height),
                new PdfStringFormat(PdfTextAlignment.Center));

            Document.Template.Top = header;
            return header;
        }

        public virtual PdfPageTemplateElement DrawFooter()
        {
            RectangleF bounds = new RectangleF(0, 0, GetClient(0).Width, 50);
            PdfPageTemplateElement header = new PdfPageTemplateElement(bounds);

            PdfGraphics graphics = header.Graphics;

            Document.Template.Bottom = header;
            return header;
        }

        public virtual void DrawTable(DataTable table)
        {
            PdfGrid grid = new PdfGrid();

            grid.RepeatHeader = true;
            grid.DataSource = table;

            grid.Columns[0].Width = 30;
            grid.Columns[0].Format.Alignment = PdfTextAlignment.Center;

            grid.Columns[1].Width = 200;
            grid.Columns[2].Width = 90;
            grid.Columns[3].Width = 100;
            grid.Columns[3].Width = 60;
            grid.Columns[3].Width = 60;

            for (int i = 0; i < grid.Headers.Count; i++)
            {
                grid.Headers[i].Height = 18;
                for (int j = 0; j < grid.Columns.Count; j++)
                {
                    PdfStringFormat pdfStringFormat = new PdfStringFormat();
                    pdfStringFormat.LineAlignment = PdfVerticalAlignment.Middle;
                    grid.Headers[i].Cells[j].StringFormat = pdfStringFormat;

                    grid.Headers[i].Cells[j].Style.Font = NormalTextFontBold;
                    grid.Headers[i].Cells[j].Style.Borders.Left = new PdfPen(Color.Transparent);
                    grid.Headers[i].Cells[j].Style.Borders.Right = new PdfPen(Color.Transparent);

                }
            }
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                grid.Rows[i].Height = 18;
                for (int j = 0; j < grid.Columns.Count; j++)
                {   
                    PdfStringFormat pdfStringFormat = new PdfStringFormat();
                    pdfStringFormat.LineAlignment = PdfVerticalAlignment.Middle;

                    grid.Rows[i].Cells[j].StringFormat = pdfStringFormat;
                    grid.Rows[i].Cells[j].Style.Font = NormalTextFont;
                    grid.Rows[i].Cells[j].Style.Borders.Left = new PdfPen(Color.Transparent);
                    grid.Rows[i].Cells[j].Style.Borders.Right = new PdfPen(Color.Transparent);
                }

            }

            var page = GetPage();
            PdfGridLayoutResult result = grid.Draw(page, new PointF(0, Y));

            Y = result.Bounds.Bottom + LineSpace;
            CurrentPage = GetPage();
        }

        public void Save()
        {
            Document.Save("Zugferd.pdf");
            Document.Close(true);


            //Message box confirmation to view the created PDF document.
            if (MessageBox.Show("Do you want to view the PDF file?", "PDF File Created",
                MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                //Launching the PDF file using the default Application.[Acrobat Reader]
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo("Zugferd.pdf") { UseShellExecute = true };
                process.Start();
            }
        }
    }
}
