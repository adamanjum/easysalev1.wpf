﻿using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Comman
{
    public static class Session
    {
        public static UserModel User { get; set; }
        public static List<CartModel> Carts { get; set; }
        public static RegisterModel Register { get; set; }
        public static StoreModel Store { get; set; }
        public static PrinterModel PrinterSettings { get; set; }
        public static InvoiceSetting InvoiceSetting { get; set; }
        public static RoleModel Role { get; set; }
        public static string ThemeName { get; set; } = "MaterialDark";
    }
}
