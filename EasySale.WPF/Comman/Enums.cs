﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace EasySale.WPF.Comman
{
    public enum ProductType
    {
        Standard,
        Combo,
        Service
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum PurchaseStatus
    {
        Received,
        NotReceived
    }

    public enum PaymentMode
    {
        Cash,
        Card
    }

    public enum SaleStatus
    {
        Completed,
        Hold,
    }

    public enum DiscountType
    {
        Flate,
        Percentage
    }

    public enum ReasonType
    {
        Discount,
        Stock
    }

    public enum PurchaseOrderType
    {
        [Display(Name = "Low Stock")]
        LowStock
    }

    public enum PurchaseOrderStatus
    {
        Draft,
        Dispached
    }

    public enum StockLevels
    {
        Zero = 1,
        LowStock,
        BetweenMinMaxThresholsd,
        Surplus
    }

    public enum QtyMultiplier
    {
        Cost,
        Price
    }
}
