﻿using EasySale.WPF.Views;
using Syncfusion.SfSkinManager;
using Syncfusion.Themes.MaterialDark.WPF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace EasySale.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            new Login().Show();
            base.OnStartup(e);
        }
    }
}
