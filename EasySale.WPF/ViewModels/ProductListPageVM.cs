﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.ProductList;
using EasySale.WPF.Models;
using EasySale.WPF.Views;
using EasySale.WPF.Views.Popups;
using MongoDB.Driver;
using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EasySale.WPF.ViewModels
{
    public class ProductListVM : ViewModelBase
    {
        private readonly QueryModel queryModel = new QueryModel { NoOfRecord = 200 };
        private readonly ProductListFilter productListFilter = new ProductListFilter();
        private readonly ProductListData productListData = new ProductListData();
        private readonly DebounceDispatcher debounceDispatcher = new DebounceDispatcher();

        // filter collections
        private List<CategoryModel> categories;
        private List<ManufacturerModel> manufacturers;


        // grid collection
        private ObservableCollection<ProductModel> products;


        public ProductListVM()
        {
            // filters background job
            productListFilter.onInitialized += OnFilterInitialized;
            productListFilter.Initialize(null);

            // products background job
            productListData.onInitialized += OnDataInitialized;
        }

        public ProductListData DataProvider { get => productListData; }

        /**
         * @properties
         **/
        public List<CategoryModel> Categories
        {
            get { return categories; }
            set
            {
                categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public List<ManufacturerModel> Manufacturers
        {
            get { return manufacturers; }
            set
            {
                manufacturers = value;
                OnPropertyChanged(nameof(Manufacturers));
            }
        }

        public ObservableCollection<ProductModel> Products
        {
            get { return products; }
            set
            {
                products = value;
                OnPropertyChanged(nameof(Products));
            }
        }


        /**
         * @filter properties
         **/

        public string Search
        {
            set
            {
                if (!productListData.IsBusy)
                {
                    debounceDispatcher.Debounce(300, (param) =>
                    {
                        queryModel.SetFilter("Search", value);
                        queryModel.PageNo = 1;

                        productListData.Initialize(queryModel);
                    });
                }
            }
        }

        public CategoryModel SelectedCategory
        {
            set
            {
                if (!productListData.IsBusy)
                {
                    debounceDispatcher.Debounce(300, (param) =>
                    {
                        queryModel.SetFilter("CategoryId", value?.Id);
                        queryModel.PageNo = 1;

                        productListData.Initialize(queryModel);
                    });
                }
            }
        }

        public ManufacturerModel SelectedManufacturer
        {
            set
            {
                if (!productListData.IsBusy)
                {
                    debounceDispatcher.Debounce(300, (param) =>
                    {
                        queryModel.SetFilter("ManufacturerId", value?.Id);
                        queryModel.PageNo = 1;

                        productListData.Initialize(queryModel);
                    });
                }
            }
        }

        public bool IsLowStock
        {
            set
            {
                if (!productListData.IsBusy)
                {
                    debounceDispatcher.Debounce(100, (param) =>
                    {
                        string key = "IsLowStock";

                        if (value)
                            queryModel.SetFilter(key, true);
                        else
                            queryModel.SetFilter(key, null);

                        queryModel.PageNo = 1;

                        productListData.Initialize(queryModel);
                    });
                }
            }
        }


        #region pagination properties

        public int PageNo
        {
            get => queryModel.PageNo;
            set
            {
                if (!productListData.IsBusy)
                {
                    queryModel.PageNo = value;
                    productListData.Initialize(queryModel);
                }
            }
        }

        public int PageSize
        {
            get => queryModel.NoOfRecord;
            set
            {
                if (!productListData.IsBusy)
                {
                    queryModel.NoOfRecord = value;
                    queryModel.PageNo = 1;

                    productListData.Initialize(queryModel);
                }
            }
        }

        #endregion


        #region commands

        public ICommand DeleteCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                bool isDelete = ConformationalPopup.Show("Product", "Do you want to delete..?", Application.Current.Windows.OfType<MainWindow>().FirstOrDefault());
                if (!isDelete) return;

                ProductModel product = arg as ProductModel;
                await ProductDAL.DeleteOneAsync(product.Id);

                productListData.Initialize(queryModel);
            }, (o) => true);
        }

        public ICommand StockAdjustmentCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                ProductModel product = (ProductModel)arg;
                int stock = product.Stock?.Qty ?? 0;

                StockAdjustmentModel stockAdjustment = new StockAdjustmentModel
                {
                    ProductName = product.Name,
                    CurrentStockValue = stock,
                    NewStockValue = stock
                };

                var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                var popup = new StockAdjustmentPopup(stockAdjustment) { Owner = window };


                if (!(Boolean)popup.ShowDialog())
                    return;

                if (popup.result.StockDifference == 0) return;

                var updatedStock = new Stock
                {
                    Qty = Convert.ToInt32(popup.result.NewStockValue),
                    Addedby = Session.User.Id,
                    UpdatedAt = DateTime.Now
                };

                // update stock to relevant product
                var update = Builders<ProductModel>.Update.Set(x => x.Stock, updatedStock);
                await ProductDAL.UpdateByIdAsync(product.Id, update);

                // getting latest product listing

                Products.FirstOrDefault(x => x.Id.Equals(product.Id)).Stock = updatedStock;

                // save new entery in stock history

                stockAdjustment.CategoryName = product.Category.Name;
                stockAdjustment.CategoryId = product.Category.Id;

                stockAdjustment.ManufacturerName = product.Manufacturer.Name;
                stockAdjustment.ManufacturerId = product.Manufacturer.Id;

                stockAdjustment.StockChangedBy = $"{Session.User.FirstName} {Session.User.LastName}";
                stockAdjustment.StockChangedById = Session.User.Id;

                stockAdjustment.ChangedAt = DateTime.Now;

                await StockAdjustmentReportDAL.InsertAsync(stockAdjustment);

            }, (o) => true);
        }

        public ICommand ExportCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                var query = (QueryModel)queryModel.Clone();
                query.ApplyPagination = false;

                List<ProductModel> DBproducts = await ProductDAL.GetAllAsync(query);

                List<ProductCSVModel> productCSVs = CSVHelper.MapProductCSV(DBproducts, Categories, Manufacturers);

                string[] columnsToExclude = new string[] { "Id" };
                CSVHelper.ExportCSV(
                    $"EXPORT_PRODUCTS_{DateTime.Now.ToString("ddMMyyyy")}",
                    productCSVs,
                    columnsToExclude,
                    (headers) => headers
                );
            }, (o) => true);
        }

        public ICommand BulkUpdateCommand
        {
            get => new RelayCommand((arg) =>
            {
                var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                var poppup = new BulkUpdateProductPopup(ExportForUpdate, OnBulkUpdate, Categories, Manufacturers);
                poppup.Owner = window;
                poppup.ShowDialog();

            }, (o) => true);
        }

        public ICommand CellEditCommand
        {
            get => new RelayCommand((arg) =>
            {
                var e = (CurrentCellEndEditEventArgs)arg;
                var grid = (SfDataGrid)e.OriginalSender;

                string key = grid.CurrentColumn.MappingName;
                var value = grid.View.GetPropertyAccessProvider().GetValue(grid.CurrentItem, key);
                var id = ((ProductModel)grid.CurrentItem).Id;

                var update = Builders<ProductModel>.Update.Set(key, value);
                ProductDAL.UpdateByIdAsync(id, update);

            }, (o) => true);
        }

        #endregion


        #region functions

        /// <summary>
        /// Called when filters are loaded from database
        /// </summary>
        private void OnFilterInitialized(QueryResultModel result)
        {
            Categories = result.Records["categories"] as List<CategoryModel>;
            Manufacturers = result.Records["manufacturers"] as List<ManufacturerModel>;

            productListData.Initialize(queryModel);
        }

        private void OnDataInitialized(QueryResultModel result)
        {
            List<ProductModel> products = result.Records["ProductRecords"] as List<ProductModel>;

            products.ForEach(product =>
            {
                product.Category = categories?.Find(x =>
                    x.Id.Equals(product.CategoryId));

                product.Manufacturer = manufacturers?.Find(x =>
                    x.Id.Equals(product.ManufacturerId));
            });

            Products = new ObservableCollection<ProductModel>(products);
        }

        private void OnBulkUpdate()
        {
            if (!productListData.IsBusy)
            {
                productListData.Initialize(queryModel);
            }
        }

        private async void ExportForUpdate()
        {
            try
            {
                var query = (QueryModel)queryModel.Clone();
                query.ApplyPagination = false;

                List<ProductModel> DBproducts = await ProductDAL.GetAllAsync(query);
                List<ProductCSVModel> productCSVs = CSVHelper.MapProductCSV(DBproducts, categories, manufacturers);

                CSVHelper.ExportCSV(
                    $"EXPORT_PRODUCTS_{DateTime.Now.ToString("ddMMyyyy")}",
                    productCSVs,
                    null,
                    (headers) => headers
                );
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Import Products", ex.Message);
            }
        }

        #endregion
    }
}
