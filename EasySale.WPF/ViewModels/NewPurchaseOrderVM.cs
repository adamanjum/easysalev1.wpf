﻿using EasySale.WPF.DAL;
using EasySale.WPF.Helper.NewPurchaseOrder;
using EasySale.WPF.Helper.ProductList;
using EasySale.WPF.Models;
using EasySale.WPF.Models.PurchaseOrder;
using EasySale.WPF.Views;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EasySale.WPF.ViewModels
{
    public class NewPurchaseOrderVM : ViewModelBase
    {
        private readonly List<string> readonlyColumns = new List<string> { "QtyToBeOrdered" };
        private readonly QueryModel queryModel = new QueryModel() { NoOfRecord = 500 };
        private readonly NewPurchaseOrderFilters productListFilter = new NewPurchaseOrderFilters();
        private readonly NewPurchaseOrderData newPurchaseOrderData = new NewPurchaseOrderData();

        // filter collections
        private List<CategoryModel> categories;
        private List<ManufacturerModel> manufacturers;
        private int filterCount;

        private ObservableCollection<PurchaseOrderItemGridModel> gridItems;

        public NewPurchaseOrderVM()
        {
            // filters background job
            productListFilter.onInitialized += OnFilterInitialized;
            productListFilter.Initialize(null);


            newPurchaseOrderData.onInitialized += OnDataInitialized;

            PurchaseOrder = new PurchaseOrderModel();
        }

        /**
         * @properties
         **/
        public List<CategoryModel> Categories
        {
            get { return categories; }
            set
            {
                categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public List<ManufacturerModel> Manufacturers
        {
            get { return manufacturers; }
            set
            {
                manufacturers = value;
                OnPropertyChanged(nameof(Manufacturers));
            }
        }

        public ObservableCollection<PurchaseOrderItemGridModel> GridItems
        {
			get { return gridItems; }
			set
			{
                gridItems = value;
				OnPropertyChanged(nameof(GridItems));
			}
		}

        public int FilterCount
        {
            get { return filterCount; }
            set
            {
                filterCount = value;
                OnPropertyChanged(nameof(FilterCount));
            }
        }

        public PurchaseOrderModel PurchaseOrder { get; set; }


        public ICommand ImportFilterCommand
        {
            get => new RelayCommand((arg) =>
            {
                var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                var modal = new FilterPopup(Categories, Manufacturers) { Owner = window };
                if ((Boolean)modal.ShowDialog())
                {
                    List<ObjectId> selectedCategories = new List<ObjectId>();
                    List<ObjectId> selectedManufacturers = new List<ObjectId>();

                    modal.SelectedCategories.ForEach((category) => selectedCategories.Add(category.Id));
                    modal.SelectedManufacturers.ForEach((manufacturer) => selectedManufacturers.Add(manufacturer.Id));

                    FilterCount = 0;

                    if (selectedCategories.Count > 0)
                    {
                        queryModel.SetFilter("CategoryIds", selectedCategories);
                        FilterCount++;
                    }
                    else
                    {
                        queryModel.SetFilter("CategoryIds", null);
                    }

                    if (selectedManufacturers.Count > 0)
                    {
                        queryModel.SetFilter("ManufacturerIds", selectedManufacturers);
                        FilterCount++;
                    }
                    else
                    {
                        queryModel.SetFilter("ManufacturerIds", null);
                    }

                    if (modal.StockLevels.HasValue)
                    {
                        queryModel.SetFilter("StockLevels", modal.StockLevels);
                        FilterCount++;
                    }
                    else
                    {
                        queryModel.SetFilter("StockLevel", null);
                    }


                    newPurchaseOrderData.Initialize(queryModel);
                }
            }, (o) => 
            {
                return true;
                //return PurchaseOrder.PoType.HasValue;
            });
        }

        public ICommand CellEditCommand
        {
            get => new RelayCommand((arg) =>
            {
                var e = (CurrentCellEndEditEventArgs)arg;
                var grid = (SfDataGrid)e.OriginalSender;
                grid.Print();

                string key = grid.CurrentColumn.MappingName;
                var value = grid.View.GetPropertyAccessProvider().GetValue(grid.CurrentItem, key);
                var id = ((PurchaseOrderItemGridModel)grid.CurrentItem).Id;

                var update = Builders<ProductModel>.Update.Set(key, value);
                ProductDAL.UpdateByIdAsync(id, update);

            }, (arg) =>
            {
                var e = (CurrentCellEndEditEventArgs)arg;
                var grid = (SfDataGrid)e.OriginalSender;

                string key = grid.CurrentColumn.MappingName;
                return !readonlyColumns.Any(x => x == key);
            });
        }

        public ICommand DeleteCommand
        {
            get => new RelayCommand((arg) =>
            {
                GridItems.Remove((PurchaseOrderItemGridModel)arg);
            }, (arg) => true);
        }
        
        public ICommand SaveCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                PurchaseOrder.Items = new ObservableCollection<PurchaseOrderItemModel>();
                PurchaseOrder.Status = Comman.PurchaseOrderStatus.Dispached;
                PurchaseOrder.CreatedAt = DateTime.Now;

                gridItems.ForEach((item) =>
                    PurchaseOrder.Items.Add(new PurchaseOrderItemModel
                    {
                        ProductId = item.Id,
                        ProductName = item.Name,
                        CategoryId = item.Category.Id,
                        ManufacrurerId = item?.Manufacturer?.Id,
                        QtyToBeOrdered = item.QtyToBeOrdered

                    }));

                await PurchaseOrderDAL.InsertAsync(PurchaseOrder);

                PurchaseOrder = new PurchaseOrderModel();
                GridItems.Clear();
                FilterCount = 0;

                var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                InformationalPopup.Show("Purchase Order", "Purchase Order has been shaved successfully", owner: window);
            }, (arg) => !(GridItems == null || GridItems.Count == 0));
        }



        private void OnFilterInitialized(QueryResultModel result)
        {
            Categories = result.Records["categories"] as List<CategoryModel>;
            Manufacturers = result.Records["manufacturers"] as List<ManufacturerModel>;
        }

        private void OnDataInitialized(QueryResultModel result)
        {
            List<PurchaseOrderItemGridModel> list = new List<PurchaseOrderItemGridModel>();
            ((List<ProductModel>)result.Records["ProductRecords"])
                .ForEach((product) =>
                {
                    product.Category = categories?.Find(x =>
                        x.Id.Equals(product.CategoryId));

                    product.Manufacturer = manufacturers?.Find(x =>
                        x.Id.Equals(product.ManufacturerId));

                    var poi = new PurchaseOrderItemGridModel
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Code = product.Code,
                        Description = product.Description,
                        Category = product.Category,
                        Manufacturer = product.Manufacturer,
                        MinQty = product.MinQty,
                        MaxQty = product.MaxQty,
                        Stock = product.Stock,
                        Cost = product.Cost,
                        Price = product.Price
                    };

                    poi.SetOrderQty();

                    list.Add(poi);
                });

            GridItems = new ObservableCollection<PurchaseOrderItemGridModel>(list);
            OnPropertyChanged(nameof(SaveCommand));
        }

    }
}
