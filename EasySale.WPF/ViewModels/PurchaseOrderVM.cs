﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.PurchaseOrder;
using EasySale.WPF.Models;
using EasySale.WPF.PDF;
using EasySale.WPF.Views;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using Syncfusion.Data.Extensions;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Helpers;
using Syncfusion.XPS;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Menu;

namespace EasySale.WPF.ViewModels
{
    public class PurchaseOrderVM : ViewModelBase
    {
        private readonly QueryModel queryModel = new QueryModel { NoOfRecord = 200 };
        private readonly PurchaseOrderFilter PurchaseOrderFilter = new PurchaseOrderFilter();
        private readonly PurchaseOrderData PurchaseOrderData = new PurchaseOrderData();
        private readonly DebounceDispatcher debounceDispatcher = new DebounceDispatcher();

        // filter collections
        private List<CategoryModel> categories;
        private List<ManufacturerModel> manufacturers;


        // grid collection
        private ObservableCollection<PurchaseOrderModel> purchaseOrders;


        public PurchaseOrderVM()
        {
            // filters background job
            PurchaseOrderFilter.onInitialized += OnFilterInitialized;
            PurchaseOrderFilter.Initialize(null);

            // products background job
            PurchaseOrderData.onInitialized += OnDataInitialized;
        }

        public PurchaseOrderData DataProvider { get => PurchaseOrderData; }

        /**
         * @properties
         **/
        public List<CategoryModel> Categories
        {
            get { return categories; }
            set
            {
                categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public List<ManufacturerModel> Manufacturers
        {
            get { return manufacturers; }
            set
            {
                manufacturers = value;
                OnPropertyChanged(nameof(Manufacturers));
            }
        }

        public ObservableCollection<PurchaseOrderModel> PurchaseOrders
        {
            get { return purchaseOrders; }
            set
            {
                purchaseOrders = value;
                OnPropertyChanged(nameof(purchaseOrders));
            }
        }


        /**
         * @filter properties
         **/

        public string PoNumber
        {
            set
            {
                if (!PurchaseOrderData.IsBusy)
                {
                    debounceDispatcher.Debounce(300, (param) =>
                    {
                        queryModel.SetFilter("Search", value);
                        queryModel.PageNo = 1;

                        PurchaseOrderData.Initialize(queryModel);
                    });
                }
            }
        }


        #region pagination properties

        public int PageNo
        {
            get => queryModel.PageNo;
            set
            {
                if (!PurchaseOrderData.IsBusy)
                {
                    queryModel.PageNo = value;
                    PurchaseOrderData.Initialize(queryModel);
                }
            }
        }

        public int PageSize
        {
            get => queryModel.NoOfRecord;
            set
            {
                if (!PurchaseOrderData.IsBusy)
                {
                    queryModel.NoOfRecord = value;
                    queryModel.PageNo = 1;

                    PurchaseOrderData.Initialize(queryModel);
                }
            }
        }

        #endregion


        #region commands

        public ICommand DeleteCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                bool isDelete = ConformationalPopup.Show("PurchaseOrder", "Do you want to delete..?", Application.Current.Windows.OfType<MainWindow>().FirstOrDefault());
                if (!isDelete) return;

                PurchaseOrderModel po = arg as PurchaseOrderModel;
                await PurchaseOrderDAL.DeleteByIdAsync(po.Id);

                PurchaseOrderData.Initialize(queryModel);
            }, (o) => true);
        }

        public ICommand PrintCommand
        {
            get => new RelayCommand(async(arg) =>
            {
                var po = (PurchaseOrderModel)arg;
                await FeedProducts(po);

                var pdf = new PurchaseOrderPDF(po);
                pdf.Save();
            }, (o) => true);
        }

        public ICommand BulkUpdateCommand
        {
            get => new RelayCommand((arg) =>
            {
                var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

                var poppup = new BulkUpdateProductPopup(ExportForUpdate, OnBulkUpdate, Categories, Manufacturers);
                poppup.Owner = window;
                poppup.ShowDialog();

            }, (o) => true);
        }

        public ICommand DetailsViewExpandingCommand
        {
            get => new RelayCommand((arg) =>
            {
                PurchaseOrderModel po = (PurchaseOrderModel)((GridDetailsViewExpandingEventArgs)arg).Record;
                var _ = FeedProducts(po);

            }, (o) => true);
        }

        #endregion


        #region functions

        /// <summary>
        /// Called when filters are loaded from database
        /// </summary>
        private void OnFilterInitialized(QueryResultModel result)
        {
            Categories = result.Records["categories"] as List<CategoryModel>;
            Manufacturers = result.Records["manufacturers"] as List<ManufacturerModel>;

            PurchaseOrderData.Initialize(queryModel);
        }

        private void OnDataInitialized(QueryResultModel result)
        {
            PurchaseOrders = new ObservableCollection<PurchaseOrderModel>((List<PurchaseOrderModel>)result.Records["PurchaseOrdersRecords"]);
        }

        private void OnBulkUpdate()
        {
            if (!PurchaseOrderData.IsBusy)
            {
                PurchaseOrderData.Initialize(queryModel);
            }
        }

        private async void ExportForUpdate()
        {
            try
            {
                List<ProductModel> DBproducts = await ProductDAL.GetAllAsync(queryModel);
                List<ProductCSVModel> productCSVs = CSVHelper.MapProductCSV(DBproducts, categories, manufacturers);

                CSVHelper.ExportCSV(
                    $"EXPORT_PRODUCTS_{DateTime.Now.ToString("ddMMyyyy")}",
                    productCSVs,
                    null,
                    (headers) => headers
                );
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Import Products", ex.Message);
            }
        }

        private async Task FeedProducts(PurchaseOrderModel po)
        {
            List<ObjectId> productId = new List<ObjectId>();

            po.Items
                .ToList()
                .FindAll((item) => !item.AdditionalRecordFetched)
                .ForEach((item) => productId.Add(item.ProductId));

            if (productId.Count == 0) return;

            List<ProductModel> products = await ProductDAL.GetAllAsync(new QueryModel
            {
                Filters = new Dictionary<string, object>
                {
                    ["Ids"] = productId,
                },
                NoOfRecord = 1000
            });

            po.Items.ForEach((item) =>
            {
                item.Product = products.Find(x => x.Id.Equals(item.ProductId));
                item.Category = Categories.Find(x => x.Id.Equals(item.CategoryId));
                item.Manufacturer = Manufacturers.Find(x => x.Id.Equals(item.ManufacrurerId));
                item.AdditionalRecordFetched = true;
            });
        }

        #endregion
    }
}
