﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.AddPurchase;
using EasySale.WPF.Helper.ProductList;
using EasySale.WPF.Models;
using EasySale.WPF.Models.AddPurchasePage;
using EasySale.WPF.Models.PurchaseOrder;
using EasySale.WPF.Views;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

//using System.Windows.Forms;
using System.Windows.Input;

namespace EasySale.WPF.ViewModels
{
    public class NewPurchaseVM : ViewModelBase
    {
        private readonly AddPurchaseData data = new AddPurchaseData();

        private List<SupplierModel> suppliers;
        private ObservableCollection<PurchaseItemsGridModel> purchaseItems;
        private string totalQty;
        private string totalDiscount;
        private string totalAmount;
        private PurchaseModel purchaseModel;

        public NewPurchaseVM()
        {
            data.onInitialized += DataOnInitialized;
            data.Initialize(null);

            Purchase = new PurchaseModel();
            PurchaseItems = new ObservableCollection<PurchaseItemsGridModel>();
            purchaseItems.CollectionChanged += PurchaseItems_CollectionChanged;

            UpdateStats();
        }


        /********** Properties ***********/

        public PurchaseModel Purchase
        {
            get => purchaseModel;
            set
            {
                purchaseModel = value;
                OnPropertyChanged(nameof(Purchase));
            }
        }

        public List<CategoryModel> Categories { get; set; }
        public List<ManufacturerModel> Manufacturers { get; set; }

        public List<SupplierModel> Suppliers
        {
            get => suppliers;
            set
            {
                suppliers = value;
                OnPropertyChanged(nameof(Suppliers));
            }
        }

        public ObservableCollection<PurchaseItemsGridModel> PurchaseItems
        {
            get { return purchaseItems; }
            set
            {
                purchaseItems = value;
                OnPropertyChanged(nameof(PurchaseItems));
            }
        }


        public List<string> DiscountTypes => Enum.GetNames(typeof(DiscountType)).ToList();
        public List<string> QtyMultipliers => Enum.GetNames(typeof(QtyMultiplier)).ToList();

        public string TotalQty
        {
            get { return totalQty; }
            set
            {
                totalQty = value;
                OnPropertyChanged(nameof(TotalQty));
            }
        }

        public string TotalDiscount
        {
            get { return totalDiscount; }
            set
            {
                totalDiscount = value;
                OnPropertyChanged(nameof(TotalDiscount));
            }
        }

        public string TotalAmount
        {
            get { return totalAmount; }
            set
            {
                totalAmount = value;
                OnPropertyChanged(nameof(TotalAmount));
            }
        }


        /********** Commands ***********/

        public ICommand DeleteCommand
        {
            get => new RelayCommand((arg) =>
            {
                PurchaseItems.Remove((PurchaseItemsGridModel)arg);
            }, (arg) => true);
        }

        public ICommand CellEditCommand
        {
            get => new RelayCommand((arg) =>
            {
                UpdateStats();

            }, (arg) => true);
        }

        public ICommand SaveCommand
        {
            get => new RelayCommand(async (arg) =>
            {
                try
                {
                    //if (path != null)
                    //{
                    //    ImageHelper.SaveAttachments("PurchaseAttachments", path.Paths, path.FileNames);
                    //    Purchase.Attachments = path.FileNames.ToList();
                    //}

                    if (Purchase.Date == null)
                        throw new Exception("Please select date...!");

                    if (string.IsNullOrWhiteSpace(Purchase.Ref))
                        throw new Exception("Reference is required");


                    Purchase.PurchaseItems = PurchaseUtils.FormatPurchaseItems(PurchaseItems.ToList());


                    if (Purchase.PurchaseItems == null || Purchase.PurchaseItems.Count == 0)
                        throw new Exception("Add purchase items");


                    MongoCRUD db = new MongoCRUD();
                    if (Purchase.Id == ObjectId.Empty)
                    {
                        Purchase.Addedby = Session.User.Id;
                        Purchase.CreatedAt = DateTime.Now;

                        // insert new purchase record
                        await PurchaseDAL.InsertAsync(Purchase);

                        // bulk update product stock
                        await ProductDAL.BulkUpdateAsync(PurchaseUtils.FormatProductStockBulkUpdate(PurchaseItems.ToList()));
                    }
                    else
                    {
                        await db.UpsertRecordAsync("Purchase", Purchase.Id, Purchase);
                    }

                    var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                    InformationalPopup.Show("Add Purchase", "Purchases has been saved successfully", "Ok", window);

                    Purchase = new PurchaseModel();
                    PurchaseItems = new ObservableCollection<PurchaseItemsGridModel>();
                    UpdateStats();

                }
                catch (Exception ex)
                {
                    var window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                    InformationalPopup.Show("Add Purchase", ex.Message, "Ok", window);
                }
            }, (arg) => true);
        }



        /********** Private Functions ***********/


        private void PurchaseItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateStats();
        }

        private void DataOnInitialized(QueryResultModel data)
        {
            Suppliers = data.Records["Suppliers"] as List<SupplierModel>;
            Manufacturers = data.Records["Manufacturers"] as List<ManufacturerModel>;
            Categories = data.Records["Categories"] as List<CategoryModel>;
        }

        private void UpdateStats()
        {
            TotalQty = GetQty();
            TotalDiscount = GetTotalDiscount();
            TotalAmount = GetTotalAmount();
        }


        private string GetQty()
        {
            int totalItems = purchaseItems.Count;
            int totalQty = purchaseItems.Sum(x => x.Qty);

            return $"{totalItems} ({totalQty})";
        }

        private string GetTotalDiscount()
        {
            int totalItems = purchaseItems.Count;

            double avgDiscount = purchaseItems.Sum(x => x.GetDiscountPercentage()) / totalItems;
            if (double.IsNaN(avgDiscount))
                avgDiscount = 0;

            double discountAmount = purchaseItems.Sum(x => x.GetDiscountAmount());

            return $"{Math.Round(avgDiscount, 1)}% | {discountAmount.ToString("C", CultureHelper.CustomCulture())}";
        }

        private string GetTotalAmount()
        {
            return purchaseItems.Sum(x => x.Total).ToString("C", CultureHelper.CustomCulture());
        }


        public Task<List<ProductModel>> SearchProducts(string text)
        {
            QueryModel query = new QueryModel();
            query.NoOfRecord = 13;
            query.SetFilter("Search", text);

            return ProductDAL.GetAllAsync(query);
        }

        public void AddToPurchaseItems(ProductModel product)
        {
            PurchaseItemsGridModel pi = purchaseItems.FirstOrDefault(x => x.ProductId.Equals(product.Id));
            if (pi != null)
            {
                pi.Qty++;
            }
            else
            {
                PurchaseItemsGridModel item = new PurchaseItemsGridModel
                {
                    Product = product,
                    ProductId = product.Id,
                    Name = product.Name,
                    Qty = 1,
                    Cost = product.Cost,
                    Price = product.Price
                };

                item.Product.Category = Categories.Find(x => x.Id.Equals(item.Product.CategoryId));
                item.Product.Manufacturer = Manufacturers.Find(x => x.Id.Equals(item.Product.ManufacturerId));

                purchaseItems.Insert(0, item);
            }
        }
    }
}
