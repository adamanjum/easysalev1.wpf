﻿using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using EasySale.WPF.Helper.RegisterReport;
using System.Windows;
using EasySale.WPF.Helper;

namespace EasySale.WPF.ViewModels
{
    public class RegisterReportVM : ViewModelBase
    {
        private readonly QueryModel queryModel = new QueryModel { NoOfRecord = 200 };
        private readonly RegisterReportFilter productReportFilter = new RegisterReportFilter();
        private readonly RegisterReportData registerReportData = new RegisterReportData();

        // filter collections
        private List<UserModel> users;

        // grid collection
        private List<RegisterModel> registerReports;


        public RegisterReportVM()
        {
            // filters background job
            productReportFilter.onInitialized += OnFilterInitialized;
            productReportFilter.Initialize();

            // report background job
            registerReportData.onInitialized += OnDataInitialized;
            registerReportData.Initialize(queryModel);
        }

        public RegisterReportData DataProvider { get => registerReportData; }

        /**
         * @properties
         **/
        public List<UserModel> Users
        {
            get { return users; }
            set
            {
                users = value;
                OnPropertyChanged(nameof(Users));
            }
        }

        public List<RegisterModel> RegisterReports
        {
            get { return registerReports; }
            set
            {
                registerReports = value;
                OnPropertyChanged(nameof(RegisterReports));
            }
        }


        /**
         * @filter properties
         **/

        public UserModel SelectedUser
        {
            set
            {
                if (!registerReportData.IsBusy)
                {
                    queryModel.SetFilter("UserId", value?.Id);
                    queryModel.PageNo = 1;

                    registerReportData.Initialize(queryModel);
                }
            }
        }

        public DateTime? FromDate
        {
            set
            {
                if (!registerReportData.IsBusy)
                {
                    queryModel.SetFilter(nameof(FromDate), value);
                    queryModel.PageNo = 1;

                    registerReportData.Initialize(queryModel);
                }
            }
        }

        public DateTime? ToDate
        {
            set
            {
                if (!registerReportData.IsBusy)
                {
                    queryModel.SetFilter(nameof(ToDate), value?.AddHours(23).AddMinutes(59).AddSeconds(59));
                    queryModel.PageNo = 1;

                    registerReportData.Initialize(queryModel);
                }
            }
        }


        /**
         * @pagination properties
         **/

        public int PageNo
        {
            get => queryModel.PageNo;
            set
            {
                if (!registerReportData.IsBusy)
                {
                    queryModel.PageNo = value;
                    registerReportData.Initialize(queryModel);
                }
            }
        }

        public int PageSize
        {
            get => queryModel.NoOfRecord;
            set
            {
                if (!registerReportData.IsBusy)
                {
                    queryModel.NoOfRecord = value;
                    queryModel.PageNo = 1;

                    registerReportData.Initialize(queryModel);
                }
            }
        }


        /**
         * @commands
         **/

        public ICommand PageChangeCommand
        {
            get => new RelayCommand((x) =>
            {
                // todo: business logic
            }, (o) => true);
        }


        /**
         * @functions
         **/
        
        /// <summary>
        /// Called when filters are loaded from database
        /// </summary>
        private void OnFilterInitialized(QueryResultModel result)
        {
            Users = result.Records["users"] as List<UserModel>;
        }

        private void OnDataInitialized(QueryResultModel result)
        {
            RegisterReports = result.Records["RegisterRecords"] as List<RegisterModel>;
        }
    }
}
