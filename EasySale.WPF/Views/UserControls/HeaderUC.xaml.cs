﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Views.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MaterialDesignColors;
using MaterialDesignThemes.Wpf;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for HeaderUC.xaml
    /// </summary>
    public partial class HeaderUC : UserControl
    {
        private ITheme theme;
        private PaletteHelper paletteHelper;

        public HeaderUC()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
            timer.Tick += (object sender, EventArgs e) => txtTime.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
            timer.Start();

            try
            {
                string path = ImageHelper.GetDirectoryPath("User") + Session.User.ImgPath;
                var image = new BitmapImage(new Uri(path));
                img.Fill = new ImageBrush(image);
                imgLarg.Fill = new ImageBrush(image);
            }
            catch
            {
                InformationalPopup.Show("Profile Image", "Unable to find your profile image Please set a new from profile settings");
            }

            if (Session.Register == null)
            {
                btnRegisterDetails.IsEnabled = false;
                btnRegisterClose.IsEnabled = false;
            }

            GlobalEvent.RegisterClosed += parameter =>
            {
                btnRegisterDetails.IsEnabled = false;
                btnRegisterClose.IsEnabled = false;
            };

            GlobalEvent.RegisterOpened += parameter =>
            {
                btnRegisterDetails.IsEnabled = true;
                btnRegisterClose.IsEnabled = true;
            };
        }

        private void BtnLogout_Click(object sender, RoutedEventArgs e)
            => GlobalEvent.OnLogout(null);

        private void BtnRegisterDetails_Click(object sender, RoutedEventArgs e)
        {
            RegisterDetailsPopup registerDetails = new RegisterDetailsPopup();
            registerDetails.Show();
        }

        private void BtnRegisterClose_Click(object sender, RoutedEventArgs e)
        {
            CloseRegisterPopup registerClose = new CloseRegisterPopup();
            registerClose.Show();
        }

        private void BtnMinimize_Click(object sender, RoutedEventArgs e)
            => GlobalEvent.OnMinimizeWindow(null);

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // get current theme
            paletteHelper = new PaletteHelper();
            theme = paletteHelper.GetTheme();
            BaseTheme baseTheme = theme.GetBaseTheme();

            if (baseTheme == BaseTheme.Light)
            {
                listboxTheme.SelectedIndex = 0;
                Session.ThemeName = "MaterialLight";
            }
            else
            {
                listboxTheme.SelectedIndex = 1;
                Session.ThemeName = "MaterialDark";
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = (ListBox)sender;
            //Change the base theme to Dark
            if (list.SelectedIndex == 0)
            {
                theme.SetBaseTheme(Theme.Light);
                Session.ThemeName = "MaterialLight";
            }
            else
            {
                theme.SetBaseTheme(Theme.Dark);
                Session.ThemeName = "MaterialDark";
            }


            //Change the app's current theme
            paletteHelper.SetTheme(theme);
        }
    }
}
