﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls.Controls
{
    /// <summary>
    /// Interaction logic for Dropdown.xaml
    /// </summary>
    public partial class Dropdown : UserControl
    {
        public Dropdown()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event Action<Object> OnChange;

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Placeholder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.Register("Placeholder", typeof(string), typeof(Dropdown), new PropertyMetadata(""));


        public IEnumerable Items
        {
            get { return (IEnumerable)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Items.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(IEnumerable), typeof(Dropdown));

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
            => OnChange?.Invoke(cbxCategory.SelectedItem);

        private void Button_Click(object sender, RoutedEventArgs e)
            => cbxCategory.SelectedIndex = -1;
    }
}
