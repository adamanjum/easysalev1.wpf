﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls.Controls
{
    /// <summary>
    /// Interaction logic for Textfield.xaml
    /// </summary>
    public partial class Textfield : UserControl
    {
        public Textfield()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event Action<string> OnTextChange;

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Placeholder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.Register("Placeholder", typeof(string), typeof(Textfield), new PropertyMetadata(""));

        private void textbox_TextChanged(object sender, TextChangedEventArgs e)
            => OnTextChange?.Invoke(textbox.Text);

        private void Button_Click(object sender, RoutedEventArgs e)
            => textbox.Text = string.Empty;
    }
}
