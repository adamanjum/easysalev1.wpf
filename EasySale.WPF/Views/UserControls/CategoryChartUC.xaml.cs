﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for CategoryChartUC.xaml
    /// </summary>
    public partial class CategoryChartUC : UserControl
    {
        public CategoryChartUC()
        {
            InitializeComponent();
            SeriesCollection = new SeriesCollection
            {
                new PieSeries
                {
                    Title = "Pencil",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(500) },
                    DataLabels = true,
                    LabelPosition = PieLabelPosition.OutsideSlice
                },
                new PieSeries
                {
                    Title = "colors",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(2000) },
                    DataLabels = true
                },
                new PieSeries
                {
                    Title = "Pencil",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(700) },
                    DataLabels = true
                },
                new PieSeries
                {
                    Title = "colors",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(234) },
                    DataLabels = true
                },


            };

            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }
    }
}
