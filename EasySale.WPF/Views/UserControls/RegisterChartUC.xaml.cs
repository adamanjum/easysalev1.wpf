﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for RegisterChartUC.xaml
    /// </summary>
    public partial class RegisterChartUC : UserControl
    {
        public RegisterChartUC()
        {
            InitializeComponent();

            SeriesCollection = new SeriesCollection
            {
                new PieSeries
                {
                    Title = "Cash",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(5000) },
                },
                new PieSeries
                {
                    Title = "Credit Card",
                    Values = new ChartValues<ObservableValue> { new ObservableValue(2000) },
                }
            };

            DataContext = this;
        }
        public SeriesCollection SeriesCollection { get; set; }
    }
}
