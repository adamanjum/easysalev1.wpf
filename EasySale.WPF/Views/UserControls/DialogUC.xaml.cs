﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for DialogUC.xaml
    /// </summary>
    public partial class DialogUC : UserControl
    {
        public DialogUC()
        {
            InitializeComponent();
            DataContext = this;
        }



        public string HeaderText
        {
            get { return (string)GetValue(HeaderContentText ); }
            set { SetValue(HeaderContentText, value); }
        }

        public static readonly DependencyProperty HeaderContentText =
            DependencyProperty.Register("HeaderText", typeof(string), typeof(DialogUC));



        public object FooterContent
        {
            get { return (object)GetValue(FooterContentProperty); }
            set { SetValue(FooterContentProperty, value); }
        }

        public static readonly DependencyProperty FooterContentProperty =
            DependencyProperty.Register("FooterContent", typeof(object), typeof(DialogUC));


        public object BodyContant
        {
            get { return (object)GetValue(BodyContantProperty); }
            set { SetValue(BodyContantProperty, value); }
        }

        public static readonly DependencyProperty BodyContantProperty =
            DependencyProperty.Register("BodyContant", typeof(object), typeof(DialogUC));




        /**
         * @Methods
         */
        public void Open(string headerText, object bodyContent)
        {
            this.HeaderText = headerText;
            this.BodyContant = bodyContent;
            this.FooterContent = Resources["btnOk"];
            dialoge.IsOpen = true;
        }

        public void Open(string headerText, object bodyContent, object footerContent)
        {
            this.Open(headerText, bodyContent);
            this.FooterContent = footerContent;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dialoge.IsOpen = false;
        }
    }
}
