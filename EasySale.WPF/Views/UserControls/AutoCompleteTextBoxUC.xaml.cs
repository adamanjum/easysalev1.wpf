﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for AutoCompleteTextBoxUC.xaml
    /// </summary>
    public partial class AutoCompleteTextBoxUC : UserControl
    {
        //public static RoutedEvent OnTextChangedEvent;
        public AutoCompleteTextBoxUC()
        {
            InitializeComponent();
            DataContext = this;

           // OnTextChangedEvent = EventManager.RegisterRoutedEvent("OnTextChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(AutoCompleteTextBoxUC));
        }

        //public event RoutedEventHandler OnTextChanged
        //{
        //    add { AddHandler(OnTextChangedEvent, value); }
        //    remove { RemoveHandler(OnTextChangedEvent, value); }
        //}

        /**
         * @events
         */
        public event Action<Object> OnSelected;
        public event Action<string> OnTextChanged;

        /**
         * @dependencyProperties
         */
        public IEnumerable Items
        {
            get { return (IEnumerable)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(IEnumerable), typeof(AutoCompleteTextBoxUC));

        /**
         * @DropdownMaxHeight
         */
        public int? DropdownMaxHeight
        {
            get { return (int?)GetValue(DropdownMaxHeightProperty); }
            set { SetValue(DropdownMaxHeightProperty, value); }
        }

        public static readonly DependencyProperty DropdownMaxHeightProperty =
            DependencyProperty.Register("DropdownMaxHeight", typeof(int?), typeof(AutoCompleteTextBoxUC));



        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(AutoCompleteTextBoxUC), new PropertyMetadata(string.Empty));




        public ViewBase ItemsView
        {
            get { return (ViewBase)GetValue(ItemsViewProperty); }
            set { SetValue(ItemsViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsViewProperty =
            DependencyProperty.Register("ItemsView", typeof(ViewBase), typeof(AutoCompleteTextBoxUC));




        public ICommand TextChangeCommand
        {
            get { return (ICommand)GetValue(TextChangeCommandProperty); }
            set { SetValue(TextChangeCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextChangeCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextChangeCommandProperty =
            DependencyProperty.Register("TextChangeCommand", typeof(ICommand), typeof(AutoCompleteTextBoxUC));





        private void ListDropDown_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down) return;
            if (e.Key == Key.Back)
            {
                txtsearch.Text = txtsearch.Text.Remove(txtsearch.Text.Length - 1);
            }

            if ((e.Key >= Key.A && e.Key <= Key.Z) || (e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                txtsearch.Text += e.Key.ToString().ToLower();
            }

            if (e.Key == Key.Enter)
            {
                if (listDropDown.SelectedItem != null)
                    OnSelected?.Invoke((ProductModel)listDropDown.SelectedItem);
                bdrDropDown.Visibility = Visibility.Collapsed;
                txtsearch.Text = string.Empty;
                listDropDown.SelectedIndex = -1;
            }

            txtsearch.Focus();
            txtsearch.CaretIndex = txtsearch.Text.Length;
        }

        private void ListViewItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (listDropDown.SelectedItem != null)
                OnSelected?.Invoke(listDropDown.SelectedItem);
            bdrDropDown.Visibility = Visibility.Collapsed;
            txtsearch.Text = string.Empty;
        }

        private void HandleUpDown(Key key)
        {
            int index = listDropDown.SelectedIndex;

            if (listDropDown.Items.Count == 0) return;

            if (key == Key.Down)
            {
                if (index == (listDropDown.Items.Count - 1)) return;

                listDropDown.SelectedIndex = ++index;
            }
            else if (key == Key.Up)
            {
                if (index <= 0) return;

                listDropDown.SelectedIndex = --index;
            }

            listDropDown.ScrollIntoView(listDropDown.Items[index]);
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up)
            {
                HandleUpDown(e.Key);
                return;
            }

            if (e.Key == Key.Escape)
            {
                txtsearch.Text = string.Empty;
            }

            if (e.Key == Key.Enter)
            {
                if (listDropDown.SelectedItem != null)
                    OnSelected?.Invoke(listDropDown.SelectedItem);
                bdrDropDown.Visibility = Visibility.Collapsed;
                txtsearch.Text = string.Empty;
                listDropDown.SelectedIndex = -1;
            }

            //RaiseEvent(new RoutedEventArgs(OnTextChangedEvent, this));
            OnTextChanged?.DynamicInvoke(txtsearch.Text.ToLower());

            if (!string.IsNullOrEmpty(txtsearch.Text) && bdrDropDown.Visibility == Visibility.Collapsed)
            {
                bdrDropDown.Visibility = Visibility.Visible;
                listDropDown.SelectedIndex = 0;
            }
            
            if (string.IsNullOrEmpty(txtsearch.Text))
            {
                bdrDropDown.Visibility = Visibility.Collapsed;
            }
        }
    }
}
