﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for SaleCartUC.xaml
    /// </summary>
    public partial class SaleCartUC : UserControl
    {
        public SaleCartUC()
        {
            InitializeComponent();
            SeriesCollection = new SeriesCollection()
            {
                new ColumnSeries()
                {
                    Title = "Sales",
                    Values = new ChartValues<int> {500, 34, 345, 1500, 500, 500, 34, 345, 1500, 500, 500, 34, 345, 1500, 500},
                    //LineSmoothness = 0
                    MaxColumnWidth = 15
                },
                new ColumnSeries()
                {
                    Title = "Purchases",
                    Values = new ChartValues<int> {5000, 200, 5222, 600, 200, 500, 600, 200, 500, 0, 0, 0},
                    //LineSmoothness = 0
                    MaxColumnWidth = 15

                },
                new ColumnSeries()
                {
                    Title = "Expense",
                    Values = new ChartValues<int> {500, 20, 222, 600, 2000, 70, 70, 200, 5500, 450, 2520, 220},
                    //LineSmoothness = 0
                    MaxColumnWidth = 15

                }
            };

            Labels = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jan", "Feb", "Mar", "Apr", "May", "Jan", "Feb", "Mar", "Apr", "May" };
            YFormatter = value => value.ToString("C");
            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }
    }
}
