﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.Cart;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for CartUC.xaml
    /// </summary>
    public partial class CartUC : UserControl
    {
        private IMongoDatabase db = ConnectionHelper.GetConnection();
        private CartIntitialData initialData = new CartIntitialData();
        private CartModel cart;

        public CartUC(CartModel saleCart)
        {
            InitializeComponent();
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();

            //var a = this.Resources["dialogeContent"];
            //GlobalEvent.OnOpenDialoge("sdfsfss", a);

            initialData.onInitialized += InitialData_onInitialized;
            initialData.Initialize();

            this.cart = saleCart;

            if(cart != null && cart.CartItems != null)
            {
                cart.CartItems.ForEach(items => cartItemList.Items.Add(items));
                if (cart.CustomerId.HasValue && cart.CustomerId.Value != ObjectId.Empty)
                {
                    var customers = cbxCustomers.Items.Cast<CustomerModel>().ToList();
                    cbxCustomers.SelectedItem = customers.FirstOrDefault(x => x.Id.Equals(cart.CustomerId));
                }
            }
            BillSummary();

            GlobalEvent.AddNewCustomer += (CustomerModel customer) =>
                initialData.Initialize();
        }

        private void GlobalEvent_AddNewCustomer(CustomerModel obj)
        {
            throw new NotImplementedException();
        }

        private void InitialData_onInitialized(QueryResultModel result)
        {
            cbxCustomers.ItemsSource = result.Records["customers"] as List<CustomerModel>;
        }

        private void BtnProceed_Click(object sender, RoutedEventArgs e)
        {
            if (cartItemList.Items.Count != 0)
            {
                var popup = new CheckoutPopup(cart, () =>
                {
                    cartItemList.Items.Clear();
                    cbxCustomers.SelectedIndex = -1;
                    cart = new CartModel();
                    BillSummary();
                });

                popup.Owner = Window.GetWindow(this);
                popup.Show();
            }
        }

        private void BtnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            AddCustomerPopup customerPopup = new AddCustomerPopup();
            customerPopup.Show();
        }

        private void AddToCart(ProductModel product)
        {
            if(product.Type == (int)ProductType.Standard && (product.Stock == null || product.Stock.Qty == 0))
            {
                InformationalPopup.Show("Cart", "Out of stock");
                return;
            }

            List<CartItemModel> products = cartItemList.Items.Cast<CartItemModel>().ToList();

            CartItemModel cp = products.FirstOrDefault(x => x.ProductId.Equals(product.Id));
            if (cp != null)
                cp.Qty++;
            else
            {
                CartItemModel cartItem = new CartItemModel
                {
                    Name = product.Name,
                    Qty = 1,
                    Price = product.Price,
                    Stock = product.Stock,
                    Type = product.Type,
                    ProductId = product.Id,
                    CategoryId = product.CategoryId,
                    CategoryName = product.Category.Name,
                    ManufacturerId = product.ManufacturerId,
                    ManufacturerName = product.Manufacturer.Name
                };
                if(product.Type == (int)ProductType.Combo && product.Combo != null)
                {
                    cartItem.SubCartItems = new List<SubCartItemModel>();
                    product.Combo.ForEach(combo =>
                    {
                        cartItem.SubCartItems.Add(new SubCartItemModel
                        {
                            Name = combo.Name,
                            Qty = combo.Qty
                        });
                    });
                }

                cartItemList.Items.Insert(0, cartItem);
            }
            cartItemList.Items.Refresh();
            cartItemList.SelectedIndex = 0;
            BillSummary();
        }

        private void CartItemList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ListBoxItem)cartItemList.ItemContainerGenerator.ContainerFromIndex(0);
        }

        private void BtnAddNote_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CartItemModel cartItem = (CartItemModel)button.DataContext;
            var result = InputPopup.Show("Cart", "Enter Note for the Product", cartItem.Note);
            if (result.Result) cartItem.Note = result.Text;
            cartItemList.Items.Refresh();
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CartItemModel cartItem = (CartItemModel)button.DataContext;
            if(cartItem.Type == (int)ProductType.Standard && cartItem.Qty == cartItem.Stock.Qty)
            {
                InformationalPopup.Show("Cart", "Insuffitiant Stock");
                return;
            }

            cartItem.Qty++;
            cartItemList.Items.Refresh();
            BillSummary();
        }

        private void BtnRemove_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CartItemModel cartItem = (CartItemModel)button.DataContext;
            if (cartItem.Qty == 1) return;

            cartItem.Qty--;
            cartItemList.Items.Refresh();
            BillSummary();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CartItemModel cartItem = (CartItemModel)button.DataContext;
            cartItemList.Items.Remove(cartItem);

            if (cartItemList.Items.Count == 0)
            {
                cart.CartDiscount = 0;
            }

            BillSummary();
        }


        private void BillSummary()
        {
            if (cart == null)
                cart = new CartModel();

            cart.CartItems = cartItemList.Items.Cast<CartItemModel>().ToList();

            txtSubTotal.Text = cart.SubTotalWitoutDiscount.ToString("C", CultureHelper.CustomCulture());
            txtTotalPayable.Text = cart.Total.ToString("C", CultureHelper.CustomCulture());
            txtDiscount.Text = cart.Discount.ToString("C", CultureHelper.CustomCulture());
            TotalItems.Text = cart.CartItems.Count + " (" + cart.TotalItems + ")";
        }

        private void CbxCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cart == null)
                cart = new CartModel();

            if (cbxCustomers.SelectedItem != null)
            {
                cart.CustomerId = ((CustomerModel)cbxCustomers.SelectedItem).Id;
                cart.CustomerName = ((CustomerModel)cbxCustomers.SelectedItem).Name;
            }
            else
            {
                cart.CustomerId = null;
                cart.CustomerName = null;
            }
        }

        private async void BtnHold_Click(object sender, RoutedEventArgs e)
        {
            if(cartItemList.Items.Count == 0)
            {
                InformationalPopup.Show("Cart", "Cart is Empty");
                return;
            }

            var result = InputPopup.Show("Cart", "Reference Note");
            if (!result.Result) return;

            cart.RefNote = result.Text;
            cart.Status = (int)SaleStatus.Hold;
            cart.UserId = Session.User.Id;
            cart.Date = DateTime.Now;

            try
            {
                var db = ConnectionHelper.GetConnection();
                await db.GetCollection<CartModel>("Sale").InsertOneAsync(cart);
                GlobalEvent.OnCheckout(null);
            }
            catch(Exception ex)
            {
                InformationalPopup.Show("Cart", ex.Message);
            }
        }

        private void BtnClearCart_Click(object sender, RoutedEventArgs e)
        {
            if (cartItemList.Items.Count == 0) return;

            bool result = ConformationalPopup.Show("Cart", "Do you want to clear cart items..?", Window.GetWindow(this));
            if(result)
            {
                GlobalEvent.OnCheckout(null);
            }
        }

        private void btnQty_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            var cartItem = (CartItemModel)button.DataContext;
            int qty = CartQtyInputPopup.Show(cartItem.Qty.ToString());
            if (cartItem.Type == (int)ProductType.Standard && qty > cartItem.Stock.Qty)
            {
                InformationalPopup.Show("Cart", "Insuffitiant Stock");
                return;
            }

            cartItem.Qty = qty;
            cartItemList.Items.Refresh();
            BillSummary();
           // txtsearch.Focus();
        }

        private void btnPrice_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            var cartItem = (CartItemModel)button.DataContext;
            var result = CartPriceInputPopup.Show(cartItem.Price.ToString());
            if (result.price <= 0)
            {
                InformationalPopup.Show("Cart", "Price must be greater than 0");
                return;
            }

            cartItem.Price = result.price;
            cartItemList.Items.Refresh();
            BillSummary();

            if (result.updatePrice)
            {
                var update = Builders<ProductModel>.Update.Set(x => x.Price, result.price);
                ProductDAL.UpdateByIdAsync(cartItem.ProductId, update);
            }
            //txtsearch.Focus();
        }

        private async void autoCompleteTextBoxUC_OnTextChanged(string text)
        {
            List<ProductModel> products = await ProductDAL.SearchAsync(text, 50);
            List<CategoryModel> categories = initialData.Result.Records["categories"] as List<CategoryModel>;
            List<ManufacturerModel> manufacturers = initialData.Result.Records["manufacturers"] as List<ManufacturerModel>;

            products.ForEach(product =>
            {
                product.Category = categories?.Find(category => category.Id.Equals(product.CategoryId));
                product.Manufacturer = manufacturers?.Find(manufacturer => manufacturer.Id.Equals(product.ManufacturerId));
            });

            autoCompleteTxt.Items = products;
        }

        private void autoCompleteTxt_OnSelected(object seletedProduct)
        {
            AddToCart(seletedProduct as ProductModel);
        }

        private void btnDiscount_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            var cartItem = (CartItemModel)button.DataContext;

            CartDiscountPopupModel discountModel = new CartDiscountPopupModel
            {
                Title = cartItem.Name,
                OriginalPrice = cartItem.LineTotal
            };

            if (cartItem.Discount != null)
            {
                discountModel.Reason = cartItem.Discount.Reason;
                discountModel.DiscountValue = cartItem.Discount.DiscountValue;
                discountModel.DiscountType = cartItem.Discount.DiscountType;
            }
            discountModel = CartDiscountPopup.Show(discountModel);

            if (discountModel != null)
            {
                if (cartItem.Discount == null)
                    cartItem.Discount = new CartDiscountModel();

                cartItem.Discount.DiscountType = discountModel.DiscountType;
                cartItem.Discount.DiscountValue = discountModel.DiscountValue;
                cartItem.Discount.Reason = discountModel.Reason;
                cartItemList.Items.Refresh();
                BillSummary();
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            autoCompleteTxt.DropdownMaxHeight = Convert.ToInt32(e.NewSize.Height - 340);
            cartItemList.Height = e.NewSize.Height - 300;
        }
    }
}
