﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for SelectionSummaryUC.xaml
    /// </summary>
    public partial class SelectionSummaryUC : UserControl
    {
        public event Action OnRemoveSelection;

        public SelectionSummaryUC()
            => InitializeComponent();

        public int SelectedValue
        {
            set
            {
                btnRemoveSelection.Content = $"Remove Selection ({value.ToString("N0")})";
                btnRemoveSelection.Visibility = value == 0 ? Visibility.Hidden : Visibility.Visible;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
            => OnRemoveSelection?.Invoke();
    }
}
