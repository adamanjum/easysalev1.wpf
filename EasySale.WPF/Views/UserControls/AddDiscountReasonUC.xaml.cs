﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for AddDiscountReasonUC.xaml
    /// </summary>
    public partial class AddDiscountReasonUC : UserControl
    {
        /**
         * @events
         **/

        public event Action<AddDiscountReasonUC> OnCancel;
        public event Action<AddDiscountReasonUC, ReasonModel> OnSave;

        /**
         * @properties
         **/

        public ReasonModel Reason { get; set; }

        public AddDiscountReasonUC(ReasonType type)
        {
            InitializeComponent();

            Reason = new ReasonModel(type);
            DataContext = this;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            OnCancel?.Invoke(this);
        }

        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Reason.Title))
                {
                    throw new Exception("Title is required field");
                }

                var query = new Dictionary<string, object>
                {
                    ["Title"] = Reason.Title,
                    ["Type"] = Reason.Type
                };

                var existingReason = await ReasonDAL.GetOneAsync(query);

                if (existingReason != null)
                {
                    throw new Exception("Discount reason already exist");
                }

                await ReasonDAL.InsertAsync(Reason);
                OnSave?.Invoke(this, Reason);
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Expense", ex.Message);
            }
        }
    }
}
