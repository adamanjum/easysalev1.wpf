﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for TabHeader.xaml
    /// </summary>
    public partial class TabHeaderUC : UserControl
    {
        private readonly Action<int> onDelete;
        public TabHeaderUC(Action<int> onDelete)
        {
            InitializeComponent();
            this.onDelete = onDelete;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            onDelete(1);
        }
    }
}
