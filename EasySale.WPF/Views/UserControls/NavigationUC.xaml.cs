﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Pages;
using EasySale.WPF.Views.Pages.Purchase;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for NavigationUC.xaml
    /// </summary>
    public partial class NavigationUC : UserControl
    {
        private Frame frame;
        public NavigationUC(Frame contentFrame)
        {
            InitializeComponent();
            frame = contentFrame;
        }

        private void BtnDashboard_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new DashboardPage());

        private void MenuSaleHistory_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new SaleListPage());

        private void MenuNewPurchase_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new AddPurchasesPage());

        private void MenuPurchaseList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new PurchasesListPage());

        private void MenuAddProducts_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new AddProductPage());

        private void MenuProductList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new ProductListPage());

        private void MenuAddCategory_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new Uri("Views/Pages/AddCategoryPage.xaml", UriKind.Relative));

        private void MenuCategoryList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new CategoryListPage());

        private void MenuAddUser_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new AddUserPage());

        private void MenuUserList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new UsersListPage());

        private void MenuAddCustomer_Click(object sender, RoutedEventArgs e)
            => new AddCustomerPopup().Show();

        private void MenuListCustomer_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new CustomerList());

        private void MenuAddSupplier_Click(object sender, RoutedEventArgs e)
            => new AddSupplierPopup().Show();

        private void MenuSuppliersList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new SuppliersListPage());

        private void MenuHome_Click(object sender, RoutedEventArgs e)
        {
            if(Session.Register == null)
            {
                OpenRegisterPopup openRegister = new OpenRegisterPopup();
                if (!(bool)openRegister.ShowDialog())
                    return;

                Session.Register = new RegisterModel
                {
                    OpenAt = DateTime.Now,
                    CashInHand = openRegister.Amount,
                    UserId = Session.User.Id
                };
                RegisterDAL.SaveRegisterAsync(Session.Register);
                GlobalEvent.OnRegisterOpened(null);
            }

            frame.Navigate(new HomePage());
        }

        private void MenuSaleOnHold_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new SaleOnHoldPage());

        private void MenuAddExpense_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new AddExpensePage());

        private void MenuExpenseList_Click(object sender, RoutedEventArgs e)
             => frame.Navigate(new ExpenseListPage());

        private void menuShopSetting_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new ShopSettingPage());

        private void menuPrinterSettings_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new PrinterSettingPage());

        private void menuInvoice_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new InvoiceSettingPage());

        private void menuImportProduct_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new ImportProductPage());

        private void menuAddManufacturer_Click(object sender, RoutedEventArgs e)
            => new AddManufacturerPopup().Show();

        private void MenuManufacturerList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new ManufacturerListPage());

        private void MenuProductReportList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new ProductReportPage());

        private void menuStockAdjustmentReport_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new StockAdjustmentReportPage());

        private void menuRegisterReport_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new RegisterReportPage());

        private void menuPurchaseOrder_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new NewPurchaseOrderPage());

        private void menuPurchaseOrderList_Click(object sender, RoutedEventArgs e)
            => frame.Navigate(new PurchaseOrdersListPage());
    }
}
