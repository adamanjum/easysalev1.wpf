﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;

namespace EasySale.WPF.Views.UserControls
{
    /// <summary>
    /// Interaction logic for ProductV1UC.xaml
    /// </summary>
    public partial class ProductV1UC : UserControl
    {
        public ProductV1UC()
        {
            InitializeComponent();
            List<ProductModel> productV1ViewModels = new List<ProductModel>();
            productV1ViewModels.Add(new ProductModel
            {
                ImgPath = "https://upload.wikimedia.org/wikipedia/commons/0/08/Pencils_hb.jpg",
                Name = "Pencil",

                Price = 100,
                SKU = "we23sfs-45"
            });

            productV1ViewModels.Add(new ProductModel
            {
                ImgPath = "https://cdn.dick-blick.com/items/204/08/20408-Group-2-0ww-xs.jpg",
                Name = "Pencil",

                Price = 10540,
                SKU = "we23sfs-45"
            });

            productV1ViewModels.Add(new ProductModel
            {
                ImgPath = "https://cdn.dick-blick.com/items/204/08/20408-Group-2-0ww-xs.jpg",
                Name = "Pencil Books Sharpner",
                Price = 100,
                SKU = "we23sfs-45"
            });

            productList.ItemsSource = productV1ViewModels;
        }

        private void ListBoxItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            //string result = InputPopup.Show("sale", "Enter Coupon Number");
        }
    }
}
