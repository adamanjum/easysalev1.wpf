﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.UserControls
{
    public delegate void PageChange(int pageNo);
    public delegate void RangeChange(int rangeChange);
    /// <summary>
    /// Interaction logic for PaginationUC.xaml
    /// </summary>
    public partial class PaginationUC : UserControl
    {
        private readonly Regex regexInterger = new Regex("[^0-9]+");
        public event PageChange OnPageChanged;
        public event RangeChange OnRangeChanged;
        private long totalRecords;

        public long TotalRecords
        {
            get => totalRecords;
            set
            {
                totalRecords = value;
                DisplayLabels();
            }
        }

        public int PageNo
        {
            get
            {
                if (string.IsNullOrWhiteSpace(textBoxPageNo.Text))
                {
                    textBoxPageNo.Text = "1";
                    return 1;
                }
                int pageNo = Convert.ToInt32(textBoxPageNo.Text);
                if ( pageNo == 0)
                {
                    textBoxPageNo.Text = "1";
                    return 1;
                }
                return pageNo;
            }
            set => textBoxPageNo.Text = value.ToString();
        }

        public int CalculatedRecords
        {
            get => PageNo * PageRange;
        }

        public int MaxPage
        {
            get => Convert.ToInt32(Math.Ceiling(totalRecords / Convert.ToDouble(PageRange)));
        }

        public int PageRange
        {
            get
            {
                var selectedItem = cbxPagePange.SelectedItem as ComboBoxItem;
                int pageRange = 20;
                if (selectedItem != null)
                    pageRange = Convert.ToInt32(selectedItem.Tag);

                return pageRange;
            }
            set
            {
                cbxPagePange.SelectedItem = cbxPagePange
                    .Items
                    .Cast<ComboBoxItem>()
                    .FirstOrDefault((item) =>
                        Convert.ToInt32(item.Tag) == value);
            }
        }

        public PaginationUC()
        {
            InitializeComponent();
        }

        private void textBoxPageNo_PreviewTextInput(object sender, TextCompositionEventArgs e)
            => e.Handled = regexInterger.IsMatch(e.Text);

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (PageNo == 1) return;
            PageNo -= 1;
            OnPageChanged(PageNo);
            DisplayLabels();
        }

        private void cbxPagePange_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnRangeChanged?.Invoke(PageRange);
            DisplayLabels();
        }

        private void textBoxPageNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (PageNo > MaxPage)
                    PageNo = MaxPage;
                textBoxPageNo.SelectAll();
                OnPageChanged?.Invoke(PageNo);
                DisplayLabels();
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (CalculatedRecords >= TotalRecords) return;
            PageNo += 1;
            OnPageChanged(PageNo);
            DisplayLabels();
        }

        private void DisplayLabels()
        {
            long first = PageRange * (PageNo - 1) + 1;
            long last = first + PageRange - 1;
            if (last > totalRecords)
                last = totalRecords;

            txtfirst.Text = first.ToString("N0");
            txtLast.Text = last.ToString("N0");
            txtCount.Text = totalRecords.ToString("N0");
            txtTotalPage.Text = $"/{MaxPage.ToString()}";
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            textBoxPageNo.Focus();
            textBoxPageNo.CaretIndex = textBoxPageNo.Text.Length;
        }
    }
}
