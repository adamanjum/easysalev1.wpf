﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;

namespace EasySale.WPF.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        private Stopwatch processingTime = new Stopwatch();
        public Login()
        {
            Application.Current.Dispatcher.UnhandledException += Dispatcher_UnhandledException;
            InitializeComponent();
            txtEmail.Focus();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
        }

        private void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            InformationalPopup.Show("Error", e.Exception.Message);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            var data = (loginHelper)e.Result;
            if(data.User == null)
            {
                InformationalPopup.Show("Login", "Incorrect Email or Password");
                return;
            }

            Session.User = data.User;
            Session.Store = data.Store;
            Session.Register = data.Register;
            Session.PrinterSettings = data.PrinterSetting;
            Session.InvoiceSetting = data.InvoiceSettings;
            Session.Role = data.Role;
            MainWindow window = new MainWindow();
            window.Show();
            Close();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = (loginHelper)e.Argument;
            data.User = UserDAL.AuthUser(data.Email, data.Password);
            if(data.User != null)
            {
                data.Store = StoreDAL.GetStore();
                data.Register = RegisterDAL.GetOpenedRedisterByUser(data.User.Id);
                data.PrinterSetting = PrinterDAL.GetPrinterSetting();
                data.InvoiceSettings = InvoiceSettingDAL.GetByStore(data.Store.Id);
                data.Role = RoleDAL.RoleById(data.User.RoleId);
            }
            e.Result = data;

            processingTime.Stop();
            TimeSpan time = processingTime.Elapsed;
            if (time.TotalMilliseconds < 2000)
            {
                Thread.Sleep(2000 - Convert.ToInt32(time.TotalMilliseconds));
            }
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtEmail.Text) || string.IsNullOrWhiteSpace(password.Password))
            {
                InformationalPopup.Show("Login", "Email and Password are required");
                return;
            }

            if(!bw.IsBusy)
            {
                loader.Visibility = Visibility.Visible;
                processingTime.Start();
                bw.RunWorkerAsync(new loginHelper { Email = txtEmail.Text, Password = password.Password });
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
            => Close();

        private void password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnLogin_Click(null, null);
            }
        }
    }

    internal class loginHelper
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public UserModel User { get; set; }
        public RegisterModel Register { get; set; }
        public StoreModel Store { get; set; }
        public PrinterModel PrinterSetting { get; set; }
        public InvoiceSetting InvoiceSettings { get; set; }
        public RoleModel Role { get; set; }
    }
}
