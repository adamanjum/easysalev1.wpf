﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for CheckoutPopup.xaml
    /// </summary>
    public partial class CheckoutPopup : Window
    {
        private CartModel cart;
        private Action OnCheckout;
        public CheckoutPopup(CartModel saleCart, Action OnCheckout)
        {
            InitializeComponent();
            this.cart = saleCart;
            this.OnCheckout = OnCheckout;

            UpdateBilling();


            if(!string.IsNullOrWhiteSpace(cart.CustomerName))
                txtCustomer.Text = cart.CustomerName;
        }

        private void UpdateBilling()
        {
            txtSubTotal.Text = cart.SubTotalWitoutDiscount.ToString("C", CultureHelper.CustomCulture());
            txtDiscount.Text = cart.Discount.ToString("C", CultureHelper.CustomCulture());
            txtTotalPayable.Text = cart.Total.ToString("C", CultureHelper.CustomCulture());
            txtTotalItems.Text = cart.CartItems.Count + " (" + cart.TotalItems + ")";
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            => toggleCard.IsChecked = !toggleCard.IsChecked.Value;

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnCurrency_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            txtTotalPaying.Text = button.Content.ToString().Split(' ')[1];
        }

        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double.TryParse(txtTotalPaying.Text, out double paying);
                cart.PaymentMode = (int)PaymentMode.Cash;

                if (!string.IsNullOrWhiteSpace(txtNote.Text))
                    cart.Note = txtNote.Text;

                cart.UserId = Session.User.Id;
                cart.Date = DateTime.Now;

                if (toggleCard.IsChecked.Value)
                {
                    if (!string.IsNullOrWhiteSpace(txtCardNo.Text))
                        cart.CardNo = txtCardNo.Text;
                    cart.PaymentMode = (int)PaymentMode.Card;
                    paying = cart.Total;
                }

                if (paying < cart.Total)
                    throw new Exception("Paying amount should be greater then or equal to Total amount");

                cart.Paying = paying;

                cart.Status = (int)SaleStatus.Completed;
                cart.InvoiceNo = Utils.GetUID("ES-IVC-");

                var db = new MongoCRUD();
                if(cart.Id == ObjectId.Empty)
                {
                    await db.InsertRecordAsync("Sale", cart);
                }
                else
                {
                    await db.UpsertRecordAsync("Sale", cart.Id, cart);
                }

                if ((bool)ckbxShowInvoive.IsChecked)
                {
                    new InvoicePreviewPopup(cart).ShowDialog();
                } else
                {
                    new InvoiceHelper().DoPrintJob(cart, Session.Store, Session.InvoiceSetting);
                }

                cart.CartItems.ForEach(async (item) =>
                {
                    ProductModel product = db.LoadRecordById<ProductModel>("Product", item.ProductId);
                    if (product != null)
                    {
                        if (product.Stock == null)
                            product.Stock = new Stock();

                        product.Stock.Qty -= item.Qty;
                        product.Stock.UpdatedAt = new DateTime();
                        product.Stock.Addedby = Session.User.Id;

                        await db.UpsertRecordAsync<ProductModel>("Product", item.ProductId, product);
                    }
                });

                cart = null;

                GlobalEvent.OnCheckout(cart);
                Close();
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Payment", ex.Message);
            }
        }

        private string GetUID()
        {
            throw new NotImplementedException();
        }

        private void TxtTotalPaying_TextChanged(object sender, TextChangedEventArgs e)
        {
            double.TryParse(txtTotalPaying.Text, out double paying);
            double customerBalance = paying - cart.Total;
            txtCustomeBalance.Text = customerBalance.ToString("C", CultureHelper.CustomCulture());
        }

        private void btnApplyDiscount_Click(object sender, RoutedEventArgs e)
        {
            var result = CartDiscountPopup.Show(new CartDiscountPopupModel { Title = "Apply Discount", OriginalPrice = cart.Total });
            cart.CartDiscount = result?.DiscountAmount ?? 0;

            UpdateBilling();
        }
    }
}
