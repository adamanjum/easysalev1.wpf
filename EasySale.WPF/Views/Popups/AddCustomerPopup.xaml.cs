﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for AddCustomerPopup.xaml
    /// </summary>
    public partial class AddCustomerPopup : Window
    {
        private PathHelper path = null;

        public AddCustomerPopup()
        {
            InitializeComponent();
            Customer = new CustomerModel();
            DataContext = this;
        }

        public AddCustomerPopup(CustomerModel customer) : this()
        {
            Customer = customer;
            if (customer.ImgPath != null)
            {
                try
                {
                    string path = ImageHelper.GetDirectoryPath("Customer") + customer.ImgPath;
                    var image = new BitmapImage(new Uri(path));
                    img.Fill = new ImageBrush(image);
                }
                catch (Exception ex)
                {
                    InformationalPopup.Show("Profile Image", ex.Message);
                }
            }
        }

        public CustomerModel Customer { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
            {
                img.Fill = new ImageBrush(new BitmapImage(new Uri(path.Path)));
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("Customer", path.Path, path.FileName);
                    Customer.ImgPath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Customer.Name))
                    throw new Exception("Name is required");

                if (!string.IsNullOrWhiteSpace(Customer.Email) && !RegularExpressions.ValidateEmail(Customer.Email))
                    throw new Exception("Invalid email address..");

                MongoCRUD db = new MongoCRUD();
                if (Customer.Id == ObjectId.Empty)
                {
                    Customer.Addedby = Session.User.Id;
                    Customer.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync<CustomerModel>("Customer", Customer);
                }
                else
                {
                    await db.UpsertRecordAsync("Customer", Customer.Id, Customer);
                }

                GlobalEvent.OnAddNewCustomer(Customer);
                InformationalPopup.Show("Customer", "Customer has been saved successfully");
                Close();

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Customer", ex.Message);
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
            => Close();
    }
}
