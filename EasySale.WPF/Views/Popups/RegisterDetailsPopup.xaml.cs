﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for RegisterDetailsPopup.xaml
    /// </summary>
    public partial class RegisterDetailsPopup : Window
    {
        readonly BackgroundWorker bw = new BackgroundWorker();
        public RegisterDetailsPopup()
        {
            InitializeComponent();
            bw.DoWork += (object sender, DoWorkEventArgs e)
                => { e.Result = GetStats((ObjectId)e.Argument); };

            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(Session.User.Id);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            var data = e.Result as RegisterDetailModel;
            if(data == null)
            {
                // todo when data not found
            }

            txtOpenAt.Text = data.Register.OpenAt.ToString("dddd, dd MMMM yyyy hh: mm tt");
            txtCashInHand.Text = data.CashInHand.ToString("C", CultureHelper.CustomCulture());
            txtCardSale.Text = data.TotalCardSale.ToString("C", CultureHelper.CustomCulture());
            txtCashSale.Text = data.TotalCashSale.ToString("C", CultureHelper.CustomCulture());
            txtSubtotal.Text = data.Subtotal.ToString("C", CultureHelper.CustomCulture());
            txtTotalExpense.Text = data.TotalExprenses.ToString("C", CultureHelper.CustomCulture());
            txtTotalCash.Text = data.TotalCash.ToString("C", CultureHelper.CustomCulture());
        }

        private RegisterDetailModel GetStats(ObjectId userId)
        {
            var db = ConnectionHelper.GetConnection();

            var register = RegisterDAL.GetOpenedRedisterByUser(userId);

            if (register == null)
                return null;

            var sales = SaleDAL.GetSales((int)SaleStatus.Completed, null, register.OpenAt, userId);
            var expenses = ExpenseDAL.GetExpenses(register.OpenAt, null, userId, null, null);

            return new RegisterDetailModel(register)
            {
                TotalCashSale = sales.Where(x => x.PaymentMode == (int)PaymentMode.Cash).Sum(x => x.Total),
                TotalCardSale = sales.Where(x => x.PaymentMode == (int)PaymentMode.Card).Sum(x => x.Total),
                TotalExprenses = expenses.Sum(x => x.Amount)
            };
            
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
