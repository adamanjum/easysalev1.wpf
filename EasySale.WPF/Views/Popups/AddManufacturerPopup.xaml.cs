﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for AddCustomerPopup.xaml
    /// </summary>
    public partial class AddManufacturerPopup : Window
    {
        private PathHelper path = null;
        public event Action<ManufacturerModel, AddManufacturerPopup> OnSaved = null;

        public AddManufacturerPopup()
        {
            InitializeComponent();
            Manufacturer = new ManufacturerModel();
            DataContext = this;
        }

        public AddManufacturerPopup(ManufacturerModel manufacturer) : this()
        {
            Manufacturer = manufacturer;
            if (manufacturer.logoPath != null)
            {
                try
                {
                    string path = ImageHelper.GetDirectoryPath("Manufacturer") + manufacturer.logoPath;
                    var image = new ImageBrush(new BitmapImage(new Uri(path)));
                    logo.Background = image;
                }
                catch (Exception ex)
                {
                    InformationalPopup.Show("Profile Image", ex.Message);
                }
            }
        }

        public ManufacturerModel Manufacturer { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
            {
                logo.Background = new ImageBrush
                {
                    Stretch = Stretch.UniformToFill,
                    ImageSource = new BitmapImage(new Uri(path.Path))
                };
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("Manufacturer", path.Path, path.FileName);
                    Manufacturer.logoPath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Manufacturer.Name))
                    throw new Exception("Name is required");

                if (!string.IsNullOrWhiteSpace(Manufacturer.Email) && !RegularExpressions.ValidateEmail(Manufacturer.Email))
                    throw new Exception("Invalid email address...");

                if (Manufacturer.Id == ObjectId.Empty)
                {
                    Manufacturer.Addedby = Session.User.Id;
                    Manufacturer.CreatedAt = DateTime.Now;
                    await ManufacturerDAL.Insert(Manufacturer);

                    OnSaved?.Invoke(Manufacturer, this);
                }
                else
                {
                    await ManufacturerDAL.Update(Manufacturer.Id, Manufacturer);
                }

                InformationalPopup.Show("Manufacturer", "Manufacturer has been saved successfully");
                Close();

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Manufacturer", ex.Message);
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
            => Close();

    }
}
