﻿using EasySale.WPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for OpenRegisterPopup.xaml
    /// </summary>
    public partial class OpenRegisterPopup : Window
    {
        public OpenRegisterPopup()
        {
            InitializeComponent();
        }

        public double Amount { get; set; }

        private void TxtBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtBox.Text))
            {
                Amount = Convert.ToDouble(txtBox.Text);
                DialogResult = true;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
