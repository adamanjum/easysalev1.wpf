﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for ConformationalPopup.xaml
    /// </summary>
    public partial class ConformationalPopup : Window
    {
        private static ConformationalPopup popupsObj = null;
        public ConformationalPopup()
        {
            InitializeComponent();
        }

        public static bool Show(string title, string message, Window owner, string btnAccept = "Yes", string btnReject = "No")
        {
            ConformationalPopup popup = new ConformationalPopup();
            popupsObj = popup;
            popup.txtTitle.Text = title;
            popup.txtMessage.Text = message;
            popup.btnAccept.Content = btnAccept;
            popup.btnReject.Content = btnReject;
            popup.Owner = owner;
            if ((bool)popup.ShowDialog())
                return true;
            return false;
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = true;
            popupsObj.Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = false;
            popupsObj.Close();
        }
    }
}
