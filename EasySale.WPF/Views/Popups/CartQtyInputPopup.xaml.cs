﻿using EasySale.WPF.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for InputPopup.xaml
    /// </summary>
    public partial class CartQtyInputPopup : Window
    {
        private static CartQtyInputPopup popupsObj = null;
        public CartQtyInputPopup()
        {
            InitializeComponent();
        }

        public static int Show(string defaultText = "")
        {
            CartQtyInputPopup popup = new CartQtyInputPopup();
            popupsObj = popup;
            popup.txtBox.Text = defaultText;
            popup.txtBox.Focus();
            popup.txtBox.CaretIndex = popup.txtBox.Text.Length;
            popup.txtBox.SelectAll();
            if ((bool)popup.ShowDialog())
            {
                if (string.IsNullOrWhiteSpace(popup.txtBox.Text))
                    popup.txtBox.Text = defaultText;
                return Convert.ToInt32(popup.txtBox.Text);
            }
            return Convert.ToInt32(defaultText);
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = true;
            popupsObj.Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = false;
            popupsObj.Close();
        }

        private void txtBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void txtBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                popupsObj.DialogResult = true;
                popupsObj.Close();
            }
        }
    }


}
