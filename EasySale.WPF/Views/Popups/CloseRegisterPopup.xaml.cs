﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for CloseRegisterPopup.xaml
    /// </summary>
    public partial class CloseRegisterPopup : Window
    {
        readonly BackgroundWorker bw = new BackgroundWorker();
        RegisterDetailModel data = null;
        public CloseRegisterPopup()
        {
            InitializeComponent();
            bw.DoWork += (object sender, DoWorkEventArgs e)
                => { e.Result = GetStats((ObjectId)e.Argument); };

            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(Session.User.Id);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            data = e.Result as RegisterDetailModel;
            if (data == null)
            {
                // todo when data not found
            }

            txtOpenAt.Text = data.Register.OpenAt.ToString("dddd, dd MMMM yyyy hh:mm tt");
            txtCashInHand.Text = data.CashInHand.ToString("C", CultureHelper.CustomCulture());
            txtCardSale.Text = data.TotalCardSale.ToString("C", CultureHelper.CustomCulture());
            txtCashSale.Text = data.TotalCashSale.ToString("C", CultureHelper.CustomCulture());
            txtSubtotal.Text = data.Subtotal.ToString("C", CultureHelper.CustomCulture());
            txtTotalExpense.Text = data.TotalExprenses.ToString("C", CultureHelper.CustomCulture());
            txtTotalCash.Text = data.TotalCash.ToString("C", CultureHelper.CustomCulture());
        }

        private RegisterDetailModel GetStats(ObjectId userId)
        {
            var db = ConnectionHelper.GetConnection();

            var register = RegisterDAL.GetOpenedRedisterByUser(Session.User.Id);

            if (register == null)
                return null;

            var sales = SaleDAL.GetSales((int)SaleStatus.Completed, null, register.OpenAt, userId);
            var expenses = ExpenseDAL.GetExpenses(register.OpenAt, null, userId, null, null);

            return new RegisterDetailModel (register)
            {
                TotalCashSale = sales.Where(x => x.PaymentMode == (int)PaymentMode.Cash).Sum(x => x.Total),
                TotalCardSale = sales.Where(x => x.PaymentMode == (int)PaymentMode.Card).Sum(x => x.Total),
                TotalExprenses = expenses.Sum(x => x.Amount)
            };

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
            => e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));


        private async void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtbxTotalCash.Text))
                    throw new Exception("Total Cash is required");

                if (string.IsNullOrWhiteSpace(txtBxTotalCardSlip.Text))
                    throw new Exception("Total Card Slip is required");

                data.Register.TotalCash = Convert.ToDouble(txtbxTotalCash.Text);
                data.Register.CreditCardSlips = Convert.ToInt32(txtBxTotalCardSlip.Text);
                data.Register.Note = txtBxNote.Text;
                data.Register.CloseAt = DateTime.Now;

                var db = new MongoCRUD();

                await db.UpsertRecordAsync("Register", data.Register.Id, data.Register);
                GlobalEvent.OnRegisterClosed(null);
                Close();
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Register", ex.Message);
            }
        }
    }
}
