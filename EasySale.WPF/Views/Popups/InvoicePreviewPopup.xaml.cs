﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for CheckoutPopup.xaml
    /// </summary>
    public partial class InvoicePreviewPopup : Window
    {
        CartModel cart = null;
        public InvoicePreviewPopup(CartModel cart)
        {
            InitializeComponent();
            this.cart = cart;
            flowInvoive.Document = new InvoiceHelper().GetFlowDocumentAsync(cart, Session.Store, Session.InvoiceSetting);
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            if (cart != null)
            {
                new InvoiceHelper().DoPrintJob(cart, Session.Store, Session.InvoiceSetting);
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
