﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for InformationalPopup.xaml
    /// </summary>
    public partial class InformationalPopup : Window
    {
        private static InformationalPopup popupsObj = null;
        public InformationalPopup()
        {
            InitializeComponent();
        }


        public static void Show(string title, string message, string btnText = "Ok", Window owner = null)
        {
            InformationalPopup popup = new InformationalPopup();
            popupsObj = popup;
            popup.txtTitle.Text = title;
            popup.txtMessage.Text = message;
            popup.btnSubmit.Content = btnText;
            popup.Owner = owner;
            popup.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.Close();
        }
    }
}
