﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for InputPopup.xaml
    /// </summary>
    public partial class InputPopup : Window
    {
        private static InputPopup popupsObj = null;
        public InputPopup()
        {
            InitializeComponent();
        }

        public static InputHelper Show(string title, string lable, string defaultText = "", string btnSubmit = "Submit", string btnCancel = "Cancel")
        {
            InputPopup popup = new InputPopup();
            popupsObj = popup;
            popup.txtTitle.Text = title;
            popup.txtBox.Text = defaultText;
            popup.txtLable.Text = lable;
            popup.btnSubmit.Content = btnSubmit;
            popup.btnCancel.Content = btnCancel;
            if ((bool)popup.ShowDialog())
                return new InputHelper { Text = popupsObj.txtBox.Text, Result = true };
            return new InputHelper { Text = popupsObj.txtBox.Text, Result = false };
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = true;
            popupsObj.Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            popupsObj.DialogResult = false;
            popupsObj.Close();
        }
    }

    public class InputHelper
    {
        public string Text { get; set; }
        public bool Result { get; set; }
    }
}
