﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for StockAdjustmentPopup.xaml
    /// </summary>
    public partial class StockAdjustmentPopup : Window
    {
        public StockAdjustmentModel result { get; set; }

        public StockAdjustmentPopup(StockAdjustmentModel input)
        {
            InitializeComponent();
            FetchDiscountReasons();

            result = input;

            txtBoxStockValue.Text = input.NewStockValue.ToString();
            txtBoxStockValue.CaretIndex = txtBoxStockValue.Text.Length;
            txtBoxStockValue.SelectAll();
            txtBoxStockValue.Focus();

            txtProductTitle.Text = input.ProductName;

            updateResult();
        }

        private async void FetchDiscountReasons()
        {
            List<ReasonModel> discountReasons = await ReasonDAL.GetAllAsync(new Dictionary<string, object>
            {
                ["Type"] = ReasonType.Stock
            });

            comboBoxReason.ItemsSource = discountReasons;
            comboBoxReason.SelectedItem = comboBoxReason.Items.Cast<ReasonModel>().ToList().Find(x => x.Title == result.Reason);
        }

        public static CartDiscountPopupModel Show(CartDiscountPopupModel input)
        {
            CartDiscountPopup popup = new CartDiscountPopup(input);

            if ((bool)popup.ShowDialog())
            {
                return popup.result;
            }
            return null;
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void txtBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void txtBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                DialogResult = true;
                Close();
            }
        }

        private void updateResult()
        {
            txtCurrentStock.Text = $"{result.CurrentStockValue} PCS";
            txtStockDifference.Text = $"{result.StockDifference} PCS";
        }

        private void txtBoxDiscount_TextChanged(object sender, TextChangedEventArgs e)
        {
            string value = string.IsNullOrEmpty(txtBoxStockValue.Text) ? "0" : txtBoxStockValue.Text;
            result.NewStockValue = Convert.ToDouble(value);
            updateResult();
        }

        private void comboBoxReason_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            result.Reason = ((ReasonModel)comboBoxReason.SelectedItem)?.Title;
        }

        private void btnAddReason_Click(object sender, RoutedEventArgs e)
        {
            AddDiscountReasonUC userControl = new AddDiscountReasonUC(ReasonType.Stock);
            userControl.OnCancel += UserControl_OnCancel;
            userControl.OnSave += UserControl_OnSave;
            contentAddReason.Content = userControl;
        }

        private void UserControl_OnSave(AddDiscountReasonUC usercontrol, ReasonModel model)
        {
            FetchDiscountReasons();
            UserControl_OnCancel(usercontrol);
        }

        private void UserControl_OnCancel(AddDiscountReasonUC usercontrol)
        {
            contentAddReason.Content = null;
            usercontrol.OnCancel -= UserControl_OnCancel;
            usercontrol.OnSave -= UserControl_OnSave;
        }

        private async void btnDeleteReason_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            ReasonModel reason = (ReasonModel)btn.DataContext;

            Dictionary<string, object> query = new Dictionary<string, object>
            {
                ["Id"] = reason.Id
            };

            await ReasonDAL.DeleteOneAsync(query);
            FetchDiscountReasons();
        }
    }

}
