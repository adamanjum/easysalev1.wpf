﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for AddCustomerPopup.xaml
    /// </summary>
    public partial class AddSupplierPopup : Window
    {
        private PathHelper path = null;

        public AddSupplierPopup()
        {
            InitializeComponent();
            Supplier = new SupplierModel();
            DataContext = this;
        }

        public AddSupplierPopup(SupplierModel supplier) : this()
        {
            Supplier = supplier;
            if (supplier.ImgPath != null)
            {
                try
                {
                    string path = ImageHelper.GetDirectoryPath("Supplier") + supplier.ImgPath;
                    var image = new BitmapImage(new Uri(path));
                    img.Fill = new ImageBrush(image);
                }
                catch (Exception ex)
                {
                    InformationalPopup.Show("Profile Image", ex.Message);
                }
            }
        }

        public SupplierModel Supplier { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
            {
                img.Fill = new ImageBrush(new BitmapImage(new Uri(path.Path)));
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("Supplier", path.Path, path.FileName);
                    Supplier.ImgPath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Supplier.Name))
                    throw new Exception("Name is required");

                if (!string.IsNullOrWhiteSpace(Supplier.Email) && !RegularExpressions.ValidateEmail(Supplier.Email))
                    throw new Exception("Invalid email address..");

                MongoCRUD db = new MongoCRUD();
                if (Supplier.Id == ObjectId.Empty)
                {
                    Supplier.Addedby = Session.User.Id;
                    Supplier.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync<SupplierModel>("Supplier", Supplier);
                }
                else
                {
                    await db.UpsertRecordAsync("Supplier", Supplier.Id, Supplier);
                }

                InformationalPopup.Show("Supplier", "Supplier has been saved successfully");

                this.Close();

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Supplier", ex.Message);
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
            => Close();
    }
}
