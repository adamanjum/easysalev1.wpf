﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using FileHelpers;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for ProductBarcodePopup.xaml
    /// </summary>
    public partial class BulkUpdateProductPopup : Window
    {
        private readonly Action btnExport_Click;
        private PathHelper path = null;
        private List<CategoryModel> categories;
        private List<ManufacturerModel> manufacturers;
        private List<WriteModel<ProductModel>> listWrites;
        private Action OnBulkUpdate;

        public BulkUpdateProductPopup(Action btnExport_Click, Action OnBulkUpdate, List<CategoryModel> categories, List<ManufacturerModel> manufacturers)
        {
            InitializeComponent();
            this.btnExport_Click = btnExport_Click;
            btnSubmit.IsEnabled = false;

            this.categories = categories;
            this.manufacturers = manufacturers;
            this.OnBulkUpdate = OnBulkUpdate;
        }

        private async void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectExcel();
            if (path == null) return;

            int row = 1;
            listWrites = new List<WriteModel<ProductModel>>();
            try
            {
                var engine = new FileHelperEngine<ProductCSVModel>();
                var products = engine.ReadFile(path.Path);

                long total = products.Count();

                List<ProductCSVModel> duplicates = new List<ProductCSVModel>();

                List<CategoryModel> categories = this.categories;
                List<ManufacturerModel> manufacturers = this.manufacturers;

                textProcessing.Visibility = Visibility.Visible;
                btnSubmit.IsEnabled = false;

                foreach (var product in products)
                {
                    var filterDefinition = Builders<ProductModel>.Filter.Eq(p => p.Id, new ObjectId(product.Id));

                    var update = Builders<ProductModel>.Update;
                    var updates = new List<UpdateDefinition<ProductModel>>();

                    /////////////////////////////////////////////////
                    //////////////////// CODE ///////////////////////
                    /////////////////////////////////////////////////

                    if (!string.IsNullOrWhiteSpace(product.Code))
                    {
                        int codeCount = products.Where(x => x.Code.ToLower() == product.Code.ToLower()).Count();

                        var codeProduct = await ProductDAL.GetOneAsync(null, product.Code);
                        if ((codeProduct != null && !codeProduct.Id.Equals(new ObjectId(product.Id))) || codeCount > 1)
                        {
                            if (!duplicates.Any(x => x.Id == product.Id))
                                duplicates.Add(product);
                        }

                        updates.Add(update.Set(p => p.Code, product.Code));
                    }

                    /////////////////////////////////////////////////
                    ///////////////// MANUFACTURER //////////////////
                    /////////////////////////////////////////////////

                    if (!string.IsNullOrWhiteSpace(product.Manufacturer))
                    {
                        var manufacturer = manufacturers.FirstOrDefault(x => x.Name.ToLower() == product.Manufacturer.ToLower());
                        if (manufacturer == null)
                        {
                            await ManufacturerDAL.Insert(new ManufacturerModel
                            {
                                Name = product.Manufacturer,
                                CreatedAt = DateTime.Now,
                                Addedby = Session.User.Id
                            });

                            manufacturer = await ManufacturerDAL.GetByName(product.Manufacturer);
                            manufacturers.Add(manufacturer);
                        }

                        updates.Add(update.Set(p => p.ManufacturerId, manufacturer.Id));
                    }


                    /////////////////////////////////////////////////
                    ///////////////// PRODUCT NAME //////////////////
                    /////////////////////////////////////////////////

                    if (string.IsNullOrWhiteSpace(product.Name))
                        throw new Exception("Name is required");

                    int nameCount = products.Where(x => x.Name.ToLower() == product.Name.ToLower() && x.Manufacturer == product.Manufacturer).Count();

                    var manufacturerProduct = manufacturers.Find(x => x.Name == product.Manufacturer);
                    var NameProduct = await ProductDAL.GetOne(product.Name, manufacturerProduct?.Id);
                    if ((NameProduct != null && !NameProduct.Id.Equals(new ObjectId(product.Id))) || nameCount > 1)
                    {
                        if (!duplicates.Any(x => x.Id == product.Id))
                        {
                            duplicates.Add(product);
                        }
                    }

                    updates.Add(update.Set(p => p.Name, product.Name));


                    /////////////////////////////////////////////////
                    /////////////////// CATEGORY ////////////////////
                    /////////////////////////////////////////////////

                    if (!string.IsNullOrWhiteSpace(product.Category))
                    {
                        var category = categories.FirstOrDefault(x => x.Name.ToLower() == product.Category.ToLower());
                        if (category == null)
                        {
                            await CategoryDAL.Insert(new CategoryModel
                            {
                                Name = product.Category,
                                CreatedAt = DateTime.Now,
                                Addedby = Session.User.Id
                            });

                            category = await CategoryDAL.GetByName(product.Category);
                            categories.Add(category);
                        }

                        updates.Add(update.Set(p => p.CategoryId, category.Id));
                    }


                    /////////////////////////////////////////////////
                    ////////////////////// COST /////////////////////
                    /////////////////////////////////////////////////
                    
                    if (product.Cost.HasValue)
                        updates.Add(update.Set(p => p.Cost, product.Cost));


                    /////////////////////////////////////////////////
                    ///////////////////// PRICE /////////////////////
                    /////////////////////////////////////////////////
                    
                    if (product.Price < 0)
                        throw new Exception("Price must be greater then Zero");

                    updates.Add(update.Set(p => p.Price, product.Price));


                    /////////////////////////////////////////////////
                    //////////////////// MIN. QTY ///////////////////
                    /////////////////////////////////////////////////
                    
                    if (product.MinQty.HasValue)
                        updates.Add(update.Set(p => p.MinQty, product.MinQty));


                    /////////////////////////////////////////////////
                    ////////////////// DESCRIPTION //////////////////
                    /////////////////////////////////////////////////

                    if (!string.IsNullOrWhiteSpace(product.Description))
                        updates.Add(update.Set(p => p.Description, product.Description));

                    /////////////////////////////////////////////////
                    /////////////////// STOCK QTY ///////////////////
                    /////////////////////////////////////////////////
                    
                    if (product.StockQty.HasValue)
                    {
                        var stock = new Stock
                        {
                            Qty = product.StockQty.Value,
                            Addedby = Session.User.Id
                        };

                        updates.Add(update.Set(p => p.Stock, stock));
                    }

                    listWrites.Add(new UpdateManyModel<ProductModel>(filterDefinition, update.Combine(updates)));

                    progress.Value = (row * 100) / total;

                    textProcessing.Text = $"Processing... {progress.Value}%";
                    if (progress.Value == 100)
                        textProcessing.Text = "Processed";

                    row++;
                }
                    
                listSummary.ItemsSource = duplicates;
                if (duplicates.Count > 0)
                    bdrDuplicates.Visibility = Visibility.Visible;
                else
                {
                    bdrDuplicates.Visibility = Visibility.Collapsed;
                    btnSubmit.IsEnabled = true;
                }

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Import Products", ex.Message);
            }
            finally
            {
                row = 1;
            }
        }

        private void btnGetFile_Click(object sender, RoutedEventArgs e)
        {
            btnExport_Click();
        }

        private async void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (path != null)
            {
                btnSubmit.SetValue(ButtonProgressAssist.IsIndicatorVisibleProperty, true);
                await ProductDAL.BulkUpdateAsync(listWrites);
                btnSubmit.SetValue(ButtonProgressAssist.IsIndicatorVisibleProperty, false);
                InformationalPopup.Show("Bulk Update Product", "Products have been Updated successfully");
                OnBulkUpdate();
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void listSummary_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double maxHeight = this.ActualHeight - 500;
            listSummary.MaxHeight = maxHeight > 0 ? maxHeight : listSummary.MaxHeight;
        }

        private async void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var product = (ProductCSVModel)button.DataContext;

            Clipboard.SetText(product.Name);
            Snackbar.Message = new SnackbarMessage { Content = product.Name };
            Snackbar.IsActive = true;

            await Task.Delay(1000).ContinueWith((task) => Dispatcher?.Invoke(() => Snackbar.IsActive = false));
        }
    }
}
