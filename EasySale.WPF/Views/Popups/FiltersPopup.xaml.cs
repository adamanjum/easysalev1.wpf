﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.UserControls;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Popups
{
    /// <summary>
    /// Interaction logic for InputPopup.xaml
    /// </summary>
    public partial class FilterPopup : Window
    {
        public List<CategoryModel> SelectedCategories { get; set; }
        public List<ManufacturerModel> SelectedManufacturers { get; set; }
        public StockLevels? StockLevels { get; set; }

        public FilterPopup(List<CategoryModel> categories, List<ManufacturerModel> manufacturers)
        {
            InitializeComponent();
            listCategory.ItemsSource = categories;
            listManufacturer.ItemsSource = manufacturers;
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            SelectedCategories = listCategory.SelectedItems.Cast<CategoryModel>().ToList();
            SelectedManufacturers = listManufacturer.SelectedItems.Cast<ManufacturerModel>().ToList();

            DialogResult = true;
            Close();
        }

        private void BtnReject_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void btnCategoryRemoveSelection_Click(object sender, RoutedEventArgs e)
        {
            listCategory.SelectedItems.Clear();
        }

        private void btnCategorySelectAll_Click(object sender, RoutedEventArgs e)
        {
            listCategory
                .Items
                .ForEach<CategoryModel>((category)
                    => listCategory.SelectedItems.Add(category));
        }

        private void btnManufacturerRemoveSelection_Click(object sender, RoutedEventArgs e)
        {
            listManufacturer.SelectedItems.Clear();
        }

        private void btnManufacturerSelectAll_Click(object sender, RoutedEventArgs e)
        {
            listManufacturer
                .Items
                .ForEach<ManufacturerModel>((manufacturer)
                    => listManufacturer.SelectedItems.Add(manufacturer));
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ComboBoxItem)((ComboBox)sender).SelectedItem;
            StockLevels = (StockLevels)Enum.Parse(typeof(StockLevels), (string)item.Tag);
        }
    }
}
