﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EasySale.WPF.Views.UserControls;
using EasySale.WPF.Views.Pages;
using EasySale.WPF.Helper;
using EasySale.WPF.Comman;
using EasySale.WPF.Views.Popups;

namespace EasySale.WPF.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Application.Current.Dispatcher.UnhandledException += Dispatcher_UnhandledException;
            navContent.Content = new NavigationUC(contentFrame);

            headerContent.Content = new HeaderUC();

            contentFrame.Navigate(new DashboardPage());

            GlobalEvent.Logout += (object parameter) =>
            {
                Session.User = null;
                Login login = new Login();
                login.Show();
                Close();
            };

            GlobalEvent.RegisterClosed += (object parameter) =>
            {
                Session.Register = null;
                contentFrame.Navigate(new DashboardPage());
                Session.Register = null;
            };

            //GlobalEvent.OpenDialoge += (string headerText, object bodyContent) =>
            //{
            //    dialogBox.Open(headerText, bodyContent);
            //};

            //GlobalEvent.OpenDialogeWithCustomFooter += (string headerText, object bodyContent, object footerContent) =>
            //{
            //    dialogBox.Open(headerText, bodyContent, footerContent);
            //};

            GlobalEvent.MinimizeWindow += (object parameter) => this.WindowState = WindowState.Minimized;
        }

        private void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            InformationalPopup.Show("Error", e.Exception.Message);
        }

    }
}
