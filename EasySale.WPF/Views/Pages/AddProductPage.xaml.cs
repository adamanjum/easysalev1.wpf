﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddProductPage.xaml
    /// </summary>
    public partial class AddProductPage : Page
    {

        /**
         * @visibility
         **/

        public Visibility InitialQtyVisibility
        {
            get { return (Visibility)GetValue(InitialQtyVisibilityProperty); }
            set { SetValue(InitialQtyVisibilityProperty, value); }
        }

        public static readonly DependencyProperty InitialQtyVisibilityProperty =
            DependencyProperty.Register("InitialQtyVisibilityProperty", typeof(Visibility), typeof(AddProductPage), new PropertyMetadata(Visibility.Visible));


        private PathHelper path = null;
        private IMongoDatabase db = ConnectionHelper.GetConnection();
        private QueryModel query = null;
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };

        public AddProductPage()
        {
            InitializeComponent();
            Product = new ProductModel { Stock = new Stock() };
            Product.Stock.Qty = 0;

            this.DataContext = this;

            cbxType.SelectedIndex = 0;

            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();

            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        public AddProductPage(ProductModel product) : this()
        {
            Product = product;
            InitialQtyVisibility = Visibility.Collapsed;
            if (!string.IsNullOrWhiteSpace(product.ImgPath))
            {
                string path = ImageHelper.GetDirectoryPath("Product");
                img.Source = new BitmapImage(new Uri(path + product.ImgPath));
            }

            if (product.Type == (int)ProductType.Combo)
            {
                if (product.Combo != null && product.Combo.Count != 0)
                {
                    product.Combo.ForEach(c => dataGridCombo.Items.Add(c));
                }

                txtPrice.Text = product.Price.ToString();
            }

            if (product.Type == (int)ProductType.Standard)
            {
                if (product.Standard != null)
                {
                    txtQty.Text = product.Standard.Qty.ToString();
                    txtPrice.Text = product.Standard.Price.ToString();
                }
            }
        }

        public AddProductPage(ProductModel product, QueryModel query) : this(product)
        {
            this.query = query;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var response = e.Result as ResponseModel;
            cbxCategory.ItemsSource = response.Categories;
            cbxManufacturer.ItemsSource = response.Manufacturers;

            SelectedCategory = cbxCategory.Items
               .Cast<CategoryModel>()
               .FirstOrDefault(x => x.Id.Equals(Product?.CategoryId));

            if (Product.ManufacturerId.HasValue && Product.ManufacturerId != ObjectId.Empty)
            {
                SelectedManufacturer = cbxManufacturer.Items
                    .Cast<ManufacturerModel>()
                .FirstOrDefault(x => x.Id.Equals(Product.ManufacturerId));
            }

        } 

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync(null, null, null);
            var manufacturers = ManufacturerDAL.Get(null, null, null);

            Task.WaitAll(categories, manufacturers);
            e.Result = new ResponseModel
            {
                Categories = categories.Result, 
                Manufacturers = manufacturers.Result
            };
        }

        public ProductModel Product { get; set; }
        public CategoryModel SelectedCategory { get; set; }
        public ManufacturerModel SelectedManufacturer { get; set; }

        private void CbxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ((ComboBox)sender).SelectedIndex;

            if (index == (int)ProductType.Service)
            {
                stackCostOptional.Visibility = Visibility.Collapsed;
                bdrExtention.Visibility = Visibility.Collapsed;
                stackPrice.Visibility = Visibility.Visible;
            }


            if (index == (int)ProductType.Standard)
            {
                bdrExtention.Visibility = Visibility.Visible;
                stackCostOptional.Visibility = Visibility.Visible;
                stackQtyOptional.Visibility = Visibility.Visible;
                stackComboProduct.Visibility = Visibility.Collapsed;

            }


            if (index == (int)ProductType.Combo)
            {
                bdrExtention.Visibility = Visibility.Visible;
                stackCostOptional.Visibility = Visibility.Collapsed;
                stackQtyOptional.Visibility = Visibility.Collapsed;
                stackComboProduct.Visibility = Visibility.Visible;
                stackPrice.Visibility = Visibility.Collapsed;
            }


        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
                img.Source = new BitmapImage(new Uri(path.Path));
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("Products", path.Path, path.FileName);
                    Product.ImgPath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Product.Name))
                    throw new Exception("Name is required");

                if (SelectedCategory == null)
                    throw new Exception("Select a category");
                Product.CategoryId = SelectedCategory.Id;

                if (SelectedManufacturer != null)
                    Product.ManufacturerId = SelectedManufacturer.Id;


                if (cbxType.SelectedIndex == (int)ProductType.Standard)
                {
                    if (txtPrice.Text != string.Empty && txtQty.Text != string.Empty)
                    {
                        Product.Standard = new StandardProductModel
                        {
                            Price = Convert.ToDecimal(txtPrice.Text),
                            Qty = Convert.ToInt32(txtQty.Text)
                        };
                    }
                }

                if (cbxType.SelectedIndex == (int)ProductType.Combo)
                {
                    if (dataGridCombo.Items.Count != 0)
                    {
                        Product.Combo = dataGridCombo.Items.Cast<ComboProductModel>().ToList();

                        if (string.IsNullOrWhiteSpace(txtPrice.Text))
                            throw new Exception("Please enter combo price");

                        Product.Price = Convert.ToDouble(txtPrice.Text);
                    }
                }

                if (Product.Price == 0)
                    throw new Exception("Price must be greater then 0");

                MongoCRUD db = new MongoCRUD();

                if (Product.Id == ObjectId.Empty)
                {
                    Product.Addedby = Session.User.Id;
                    Product.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync<ProductModel>("Product", Product);
                }
                else
                {
                    await db.UpsertRecordAsync("Product", Product.Id, Product);
                }

                InformationalPopup.Show("Product", "Product has been saved successfully");
                if (query == null)
                    NavigationService.Navigate(new ProductListPage());
                else
                    NavigationService.Navigate(new ProductListPage(query));

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Product", ex.Message);
            }

        }

        private async Task SearchProductAsync(string text)
        {
            listDropDown.ItemsSource = await db.GetCollection<ProductModel>("Product")
                .Find(x => x.Name.ToLower().Contains(text) && x.Type != (int)ProductType.Combo)
                .Limit(20)
                .ToListAsync();

            if (listDropDown.Items.Count > 0 && !string.IsNullOrEmpty(text))
            {
                listDropDown.Visibility = Visibility.Visible;
                listDropDown.SelectedIndex = 0;
            }
            else
            {
                listDropDown.Visibility = Visibility.Collapsed;
            }
        }

        private void AddToCombo(ProductModel product)
        {
            List<ComboProductModel> comboProducts = dataGridCombo.Items.Cast<ComboProductModel>().ToList();

            ComboProductModel cp = comboProducts.FirstOrDefault(x => x.Name == product.Name);
            if (cp != null)
                cp.Qty++;
            else
                dataGridCombo.Items.Insert(0, new ComboProductModel { Name = product.Name, Qty = 1 });
            dataGridCombo.Items.Refresh();
        }

        private async void Txtsearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                listDropDown.Focus();
                var item = (ListBoxItem)listDropDown.ItemContainerGenerator.ContainerFromIndex(0);
                item?.Focus();
                return;
            }

            await SearchProductAsync(txtsearch.Text.ToLower());
        }

        private void ListDropDoen_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down) return;
            if (e.Key == Key.Back)
            {
                txtsearch.Text = txtsearch.Text.Remove(txtsearch.Text.Length - 1);
            }

            if ((e.Key >= Key.A && e.Key <= Key.Z) || (e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                txtsearch.Text += e.Key.ToString().ToLower();
            }

            if (e.Key == Key.Enter)
            {
                if (listDropDown.SelectedItem != null)
                    AddToCombo((ProductModel)listDropDown.SelectedItem);
                listDropDown.Visibility = Visibility.Collapsed;
                txtsearch.Text = string.Empty;
            }

            txtsearch.Focus();
            txtsearch.CaretIndex = txtsearch.Text.Length;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            ComboProductModel cp = button.DataContext as ComboProductModel;
            if (cp != null)
            {
                dataGridCombo.Items.Remove(cp);
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnAddManufacturer_Click(object sender, RoutedEventArgs e)
        {
            AddManufacturerPopup addManufacturerPopup = new AddManufacturerPopup();
            addManufacturerPopup.OnSaved += AddManufacturerPopup_OnSaved;
            addManufacturerPopup.ShowDialog();
        }

        private void AddManufacturerPopup_OnSaved(ManufacturerModel manufacturer, AddManufacturerPopup sender)
        {
            bw.RunWorkerAsync();
            sender.OnSaved -= AddManufacturerPopup_OnSaved;
        }

        
        /**
         * @add category
         */
        ContainerPopup containerPopup;
        private void btnAddCategory_Click(object sender, RoutedEventArgs e)
        {
            AddCategoryPage addCategoryPage = new AddCategoryPage()
            {
                BackButtonVisibility = Visibility.Collapsed,
                CancelButtonVisibility = Visibility.Visible
            };

            addCategoryPage.OnCancel += AddCategoryPage_OnCancel;
            addCategoryPage.OnSave += AddCategoryPage_OnSave;

            containerPopup = new ContainerPopup(addCategoryPage);
            containerPopup.ShowDialog();
        }

        private void AddCategoryPage_OnSave(CategoryModel category, AddCategoryPage page)
        {
            bw.RunWorkerAsync();
            page.OnSave -= AddCategoryPage_OnSave;
            containerPopup?.Close();
        }

        private void AddCategoryPage_OnCancel(AddCategoryPage page)
        {
            containerPopup?.Close();
            page.OnCancel -= AddCategoryPage_OnCancel;
        }
    }
}
