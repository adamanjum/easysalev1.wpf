﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using MongoDB.Driver;
using System.Threading;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddUserPage.xaml
    /// </summary>
    public partial class PrinterSettingPage : Page
    {
        BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };

        public PrinterSettingPage()
        {
            InitializeComponent();

            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            loader.Visibility = Visibility.Visible;
            bw.RunWorkerAsync();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;
            Printer = (PrinterModel)e.Result;
            if(Printer != null)
            {
                ckbxPrintInvoice.IsChecked = Printer.AutoInvoicePrint;
                ckbxPrintLable.IsChecked = Printer.AutoLablePrint;

                if (!string.IsNullOrWhiteSpace(Printer.DefaultInvoicePrinter))
                {
                    var printers = cbxInvoice.Items.Cast<string>().ToList();
                    string invoicePrinter = printers.Find(x => x == Printer.DefaultInvoicePrinter);
                    cbxInvoice.SelectedItem = invoicePrinter;
                }

                if (!string.IsNullOrWhiteSpace(Printer.DefaultLablePrinter))
                {
                    var printers = cbxLable.Items.Cast<string>().ToList();
                    string lablePrinter = printers.Find(x => x == Printer.DefaultLablePrinter);
                    cbxLable.SelectedItem = lablePrinter;
                }
            }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var db = ConnectionHelper.GetConnection();
            var printer = db.GetCollection<PrinterModel>("Printer")
                .Find(Builders<PrinterModel>.Filter.Empty)
                .FirstOrDefault();
            e.Result = printer;
        }

        public PrinterModel Printer { get; set; }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Printer == null)
                    Printer = new PrinterModel();

                if (cbxInvoice.SelectedItem != null)
                    Printer.DefaultInvoicePrinter = cbxInvoice.Text;

                if (cbxLable.SelectedItem != null)
                    Printer.DefaultLablePrinter = cbxLable.Text;

                Printer.AutoInvoicePrint = (bool)ckbxPrintInvoice.IsChecked;
                Printer.AutoLablePrint = (bool)ckbxPrintLable.IsChecked;

                Printer.Addedby = Session.User.Id;
                Printer.CreatedAt = DateTime.Now;

                MongoCRUD db = new MongoCRUD();
                if (Printer.Id == ObjectId.Empty)
                {
                    await db.InsertRecordAsync("Printer", Printer);
                }
                else
                {
                    await db.UpsertRecordAsync("Printer", Printer.Id, Printer);
                }
                Session.PrinterSettings = Printer;

                InformationalPopup.Show("Printer", "Printer has been saved successfully");

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Printer", ex.Message);
            }
        }

    }
}
