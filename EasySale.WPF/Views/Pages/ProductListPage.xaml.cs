﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using FileHelpers;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for ProductListPage.xaml
    /// </summary>
    public partial class ProductListPage : Page
    {
        private QueryModel queryModel = new QueryModel { PageNo = 1, NoOfRecord = 20 };

        public ProductListPage()
        {
            InitializeComponent();
        }

        public ProductListPage(QueryModel query)
        {
            InitializeComponent();
        }

        private void BtnAddProduct_Click(object sender, RoutedEventArgs e)
            => NavigationService.Navigate(new Uri("Views/Pages/AddProductPage.xaml", UriKind.RelativeOrAbsolute));

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            ProductModel product = button.DataContext as ProductModel;
            this.NavigationService.Navigate(new AddProductPage(product, queryModel));
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //dataGridProducts
            //    .Items
            //    .Cast<ProductModel>()
            //    .ToList()
            //    .ForEach((product) => dataGridProducts.SelectedItems.Add(product));
        }

        private void datagrid_CurrentCellValueChanged(object sender, Syncfusion.UI.Xaml.Grid.CurrentCellValueChangedEventArgs e)
        {

        }


        //private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        //    => dataGridProducts.SelectedItems.Clear();


        //private void dataGridProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    var element = UIElementHelper.FindUid(dataGridProducts, "CheckboxHeader");
        //    CheckBox checkBox = element as CheckBox;

        //    if (dataGridProducts.SelectedItems.Count > 0 && dataGridProducts.SelectedItems.Count < dataGridProducts.Items.Count)
        //        checkBox.IsChecked = null;

        //    if (dataGridProducts.SelectedItems.Count > 0 && dataGridProducts.SelectedItems.Count == dataGridProducts.Items.Count)
        //        checkBox.IsChecked = true;

        //    if (dataGridProducts.SelectedItems.Count == 0)
        //        checkBox.IsChecked = false;

        //    e.AddedItems
        //        .Cast<ProductModel>()
        //        .ToList()
        //        .ForEach((product) =>
        //        {
        //            bool isExist = selectedProducts.Any(x => x.Id.Equals(product.Id));
        //            if (!isExist)
        //                selectedProducts.Add(product);
        //        });

        //    if (isPageChanged) return;

        //    e.RemovedItems
        //        .Cast<ProductModel>()
        //        .ToList()
        //        .ForEach((product) =>
        //        {
        //            bool isExist = selectedProducts.Any(x => x.Id.Equals(product.Id));
        //            if (isExist)
        //                selectedProducts.RemoveAll(x => x.Id.Equals(product.Id));
        //        });

        //    SelectionSummaryUC.SelectedValue = selectedProducts.Count;
        //}

        //private void OnSelectionRemoved()
        //{
        //    dataGridProducts.SelectedItems.Clear();
        //    selectedProducts.Clear();
        //}

        //private void Textfield_OnTextChange(string text)
        //{
        //    debounceDispatcher.Debounce(300, (param) =>
        //    {
        //        queryModel.SearchText = text;
        //        queryModel.PageNo = 1;
        //        if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        //    });
        //}
    }
}
