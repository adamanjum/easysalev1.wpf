﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Models;
using EasySale.WPF.Helper;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using EasySale.WPF.Comman;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddCategoryPage.xaml
    /// </summary>
    public partial class AddCategoryPage : Page
    {
        /**
         *@ dependency properties 
        **/


        public Visibility BackButtonVisibility
        {
            get { return (Visibility)GetValue(BackButtonVisibilityProperty); }
            set { SetValue(BackButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty BackButtonVisibilityProperty =
            DependencyProperty.Register("BackButtonVisibility", typeof(Visibility), typeof(AddCategoryPage), new PropertyMetadata(Visibility.Visible));


        public Visibility CancelButtonVisibility
        {
            get { return (Visibility)GetValue(CancelButtonVisibilityProperty); }
            set { SetValue(CancelButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CancelButtonVisibilityProperty =
            DependencyProperty.Register("CancelButtonVisibility", typeof(Visibility), typeof(AddCategoryPage), new PropertyMetadata(Visibility.Collapsed));



        /**
         * @ events
        **/

        public event Action<AddCategoryPage> OnCancel;
        public event Action<CategoryModel, AddCategoryPage> OnSave;


        private PathHelper path = null;


        /**
         * @ constructors
         **/

        public AddCategoryPage()
        {
            InitializeComponent();
            Category = new CategoryModel();
            DataContext = this;
        }


        public AddCategoryPage(CategoryModel category) : this()
        {
            this.Category = category;
            if(!string.IsNullOrWhiteSpace(category.ImagePath))
            {
                string path = ImageHelper.GetDirectoryPath("Category");
                img.Source = new BitmapImage(new Uri(path + category.ImagePath));
            }
        }

        public CategoryModel Category { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if(path != null)
                img.Source = new BitmapImage(new Uri(path.Path));
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(path != null)
                {
                    ImageHelper.Save("Category", path.Path, path.FileName);
                    Category.ImagePath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Category.Name))
                    throw new Exception("Name is required");

                var savedCategory = await CategoryDAL.GetByName(Category.Name);
                if (savedCategory != null && !savedCategory.Id.Equals(Category.Id))
                    throw new Exception("Category of same name already exist");

                if(Category.Id == ObjectId.Empty)
                {
                    Category.Addedby = Session.User.Id;
                    Category.CreatedAt = DateTime.Now;
                    await CategoryDAL.Insert(Category);

                    // invoke save event
                    OnSave?.Invoke(Category, this);
                }
                else
                {
                    await CategoryDAL.Update(Category.Id, Category);
                }

                InformationalPopup.Show("Category", "Category has been saved successfully");
                NavigationService.Navigate(new Uri("Views/Pages/CategoryListPage.xaml", UriKind.Relative));

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Category", ex.Message);
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OnCancel?.Invoke(this);
        }
    }
}
