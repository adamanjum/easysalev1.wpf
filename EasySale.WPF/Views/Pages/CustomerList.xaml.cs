﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for CustomerList.xaml
    /// </summary>
    public partial class CustomerList : Page
    {
        private long count = 0;

        BackgroundWorker bw = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        private QueryModel queryModel = new QueryModel
        {
            NoOfRecord = 20,
            PageNo = 1,
            SearchText = ""
        };
        public CustomerList()
        {
            InitializeComponent();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);

            txtPage.Text = queryModel.PageNo.ToString();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            List<CustomerModel> customer = e.Result as List<CustomerModel>;

            int first = queryModel.NoOfRecord * (queryModel.PageNo - 1) + 1;
            int last = first + queryModel.NoOfRecord - 1;

            count = customer.Count;
            dataGridCustomers.ItemsSource = customer;

            txtfirst.Text = first.ToString();
            txtLast.Text = last.ToString();
            txtCount.Text = count.ToString();
            txtPage.Text = queryModel.PageNo.ToString();

            if (last > count)
                txtLast.Text = count.ToString();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            QueryModel query = e.Argument as QueryModel;
            e.Result = GetCustomer(query.PageNo, query.NoOfRecord, query.SearchText);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if ((queryModel.PageNo * queryModel.NoOfRecord) > count) return;
            queryModel.PageNo++;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (queryModel.PageNo == 1) return;
            queryModel.PageNo--;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        public List<CustomerModel> GetCustomer(int pageNo, int record, string text)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<CustomerModel>("Customer")
                .Find(x => x.Name.ToLower().Contains(text.ToLower()))
                .SortByDescending(x => x.CreatedAt)
                .Limit(record)
                .Skip(record * (pageNo - 1))
                .ToList();
        }

        private void BtnAddProduct_Click(object sender, RoutedEventArgs e)
            => NavigationService.Navigate(new Uri("Views/Pages/AddProductPage.xaml", UriKind.RelativeOrAbsolute));

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            queryModel.SearchText = txtSearch.Text;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("Customer", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            CustomerModel customer = button.DataContext as CustomerModel;
            MongoCRUD db = new MongoCRUD();
            db.DeleteRecord<CustomerModel>("Customer", customer.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CustomerModel customer = button.DataContext as CustomerModel;
            AddCustomerPopup customerPopup = new AddCustomerPopup(customer);
            customerPopup.Show();
        }
    }
}
