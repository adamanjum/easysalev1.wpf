﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for UsersListPage.xaml
    /// </summary>
    public partial class UsersListPage : Page
    {
        private long count = 0;

        BackgroundWorker bw = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        private QueryModel queryModel = new QueryModel
        {
            NoOfRecord = 20,
            PageNo = 1,
            SearchText = ""
        };
        public UsersListPage()
        {
            InitializeComponent();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);
            loader.Visibility = Visibility.Visible;

            txtPage.Text = queryModel.PageNo.ToString();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            List<UserModel> users = e.Result as List<UserModel>;

            int first = queryModel.NoOfRecord * (queryModel.PageNo - 1) + 1;
            int last = first + queryModel.NoOfRecord - 1;

            count = users.Count;
            dataGridUsers.ItemsSource = users;

            txtfirst.Text = first.ToString();
            txtLast.Text = last.ToString();
            txtCount.Text = count.ToString();
            txtPage.Text = queryModel.PageNo.ToString();

            if (last > count)
                txtLast.Text = count.ToString();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            QueryModel query = e.Argument as QueryModel;
            e.Result = GetUser(query.PageNo, query.NoOfRecord, query.SearchText);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if ((queryModel.PageNo * queryModel.NoOfRecord) > count) return;
            queryModel.PageNo++;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (queryModel.PageNo == 1) return;
            queryModel.PageNo--;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        public List<UserModel> GetUser(int pageNo, int record, string text)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<UserModel>("User")
                .Find(x => x.FirstName.ToLower().Contains(text.ToLower()))
                .SortByDescending(x => x.CreatedAt)
                .Limit(record)
                .Skip(record * (pageNo - 1))
                .ToList();
        }

        private void BtnAddProduct_Click(object sender, RoutedEventArgs e)
            => NavigationService.Navigate(new Uri("Views/Pages/AddProductPage.xaml", UriKind.RelativeOrAbsolute));

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            queryModel.SearchText = txtSearch.Text;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("User", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            UserModel user = button.DataContext as UserModel;
            MongoCRUD db = new MongoCRUD();
            db.DeleteRecord<SupplierModel>("User", user.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            UserModel user = button.DataContext as UserModel;
            this.NavigationService.Navigate(new AddUserPage(user));
        }
    }
}
