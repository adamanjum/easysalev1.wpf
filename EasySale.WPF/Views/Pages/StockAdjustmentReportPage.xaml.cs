﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.StockAdjustmentReport;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using FileHelpers;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for ProductReportPage.xaml
    /// </summary>
    public partial class StockAdjustmentReportPage : Page
    {
        private List<StockAdjustmentModel> selectedRecords = new List<StockAdjustmentModel>();
        private QueryModel queryModel = new QueryModel();
        private bool isPageChanged = false;
        private DebounceDispatcher debounceDispatcher = new DebounceDispatcher();
        private StockAdjustmentFilter stockAdjustmentFilter = new StockAdjustmentFilter();
        private StockAdjustmentReportData stockAdjustmentReportData = new StockAdjustmentReportData();

        public StockAdjustmentReportPage()
        {
            InitializeComponent();
            
            // filter
            stockAdjustmentFilter.onInitialized += stockAdjustmentFilter_onInitialized;
            stockAdjustmentFilter.Initialize();

            // report
            stockAdjustmentReportData.onInitialized += stockAdjustmentReportData_onInitialized;
            stockAdjustmentReportData.onInitializing += () => loader.Visibility = Visibility.Visible;
            queryModel.Filters.Add("apply-pagination", true);
            stockAdjustmentReportData.Initialize(queryModel);

            // pagination
            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.PageRange = queryModel.NoOfRecord;
        }

        private void stockAdjustmentFilter_onInitialized(QueryResultModel result)
        {
            cbxCategory.ItemsSource = result.Records["categories"] as List<CategoryModel>;
            cbxManufacturer.ItemsSource = result.Records["manufacturers"] as List<ManufacturerModel>;
        }

        private void stockAdjustmentReportData_onInitialized(QueryResultModel result)
        {
            loader.Visibility = Visibility.Collapsed;

            var count = result.Total;

            List<StockAdjustmentModel> stockAdjustmentReports
                = result.Records["StockAdjustmentRecords"] as List<StockAdjustmentModel>;

            dataGridProductReport.ItemsSource = stockAdjustmentReports;
            paginationUC.TotalRecords = result.Total;
            paginationUC.PageNo = queryModel.PageNo;

            dataGridProductReport
                 .Items
                 .Cast<StockAdjustmentModel>()
                 .ToList()
                 .ForEach((record) =>
                 {
                     bool isExist = selectedRecords.Any(x => x.Id.Equals(record.Id));
                     if (isExist)
                         dataGridProductReport.SelectedItems.Add(record);
                 });
        }

        public void OnRangeChanged (int pageRange)
        {
            queryModel.NoOfRecord = pageRange;
            queryModel.PageNo = 1;
            if (!stockAdjustmentReportData.IsBusy)
                stockAdjustmentReportData.Initialize(queryModel);
        }

        public void OnPageChanged (int pageNo)
        {
            queryModel.PageNo = pageNo;
            if (!stockAdjustmentReportData.IsBusy)
                stockAdjustmentReportData.Initialize(queryModel);

            isPageChanged = true;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            bdrDataGrid.Margin = new Thickness(5, bdrTitle.ActualHeight + 10, 10, 10);
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Page_Loaded(null, null);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            dataGridProductReport
                .Items
                .Cast<StockAdjustmentModel>()
                .ToList()
                .ForEach((product) => dataGridProductReport.SelectedItems.Add(product));
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
            => dataGridProductReport.SelectedItems.Clear();


        private void dataGridProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var element = UIElementHelper.FindUid(dataGridProductReport, "CheckboxHeader");
            CheckBox checkBox = element as CheckBox;

            if (dataGridProductReport.SelectedItems.Count > 0 && dataGridProductReport.SelectedItems.Count < dataGridProductReport.Items.Count)
                checkBox.IsChecked = null;

            if (dataGridProductReport.SelectedItems.Count > 0 && dataGridProductReport.SelectedItems.Count == dataGridProductReport.Items.Count)
                checkBox.IsChecked = true;

            if (dataGridProductReport.SelectedItems.Count == 0)
                checkBox.IsChecked = false;

            e.AddedItems
                .Cast<StockAdjustmentModel>()
                .ToList()
                .ForEach((recods) =>
                {
                    bool isExist = selectedRecords.Any(x => x.Id.Equals(recods.Id));
                    if (!isExist)
                        selectedRecords.Add(recods);
                });

            if (isPageChanged) return;

            e.RemovedItems
                .Cast<StockAdjustmentModel>()
                .ToList()
                .ForEach((product) =>
                {
                    bool isExist = selectedRecords.Any(x => x.Id.Equals(product.Id));
                    if (isExist)
                        selectedRecords.RemoveAll(x => x.Id.Equals(product.Id));
                });

            SelectionSummaryUC.SelectedValue = selectedRecords.Count;
        }

        private void OnSelectionRemoved()
        {
            dataGridProductReport.SelectedItems.Clear();
            selectedRecords.Clear();
        }

        private void datePickerFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.SetFilter("StartDate", datePickerFrom.SelectedDate);
            if (!stockAdjustmentReportData.IsBusy)
                stockAdjustmentReportData.Initialize(queryModel);
        }

        private void datePickerTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.SetFilter("EndDate", datePickerTo.SelectedDate?.AddHours(23).AddMinutes(59).AddSeconds(59));
            if (!stockAdjustmentReportData.IsBusy)
                stockAdjustmentReportData.Initialize(queryModel);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            debounceDispatcher.Debounce(300, (param) =>
            {
                if (!stockAdjustmentReportData.IsBusy)
                {
                    TextBox tb = (TextBox)sender;
                    string value = Regex.Escape(tb.Text);
                    queryModel.SetFilter("SearchText", value);

                    queryModel.PageNo = 1;
                    stockAdjustmentReportData.Initialize(queryModel);
                }
            });
        }

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!stockAdjustmentReportData.IsBusy)
            {
                ComboBox comboBox = (ComboBox)sender;
                var category = comboBox.SelectedItem as CategoryModel;

                queryModel.SetFilter("CategoryId", category?.Id);
                queryModel.PageNo = 1;

                stockAdjustmentReportData.Initialize(queryModel);
            }
        }

        private void cbxManufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!stockAdjustmentReportData.IsBusy)
            {
                ComboBox comboBox = (ComboBox)sender;
                var manufacturer = comboBox.SelectedItem as ManufacturerModel;

                queryModel.SetFilter("ManufacturerId", manufacturer?.Id);
                queryModel.PageNo = 1;

                stockAdjustmentReportData.Initialize(queryModel);
            }
        }
    }
}
