﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddExpensePage.xaml
    /// </summary>
    public partial class AddExpensePage : Page
    {
        private AttachmentHelper path = null;

        public AddExpensePage()
        {
            InitializeComponent();
            Expense = new ExpenseModel();
            DataContext = this;
        }

        public AddExpensePage(ExpenseModel expense) : this()
        {
            Expense = expense;
            if(Expense.Attachments != null && Expense.Attachments.Count != 0)
            {
                txtFilesName.Text = string.Empty;
                Expense.Attachments.ForEach(x => txtFilesName.Text += x + ", ");
            }
        }

        public ExpenseModel Expense { get; set; }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
            => e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));

        private void BtnSelectAttachments_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectAttachments();
            if (path != null)
            {
                txtFilesName.Text = string.Empty;
                path.FileNames.ToList().ForEach(x => txtFilesName.Text += x + ", ");
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.SaveAttachments("ExpenseAttachments", path.Paths, path.FileNames);
                    Expense.Attachments = path.FileNames.ToList();
                }

                if (Expense.Date == null)
                    throw new Exception("Please select date...!");

                if (string.IsNullOrWhiteSpace(Expense.Reference))
                    throw new Exception("Reference is required");

                if (Expense.Amount <= 0)
                    throw new Exception("Amount should be greater then zero");

                MongoCRUD db = new MongoCRUD();
                if (Expense.Id == ObjectId.Empty)
                {
                    Expense.AddedBy = Session.User.Id;
                    Expense.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync<ExpenseModel>("Expense", Expense);

                }
                else
                {
                    await db.UpsertRecordAsync("Expense", Expense.Id, Expense);
                }


                InformationalPopup.Show("Expense", "Expense has been saved successfully");
                this.NavigationService.Navigate(new ExpenseListPage());

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Expense", ex.Message);
            }
        }
    }
}
