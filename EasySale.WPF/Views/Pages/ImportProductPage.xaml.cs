﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.Models;
using EasySale.WPF.Helper;
using MongoDB.Driver;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System.Collections.ObjectModel;
using System.Data;
using FileHelpers;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddProductPage.xaml
    /// </summary>
    public partial class ImportProductPage : Page
    {
        PathHelper path = null;
        private List<ProductCSVModel> selectedProducts = new List<ProductCSVModel>();
        private bool isPageChanged = false;
        private List<ProductCSVModel> productCSVs;
        private ObservableCollection<ProductCSVModel> paginatedProducts = new ObservableCollection<ProductCSVModel>();
        public ImportProductPage()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectExcel();
            if (path == null) return;
            txtFilesName.Text = path.FileName;

            var engine = new FileHelperEngine<ProductCSVModel>();
            engine.Options.RemoveField("Id");
            productCSVs = engine.ReadFile(path.Path).ToList();

            //productCSVs.Take(200)
            //    .ToList()
            //    .ForEach((product) =>
            //        paginatedProducts.Add(product));

            //dataGridProducts.ItemsSource = paginatedProducts;

        }

        private async void btnImport_Click(object sender, RoutedEventArgs e)
        {
            loader.Visibility = Visibility.Visible;
            List<ProductModel> dbProducts = new List<ProductModel>();
            int row = 1;
            try
            {
                if (path == null)
                    throw new Exception("Please select a CSV file");

                var engine = new FileHelperEngine<ProductCSVModel>();
                engine.Options.RemoveField("Id");
                var products = engine.ReadFile(path.Path);
                List<CategoryModel> categories = await CategoryDAL.GetAsync(null, null, null);
                List<ManufacturerModel> manufacturers = await ManufacturerDAL.Get(null, null, null);

                foreach (var product in products)
                {
                    ProductModel dbProduct = new ProductModel();

                    if (!string.IsNullOrWhiteSpace(product.Code))
                    {
                        int codeCount = products.Where(x => x.Code.ToLower() == product.Code.ToLower()).Count();
                        if (codeCount > 1)
                            throw new Exception($"Duplicate entery of Code({product.Code})");

                        var codeProduct = await ProductDAL.GetOneAsync(null, product.Code);
                        if (codeProduct != null)
                            throw new Exception($"Product of same Code({product.Code}) already exist");

                        dbProduct.Code = product.Code;
                    }

                    /////////////////////////////////////////////////
                    ////////////// MANUFACTURERE ////////////////////
                    /////////////////////////////////////////////////
                    if (!string.IsNullOrWhiteSpace(product.Manufacturer))
                    {
                        var manufacturer = manufacturers.FirstOrDefault(x => x.Name.ToLower() == product.Manufacturer.ToLower());
                        if (manufacturer == null)
                        {
                            await ManufacturerDAL.Insert(new ManufacturerModel
                            {
                                Name = product.Manufacturer,
                                CreatedAt = DateTime.Now,
                                Addedby = Session.User.Id
                            });

                            manufacturer = await ManufacturerDAL.GetByName(product.Manufacturer);
                            manufacturers.Add(manufacturer);
                        }

                        dbProduct.ManufacturerId = manufacturer.Id;
                    }

                    /////////////////////////////////////////////////
                    ///////////////// PRODUCT NAME //////////////////
                    /////////////////////////////////////////////////
                    if (string.IsNullOrWhiteSpace(product.Name))
                        throw new Exception("Name is required");

                    int nameCount = products.Where(x => x.Name.ToLower() == product.Name.ToLower() && x.Manufacturer == product.Manufacturer).Count();
                    if (nameCount > 1)
                        throw new Exception($"Duplicate entery of Name({product.Name})");

                    var manufacturerProduct = manufacturers.Find(x => x.Name == product.Manufacturer);
                    var NameProduct = await ProductDAL.GetOne(product.Name, manufacturerProduct?.Id);
                    
                    if (NameProduct != null)
                        throw new Exception($"Product of same Name({product.Name}) already exist");

                    dbProduct.Name = product.Name;

                    /////////////////////////////////////////////////
                    /////////////////// CATEGORY ////////////////////
                    /////////////////////////////////////////////////
                    if (!string.IsNullOrWhiteSpace(product.Category))
                    {
                        var category = categories.FirstOrDefault(x => x.Name.ToLower() == product.Category.ToLower());
                        if (category == null)
                        {
                            await CategoryDAL.Insert(new CategoryModel
                            {
                                Name = product.Category,
                                CreatedAt = DateTime.Now,
                                Addedby = Session.User.Id
                            });

                            category = await CategoryDAL.GetByName(product.Category);
                            categories.Add(category);
                        }

                        dbProduct.CategoryId = category.Id;
                        dbProduct.Addedby = Session.User.Id;
                    }


                    if (product.Cost.HasValue)
                        dbProduct.Cost = product.Cost;

                    if (product.Price < 0)
                        throw new Exception("Price must be greater then Zero");

                    dbProduct.Price = product.Price;

                    if (product.MinQty.HasValue)
                        dbProduct.MinQty = product.MinQty;

                    if (!string.IsNullOrWhiteSpace(product.Description))
                        dbProduct.Description = product.Description;

                    if (product.StockQty.HasValue)
                    {
                        dbProduct.Stock = new Stock
                        {
                            Qty = product.StockQty.Value,
                            Addedby = Session.User.Id
                        };
                    }

                    dbProduct.CreatedAt = DateTime.Now;
                    dbProducts.Add(dbProduct);
                    row++;
                }

                await ProductDAL.InsertBulk(dbProducts);
                InformationalPopup.Show("Import Product", "Products have been imported successfully");
                path = null;
                txtFilesName.Text = "No file chosen";
            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Import Products", ex.Message);
            }
            finally
            {
                dbProducts = new List<ProductModel>();
                row = 1;
            }
            loader.Visibility = Visibility.Collapsed;
        }

        private void btnGetSample_Click(object sender, RoutedEventArgs e)
        {
           ImageHelper.saveFile("sample_product");
        }

        private void dataGridProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var element = UIElementHelper.FindUid(dataGridProducts, "CheckboxHeader");
            //CheckBox checkBox = element as CheckBox;

            //if (dataGridProducts.SelectedItems.Count > 0 && dataGridProducts.SelectedItems.Count < dataGridProducts.Items.Count)
            //    checkBox.IsChecked = null;

            //if (dataGridProducts.SelectedItems.Count > 0 && dataGridProducts.SelectedItems.Count == dataGridProducts.Items.Count)
            //    checkBox.IsChecked = true;

            //if (dataGridProducts.SelectedItems.Count == 0)
            //    checkBox.IsChecked = false;

            e.AddedItems
                .Cast<ProductCSVModel>()
                .ToList()
                .ForEach((product) =>
                {
                    bool isExist = selectedProducts.Any(x => x.Id == product.Id);
                    if (!isExist)
                        selectedProducts.Add(product);
                });

            if (isPageChanged) return;

            e.RemovedItems
                .Cast<ProductCSVModel>()
                .ToList()
                .ForEach((product) =>
                {
                    bool isExist = selectedProducts.Any(x => x.Id == product.Id);
                    if (isExist)
                        selectedProducts.RemoveAll(x => x.Id == product.Id);
                });
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            dataGridProducts
                .Items
                .Cast<ProductCSVModel>()
                .ToList()
                .ForEach((product) => dataGridProducts.SelectedItems.Add(product));
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
            => dataGridProducts.SelectedItems.Clear();

        private void dataGridProducts_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var product = (ProductCSVModel)e.Row.DataContext;
            Console.WriteLine(product.Name);
        }

        int page = 1;
        private void dataGridProducts_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Console.WriteLine(
                $"\n\r\n\r\n\r\n\rExtentHeight: {e.ExtentHeight.ToString()}\r\n" +
                $"ExtentHeightChange: {e.ExtentHeightChange.ToString()}\r\n" +
                $"VerticalChange: {e.VerticalChange}\r\n" +
                $"VerticalOffset: {e.VerticalOffset}\r\n" +
                $"ViewportHeight: {e.ViewportHeight}\r\n" +
                $"ViewportHeightChange: {e.ViewportHeightChange.ToString()}\n\r\n\r\n\r\n\r");

            double offset = (e.VerticalOffset / e.ExtentHeight ) * 100;
            int maxPage = Convert.ToInt32(Math.Ceiling(productCSVs == null ? 0 : productCSVs.Count / Convert.ToDouble(200)));

            if (offset > 70 && page < maxPage) 
            {
                int skip = 200 * (page - 1);
                productCSVs.Skip(skip)
                    .Take(200)
                    .ToList()
                    .ForEach((product) => paginatedProducts.Add(product));

                page += 1;
            }
        }

        private void dataGridProducts_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
