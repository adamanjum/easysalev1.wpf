﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignExtensions.Controls;
using System.Collections.ObjectModel;
using EasySale.WPF.Views.UserControls;
using EasySale.WPF.Models.AddPurchasePage;
using EasySale.WPF.Helper.ProductReport;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper.AddPurchase;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddPurchasesPage.xaml
    /// </summary>
    public partial class AddPurchasesPage : Page
    {
        private AttachmentHelper path = null;

        InitialData initialData = new InitialData();

        public AddPurchasesPage()
        {
            InitializeComponent();
           
            initialData.Initialize();

            Purchase = new PurchaseModel();
        }

        //public AddPurchasesPage(PurchaseModel purchase) : this()
        //{
        //    Purchase = purchase;
        //    dataGridProducts.ItemsSource = purchase.PurchaseItems;
        //    if (Purchase.SupplierId != ObjectId.Empty)
        //    {
        //        cbxSuppliers.SelectedItem = cbxSuppliers.Items.Cast<SupplierModel>().FirstOrDefault(x => x.Id.Equals(Purchase.SupplierId));
        //    }
        //}

        public PurchaseModel Purchase { get; set; }

        private void BtnSelectAttachments_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectAttachments();
            if (path != null)
            {
                txtFilesName.Text = string.Empty;
                path.FileNames.ToList().ForEach(x => txtFilesName.Text += x + ", ");
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (path != null)
            //    {
            //        ImageHelper.SaveAttachments("PurchaseAttachments", path.Paths, path.FileNames);
            //        Purchase.Attachments = path.FileNames.ToList();
            //    }

            //    if (Purchase.Date == null)
            //        throw new Exception("Please select date...!");

            //    if (string.IsNullOrWhiteSpace(Purchase.Ref))
            //        throw new Exception("Reference is required");

            //    List<PurchaseItemsGridModel> purchaseGridItems = dataGridProducts.Items.Cast<PurchaseItemsGridModel>().ToList();

            //    Purchase.PurchaseItems = PurchaseUtils.FormatPurchaseItems(purchaseGridItems);

            //    if (Purchase.PurchaseItems == null || Purchase.PurchaseItems.Count == 0)
            //        throw new Exception("Add purchase items");


            //    if (cbxSuppliers.SelectedItem != null)
            //        Purchase.SupplierId = ((SupplierModel)cbxSuppliers.SelectedItem).Id;

            //    MongoCRUD db = new MongoCRUD();
            //    if (Purchase.Id == ObjectId.Empty)
            //    {
            //        Purchase.Addedby = Session.User.Id;
            //        Purchase.CreatedAt = DateTime.Now;

            //        // insert new purchase record
            //        await PurchaseDAL.InsertAsync(Purchase);

            //        // bulk update product stock
            //        await ProductDAL.BulkUpdateAsync(PurchaseUtils.FormatProductStockBulkUpdate(purchaseGridItems));
            //    }
            //    else
            //    {
            //        await db.UpsertRecordAsync("Purchase", Purchase.Id, Purchase);
            //    }

            //    GlobalEvent.OnOpenDialoge("Purchase", "Purchases has been saved successfully");
            //    this.NavigationService.Navigate(new PurchasesListPage());

            //}
            //catch (Exception ex)
            //{
            //    GlobalEvent.OnOpenDialoge("Purchase", ex.Message);
            //}
        }

        private void AddToPurchaseItems(ProductModel product)
        {
            //List<PurchaseItemsGridModel> products = dataGridProducts.Items.Cast<PurchaseItemsGridModel>().ToList();

            //PurchaseItemsGridModel pi = products.FirstOrDefault(x => x.ProductId.Equals(product.Id));
            //if (pi != null)
            //{
            //    pi.Qty++;
            //    //dataGridProducts.SelectedItem = pi;
            //}
            //else
            //{
            //    dataGridProducts.Items.Insert(0, new PurchaseItemsGridModel
            //    {
            //        Code = product.Code,
            //        ProductId = product.Id,
            //        Name = product.Name,
            //        Qty = 1,
            //        Cost = product.Cost,
            //        Category = product.Category,
            //        Manufacturer = product.Manufacturer,
            //        Stock = product.Stock
            //    });
            //    //dataGridProducts.SelectedIndex = 0;
            //}
            //dataGridProducts.Items.Refresh();
        }

        private void autoCompleteTxt_OnSelected(object seletedProduct)
        {
            AddToPurchaseItems(seletedProduct as ProductModel);
        }

        private void dataGridProducts_PreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            autoCompleteTxt.IsEnabled = false;
        }

        private void dataGridProducts_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            autoCompleteTxt.IsEnabled = true;
        }

        private void dataGridProducts_KeyUp(object sender, KeyEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                //dg.CommitEdit();
                //dg.Items.Refresh();

                //if ((dg.CurrentColumn.Header as string) == "Cost")
                //{
                //    int currentColumnIndex = dg.Columns.IndexOf(dg.CurrentColumn);
                //    dg.CurrentCell = new DataGridCellInfo(dg.CurrentItem, dg.Columns[currentColumnIndex + 1]);
                //    dg.SelectedCells.Add(dg.CurrentCell);
                //}
                //else if ((dg.CurrentColumn.Header as string) == "Qty")
                //{
                //    int currentItemIndex = dg.Items.IndexOf(dg.CurrentItem);
                //    if (currentItemIndex < dg.Items.Count -1)
                //    {
                //        DataGridColumn costColumn = dg.Columns.ToList().Find(x => x.Header as string == "Cost");
                //        dg.CurrentCell = new DataGridCellInfo(dg.Items[currentItemIndex + 1], costColumn);
                //        dg.SelectedCells.Add(dg.CurrentCell);
                //    }

                //}
            }
        }

        private void dataGridProducts_CurrentCellChanged(object sender, EventArgs e)
        {
            //DataGrid dg = (DataGrid)sender;
            //try
            //{
                
            //    dg.Items.Refresh();
            //}
            //catch
            //{
            //    dg.CommitEdit();
            //    dg.Items.Refresh();
            //}
        }

        private void txtboxDgQty_Keydown(object sender, KeyEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            var pi = (PurchaseItemsGridModel)tb.DataContext;
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                pi.Qty = Convert.ToInt32(tb.Text);
                //dataGridProducts.Items.Refresh();
            }
        }


        private void txtboxDgQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidateQty((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void txtboxDgCost_Keydpwn(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Tab)
            {
                updateCost((TextBox)sender);
            }
        }

        private void updateCost(TextBox tb)
        {
            var pi = (PurchaseItemsGridModel)tb.DataContext;

            if (pi == null) return;

            if (string.IsNullOrEmpty(tb.Text))
            {
                pi.Cost = null;
            }
            else if (tb.Text == ".")
            {
                return;
            }
            else
            {
                pi.Cost = Convert.ToDouble(tb.Text);
            }
            //dataGridProducts.Items.Refresh();
        }

        private void txtboxDgCost_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !RegularExpressions.ValidatePrice((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void btnDgDelete_Click(object sender, RoutedEventArgs e)
        {
            PurchaseItemsGridModel purchaseItemGrid = (PurchaseItemsGridModel)((Button)sender).DataContext;
            if (purchaseItemGrid != null)
            {
                //dataGridProducts.Items.Remove(purchaseItemGrid);
                //dataGridProducts.Items.Refresh();
            }
        }

        private void cbDgUpdateCost_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            PurchaseItemsGridModel purchaseItemGrid = (PurchaseItemsGridModel)cb.DataContext;
            //purchaseItemGrid.updateCost = cb.IsChecked.Value;
            //dataGridProducts.Items.Refresh();
        }

        private void txtboxDgCost_LostFocus(object sender, RoutedEventArgs e)
        {
            updateCost((TextBox)sender);
        }

        private void txtboxDgQty_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            var pi = (PurchaseItemsGridModel)tb.DataContext;

            if (pi == null) return;
                
            pi.Qty = Convert.ToInt32(tb.Text);
            //dataGridProducts.Items.Refresh();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            autoCompleteTxt.DropdownMaxHeight = Convert.ToInt32(e.NewSize.Height - 430);
        }
    }
}
