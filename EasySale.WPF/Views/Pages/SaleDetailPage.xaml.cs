﻿using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for SaleDetailPage.xaml
    /// </summary>
    public partial class SaleDetailPage : UserControl
    {
        private CartModel sale;

        public SaleDetailPage(CartModel sale)
        {
            InitializeComponent();

            this.sale = sale;
            dataGridSales.ItemsSource = sale.CartItems;
            txtSubtotal.Text = sale.SubTotalWitoutDiscount.ToString("C", CultureHelper.CustomCulture());
            txtDiscount.Text = sale.Discount.ToString("C", CultureHelper.CustomCulture());
            txtAmount.Text = sale.Total.ToString("C", CultureHelper.CustomCulture());
            txtInvoice.Text = sale.InvoiceNo;
            txtDate.Text = sale.Date.ToString("dd-MM-yyyy HH:mm:ss");
            txtCount.Text = "(" + sale.TotalItems + ")" + " " + sale.CartItems.Count;

            if (sale.Status == (int)SaleStatus.Completed)
                txtStatus.Text = "Completed";
            else
                txtStatus.Text = "Hold";


            if (sale.CustomerId.HasValue && sale.CustomerId.Value != ObjectId.Empty)
            {
                txtCustomerName.Text = sale.CustomerName;

                GetCustomerAsync(sale.CustomerId.Value);
            }
        }

        public async void GetCustomerAsync(ObjectId id)
        {
            var db = ConnectionHelper.GetConnection();
            var customer = await db.GetCollection<CustomerModel>("Customer")
                .Find(x => x.Id.Equals(id))
                .FirstOrDefaultAsync();

            if (customer != null && !string.IsNullOrWhiteSpace(customer.ImgPath))
            {
                string path = ImageHelper.GetDirectoryPath("Customer") + customer.ImgPath;
                img.Fill = new ImageBrush(new BitmapImage(new Uri(path)));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new InvoiceHelper().DoPrintJob(sale, Session.Store, Session.InvoiceSetting);
        }
    }
}
