﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for SaleListPage.xaml
    /// </summary>
    public partial class ExpenseListPage : Page
    {
        private long count = 0;

        BackgroundWorker bw = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        private QueryModel queryModel = new QueryModel
        {
            NoOfRecord = 20,
            PageNo = 1,
            SearchText = ""
        };

        public ExpenseListPage()
        {
            InitializeComponent();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);
            loader.Visibility = Visibility.Visible;

            txtPage.Text = queryModel.PageNo.ToString();
            InitializeUser();
        }

        private async void InitializeUser()
        {
            var db = ConnectionHelper.GetConnection();
            var data = await db.GetCollection<UserModel>("User")
                .Find(new BsonDocument())
                .ToListAsync();

            data.ForEach(user => cbxUser.Items.Add(user));
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            List<ExpenseModel> users = e.Result as List<ExpenseModel>;

            int first = queryModel.NoOfRecord * (queryModel.PageNo - 1) + 1;
            int last = first + queryModel.NoOfRecord - 1;

            count = users.Count;
            dataGridExpenses.ItemsSource = users;

            txtfirst.Text = first.ToString();
            txtLast.Text = last.ToString();
            txtCount.Text = count.ToString();
            txtPage.Text = queryModel.PageNo.ToString();

            if (last > count)
                txtLast.Text = count.ToString();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            QueryModel query = e.Argument as QueryModel;
            e.Result = GetExpenses(query);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if ((queryModel.PageNo * queryModel.NoOfRecord) > count) return;
            queryModel.PageNo++;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

           
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (queryModel.PageNo == 1) return;
            queryModel.PageNo--;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        public List<ExpenseModel> GetExpenses(QueryModel queryModel)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ExpenseModel>.Filter.Empty;

            if (!string.IsNullOrWhiteSpace(queryModel.SearchText))
                filter &= Builders<ExpenseModel>.Filter.Regex("Reference", BsonRegularExpression.Create(new Regex(queryModel.SearchText, RegexOptions.IgnoreCase)));

            if (queryModel.Date.HasValue)
                filter &= Builders<ExpenseModel>.Filter.Gte("Date", queryModel.Date.Value.Date.Date);

            if(queryModel.DateTo.HasValue)
            {
                queryModel.DateTo = queryModel.DateTo.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
                filter &= Builders<ExpenseModel>.Filter.Lte("Date", queryModel.DateTo.Value);
            }


            if(queryModel.UserId.HasValue)
                filter &= Builders<ExpenseModel>.Filter.Eq(x => x.AddedBy, queryModel.UserId);


            var expenses = db.GetCollection<ExpenseModel>("Expense")
                .Find(filter)
                .SortByDescending(x => x.Date)
                .Limit(queryModel.NoOfRecord)
                .Skip(queryModel.NoOfRecord * (queryModel.PageNo - 1))
                .ToList();


            expenses.ForEach(expense =>
            {
                expense.User = db.GetCollection<UserModel>("User")
                .Find(x => x.Id.Equals(expense.AddedBy))
                .FirstOrDefault();
            });

            return expenses;
        }

        private void BtnAddProduct_Click(object sender, RoutedEventArgs e)
            => NavigationService.Navigate(new Uri("Views/Pages/AddProductPage.xaml", UriKind.RelativeOrAbsolute));

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            queryModel.SearchText = txtSearch.Text;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("Expense", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            ExpenseModel expense = button.DataContext as ExpenseModel;
            MongoCRUD db = new MongoCRUD();
            db.DeleteRecord<ExpenseModel>("Expense", expense.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.Date = datePicker.SelectedDate;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void DatePickerTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.DateTo = datePickerTo.SelectedDate;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            queryModel = new QueryModel
            {
                NoOfRecord = 20,
                PageNo = 1,
                SearchText = ""
            };
            txtSearch.Text = string.Empty;
            datePicker.SelectedDate = null;
            datePickerTo.SelectedDate = null;
            cbxUser.SelectedIndex = -1;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            ExpenseModel sale = (ExpenseModel)button.DataContext;
            this.NavigationService.Navigate(new AddExpensePage(sale));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            bdrDataGrid.Margin = new Thickness(5, bdrTitle.ActualHeight + 10, 10, 10);
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Page_Loaded(null, null);
        }

        private void CbxUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cbxUser.SelectedItem != null && !bw.IsBusy)
            {
                queryModel.UserId = ((UserModel)cbxUser.SelectedItem).Id;
                bw.RunWorkerAsync(queryModel);
            }
        }
    }
}
