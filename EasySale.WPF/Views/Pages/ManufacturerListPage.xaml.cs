﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using System.ComponentModel;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for ManufacturerListPage.xaml
    /// </summary>
    public partial class ManufacturerListPage : Page
    {
        private long count = 0;
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        private DebounceDispatcher debounceDispatcher = new DebounceDispatcher();
        private QueryModel queryModel = new QueryModel
        {
            PageNo = 1,
            NoOfRecord = 20
        };

        public ManufacturerListPage()
        {
            InitializeComponent();
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);
            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.OnPageChanged += (int pageNo) =>
            {
                queryModel.PageNo = pageNo;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            };

            paginationUC.OnRangeChanged += (int pageRange) =>
            {
                queryModel.NoOfRecord = pageRange;
                queryModel.PageNo = 1;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            };
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            new AddManufacturerPopup().ShowDialog();
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // loader.Visibility = Visibility.Collapsed;

            var result = e.Result as ManufacturerResultModel;
            count = result.Count;
            List<ManufacturerModel> manufacturers = result.Manufacturers;

            dataGridManufacturer.ItemsSource = manufacturers;

            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.TotalRecords = count;
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        { 
            var query = e.Argument as QueryModel;
            int skip = query.NoOfRecord * (query.PageNo - 1);

            var count = ManufacturerDAL.Count(query.SearchText);
            var manufacturers = ManufacturerDAL.Get(query.SearchText, skip, query.NoOfRecord);

            Task.WaitAll(count, manufacturers);

            e.Result = new ManufacturerResultModel
            {
                Count = count.Result,
                Manufacturers = manufacturers.Result
            };
        }

        private async void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("Manufacturer", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            ManufacturerModel manufacturer = button.DataContext as ManufacturerModel;
            await ManufacturerDAL.Delete(manufacturer.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            ManufacturerModel manufacturer = button.DataContext as ManufacturerModel;
            new AddManufacturerPopup(manufacturer).Show();
        }

        private void Textfield_OnTextChange(string text)
        {
            debounceDispatcher.Debounce(300, (param) =>
            {
                queryModel.SearchText = text;
                queryModel.PageNo = 1;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            });
        }
    }

    public class ManufacturerResultModel
    {
        public long Count { get; set; }
        public List<ManufacturerModel> Manufacturers { get; set; }
    }
}
