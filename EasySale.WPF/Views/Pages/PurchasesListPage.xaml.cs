﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for PurchasesListPage.xaml
    /// </summary>
    public partial class PurchasesListPage : Page
    {
        private long count = 0;

        BackgroundWorker bw = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        private QueryModel queryModel = new QueryModel
        {
            NoOfRecord = 20,
            PageNo = 1,
            SearchText = ""
        };

        public PurchasesListPage()
        {
            InitializeComponent();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);
            loader.Visibility = Visibility.Visible;

            txtPage.Text = queryModel.PageNo.ToString();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;

            List<PurchaseModel> users = e.Result as List<PurchaseModel>;

            int first = queryModel.NoOfRecord * (queryModel.PageNo - 1) + 1;
            int last = first + queryModel.NoOfRecord - 1;

            count = users.Count;
            dataGridPurchase.ItemsSource = users;

            txtfirst.Text = first.ToString();
            txtLast.Text = last.ToString();
            txtCount.Text = count.ToString();
            txtPage.Text = queryModel.PageNo.ToString();

            if (last > count)
                txtLast.Text = count.ToString();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            QueryModel query = e.Argument as QueryModel;
            e.Result = GetPurchase(query);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if ((queryModel.PageNo * queryModel.NoOfRecord) > count) return;
            queryModel.PageNo++;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (queryModel.PageNo == 1) return;
            queryModel.PageNo--;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        public List<PurchaseModel> GetPurchase(QueryModel queryModel)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<PurchaseModel>.Filter.Empty;
            if (!string.IsNullOrWhiteSpace(queryModel.SearchText))
                filter &= Builders<PurchaseModel>.Filter.Regex("Ref", new Regex(queryModel.SearchText));
            if (queryModel.Date.HasValue)
                filter &= Builders<PurchaseModel>.Filter.Eq("Date", queryModel.Date.Value.Date.Date);
            if (queryModel.Status != null &&  queryModel.Status.Value != -1)
                filter &= Builders<PurchaseModel>.Filter.Eq(x => x.Status, queryModel.Status);
            

            var purchases = db.GetCollection<PurchaseModel>("Purchase")
                .Find(filter)
                .SortByDescending(x => x.CreatedAt)
                .Limit(queryModel.NoOfRecord)
                .Skip(queryModel.NoOfRecord * (queryModel.PageNo - 1))
                .ToList();

            purchases.ForEach(purchase =>
            {
                if(purchase.SupplierId != ObjectId.Empty)
                {
                    purchase.Supplier = db.GetCollection<SupplierModel>("Supplier")
                    .Find(x => x.Id.Equals(purchase.SupplierId))
                    .FirstOrDefault();
                }
            });

            return purchases;
        }

        private void BtnAddProduct_Click(object sender, RoutedEventArgs e)
            => NavigationService.Navigate(new Uri("Views/Pages/AddProductPage.xaml", UriKind.RelativeOrAbsolute));

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            queryModel.SearchText = txtSearch.Text;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("Purchase", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            PurchaseModel purchase = button.DataContext as PurchaseModel;
            MongoCRUD db = new MongoCRUD();
            db.DeleteRecord<PurchaseModel>("Purchase", purchase.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            PurchaseModel purchase = button.DataContext as PurchaseModel;
            //this.NavigationService.Navigate(new AddPurchasesPage(purchase));
        }

        private void CbxStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.Status = cbxStatus.SelectedIndex;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.Date = datePicker.SelectedDate;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            queryModel = new QueryModel
            {
                NoOfRecord = 20,
                PageNo = 1,
                SearchText = ""
            };
            txtSearch.Text = string.Empty;
            datePicker.SelectedDate = null;
            cbxStatus.SelectedIndex = -1;
            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
        }

        private void BtnDetails_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            PurchaseModel purchase = (PurchaseModel)button.DataContext;
            this.NavigationService.Navigate(new PurchaseDetailPage(purchase));
        }
    }
}
