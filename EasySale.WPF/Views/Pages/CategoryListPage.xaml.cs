﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using System.ComponentModel;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for CategoryListPage.xaml
    /// </summary>
    public partial class CategoryListPage : Page
    {
        private long count = 0;
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        private QueryModel queryModel = new QueryModel { PageNo = 1, NoOfRecord = 20 };
        private DebounceDispatcher debounceDispatcher = new DebounceDispatcher();

        public CategoryListPage()
        {
            InitializeComponent();
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
            bw.RunWorkerAsync(queryModel);

            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.OnPageChanged += (int pageNo) =>
            {
                queryModel.PageNo = pageNo;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            };

            paginationUC.OnRangeChanged += (int pageRange) =>
            {
                queryModel.NoOfRecord = pageRange;
                queryModel.PageNo = 1;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            };
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Views/Pages/AddCategoryPage.xaml", UriKind.Relative));
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var result = e.Result as CategoryResultModel;
            count = result.Count;
            List<CategoryModel> categories = result.Categories;

            dataGridCategory.ItemsSource = categories;

            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.TotalRecords = count;
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var query = e.Argument as QueryModel;
            int skip = query.NoOfRecord * (query.PageNo - 1);

            var count = CategoryDAL.Count(query.SearchText);
            var categories = CategoryDAL.GetAsync(query.SearchText, skip, query.NoOfRecord);

            Task.WaitAll(count, categories);

            e.Result = new CategoryResultModel
            {
                Count = count.Result,
                Categories = categories.Result
            };
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            bool isDelete = ConformationalPopup.Show("Category", "Do you want to delete..?", Window.GetWindow(this));
            if (!isDelete) return;

            Button button = (Button)sender;
            CategoryModel category = button.DataContext as CategoryModel;
            MongoCRUD db = new MongoCRUD();
            db.DeleteRecord<CategoryModel>("Category", category.Id);

            if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);


        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            CategoryModel category = button.DataContext as CategoryModel;
            this.NavigationService.Navigate(new AddCategoryPage(category));
        }

        private void Textfield_OnTextChange(string text)
        {
            debounceDispatcher.Debounce(300, (param) =>
            {
                queryModel.SearchText = text;
                queryModel.PageNo = 1;
                if (!bw.IsBusy) bw.RunWorkerAsync(queryModel);
            });
        }
    }

    public class CategoryResultModel
    {
        public long Count { get; set; }
        public List<CategoryModel> Categories { get; set; }
    }
}
