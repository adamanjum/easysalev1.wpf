﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for PurchaseDetailPage.xaml
    /// </summary>
    public partial class PurchaseDetailPage : Page
    {
        public PurchaseDetailPage(PurchaseModel purchase)
        {
            InitializeComponent();

            dataGridPurchaseItems.ItemsSource = purchase.PurchaseItems;
           // txtAmount.Text = purchase.Amount.ToString("C");
            txtRef.Text = purchase.Ref;
            txtDate.Text = purchase.Date.ToLongDateString();
            txtCount.Text = "(" + purchase.TotalQty + ")" + " " + purchase.PurchaseItems.Count;

            if (purchase.Status == (int)PurchaseStatus.Received)
                txtStatus.Text = "Received";
            else
                txtStatus.Text = "Not Received";


            if(purchase.Supplier != null)
            {
                txtSupplierName.Text = purchase.Supplier.Name;
                if(!string.IsNullOrWhiteSpace(purchase.Supplier.ImgPath))
                {
                    string path = ImageHelper.GetDirectoryPath("Supplier") + purchase.Supplier.ImgPath;
                    //img.Fill = new ImageBrush(new BitmapImage(new Uri(path)));
                }
            }
        }
    }
}
