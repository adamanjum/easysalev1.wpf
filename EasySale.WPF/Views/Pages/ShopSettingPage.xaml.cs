﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using MongoDB.Driver;
using System.Threading;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddUserPage.xaml
    /// </summary>
    public partial class ShopSettingPage : Page
    {
        private PathHelper path = null;
        BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };

        public ShopSettingPage()
        {
            //InitializeComponent();
            Store = new StoreModel();
            DataContext = this;

            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Store = (StoreModel)e.Result;
            InitializeComponent();

            if (Store.Logo != null)
            {
                string path = ImageHelper.GetDirectoryPath("Store");
                try
                {
                   img.Fill = new ImageBrush(new BitmapImage(new Uri(path + Store.Logo)));
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var db = ConnectionHelper.GetConnection();
            var store = db.GetCollection<StoreModel>("Store")
                .Find(Builders<StoreModel>.Filter.Empty)
                .FirstOrDefault();
            e.Result = store;
        }

        public StoreModel Store { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
            {
                img.Fill = new ImageBrush(new BitmapImage(new Uri(path.Path)));
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("Store", path.Path, path.FileName);
                    Store.Logo = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(Store.Name))
                    throw new Exception("Name is required");

                if (!string.IsNullOrWhiteSpace(Store.Email) && !RegularExpressions.ValidateEmail(Store.Email))
                    throw new Exception("Invalid email address..");


                MongoCRUD db = new MongoCRUD();
                if (Store.Id == ObjectId.Empty)
                {
                    Store.Addedby = Session.User.Id;
                    Store.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync("Store", Store);
                }
                else
                {
                    await db.UpsertRecordAsync("Store", Store.Id, Store);
                }
                Session.Store = Store;

                InformationalPopup.Show("Store", "Store has been saved successfully");

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Store", ex.Message);
            }
        }

    }
}
