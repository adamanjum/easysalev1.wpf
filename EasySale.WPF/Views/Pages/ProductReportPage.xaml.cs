﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.DAL;
using EasySale.WPF.Helper;
using EasySale.WPF.Helper.ProductReport;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using FileHelpers;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for ProductReportPage.xaml
    /// </summary>
    public partial class ProductReportPage : Page
    {
        private List<ProductReportModel> selectedRecords = new List<ProductReportModel>();
        private QueryModel queryModel = new QueryModel();
        private bool isPageChanged = false;
        private DebounceDispatcher debounceDispatcher = new DebounceDispatcher();
        private ProductReportFilter productReportFilter = new ProductReportFilter();
        private ProductReportData productReportData = new ProductReportData();

        public ProductReportPage()
        {
            InitializeComponent();
            
            // filter
            productReportFilter.onInitialized += ProductReportFilter_onInitialized;
            productReportFilter.Initialize();

            // report
            productReportData.onInitialized += ProductReportData_onInitialized;
            productReportData.onInitializing += () => loader.Visibility = Visibility.Visible;
            queryModel.Filters.Add("apply-pagination", true);
            productReportData.Initialize(queryModel);

            // pagination
            paginationUC.PageNo = queryModel.PageNo;
            paginationUC.PageRange = queryModel.NoOfRecord;
        }

        private void ProductReportFilter_onInitialized(QueryResultModel result)
        {
            cbxCategory.ItemsSource = result.Records["categories"] as List<CategoryModel>;
            cbxManufacturer.ItemsSource = result.Records["manufacturers"] as List<ManufacturerModel>;
        }

        private void ProductReportData_onInitialized(QueryResultModel result)
        {
            loader.Visibility = Visibility.Collapsed;

            var count = result.Total;

            List<ProductReportModel> productReports = result.Records["ProductRecords"] as List<ProductReportModel>;

            dataGridProductReport.ItemsSource = productReports;
            paginationUC.TotalRecords = result.Total;
            paginationUC.PageNo = queryModel.PageNo;

            dataGridProductReport
                 .Items
                 .Cast<ProductReportModel>()
                 .ToList()
                 .ForEach((record) =>
                 {
                     bool isExist = selectedRecords.Any(x => x.Id.Equals(record.Id));
                     if (isExist)
                         dataGridProductReport.SelectedItems.Add(record);
                 });
        }

        public void OnRangeChanged (int pageRange)
        {
            queryModel.NoOfRecord = pageRange;
            queryModel.PageNo = 1;
            if (!productReportData.IsBusy)
                productReportData.Initialize(queryModel);
        }

        public void OnPageChanged (int pageNo)
        {
            queryModel.PageNo = pageNo;
            if (!productReportData.IsBusy)
                productReportData.Initialize(queryModel);

            isPageChanged = true;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            bdrDataGrid.Margin = new Thickness(5, bdrTitle.ActualHeight + 10, 10, 10);
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Page_Loaded(null, null);
        }

        private async void btnExport_Click(object sender, RoutedEventArgs e)
        {
            QueryModel query = (QueryModel)queryModel.Clone();
            query.SetFilter("apply-pagination", false);

            List<ProductReportModel> reports = await SaleDAL.GetProductReportsAsync(query);
            List<ProductReportCSVModel> csvReports = ProductReportUtils.MapProductCSV(reports);

            string[] columnsToExclude = new string[] { "Code", "TotalPrice" };
            string fileName = $"PRODUCTS_REPORT_{DateTime.Now.ToString("ddMMyyyyhhmmss")}";

            CSVHelper.ExportCSV(
                fileName,
                csvReports,
                columnsToExclude,
                (headers) => headers
                    .Replace("QuantitySold", "Quantity Sold")
                    .Replace("RemainingStock", "Remaining Stock"));
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            dataGridProductReport
                .Items
                .Cast<ProductReportModel>()
                .ToList()
                .ForEach((product) => dataGridProductReport.SelectedItems.Add(product));
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
            => dataGridProductReport.SelectedItems.Clear();


        private void dataGridProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var element = UIElementHelper.FindUid(dataGridProductReport, "CheckboxHeader");
            CheckBox checkBox = element as CheckBox;

            if (dataGridProductReport.SelectedItems.Count > 0 && dataGridProductReport.SelectedItems.Count < dataGridProductReport.Items.Count)
                checkBox.IsChecked = null;

            if (dataGridProductReport.SelectedItems.Count > 0 && dataGridProductReport.SelectedItems.Count == dataGridProductReport.Items.Count)
                checkBox.IsChecked = true;

            if (dataGridProductReport.SelectedItems.Count == 0)
                checkBox.IsChecked = false;

            e.AddedItems
                .Cast<ProductReportModel>()
                .ToList()
                .ForEach((recods) =>
                {
                    bool isExist = selectedRecords.Any(x => x.Id.Equals(recods.Id));
                    if (!isExist)
                        selectedRecords.Add(recods);
                });

            if (isPageChanged) return;

            e.RemovedItems
                .Cast<ProductReportModel>()
                .ToList()
                .ForEach((product) =>
                {
                    bool isExist = selectedRecords.Any(x => x.Id.Equals(product.Id));
                    if (isExist)
                        selectedRecords.RemoveAll(x => x.Id.Equals(product.Id));
                });

            SelectionSummaryUC.SelectedValue = selectedRecords.Count;
        }

        private void OnSelectionRemoved()
        {
            dataGridProductReport.SelectedItems.Clear();
            selectedRecords.Clear();
        }

        private void datePickerFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.SetFilter("StartDate", datePickerFrom.SelectedDate);
            if (!productReportData.IsBusy)
                productReportData.Initialize(queryModel);
        }

        private void datePickerTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            queryModel.SetFilter("EndDate", datePickerTo.SelectedDate);
            if (!productReportData.IsBusy)
                productReportData.Initialize(queryModel);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            debounceDispatcher.Debounce(300, (param) =>
            {
                if (!productReportData.IsBusy)
                {
                    TextBox tb = (TextBox)sender;
                    string value = Regex.Escape(tb.Text);
                    queryModel.SetFilter("SearchText", value);

                    queryModel.PageNo = 1;
                    productReportData.Initialize(queryModel);
                }
            });
        }

        private void cbxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!productReportData.IsBusy)
            {
                ComboBox comboBox = (ComboBox)sender;
                var category = comboBox.SelectedItem as CategoryModel;

                queryModel.SetFilter("CategoryId", category?.Id);
                queryModel.PageNo = 1;

                productReportData.Initialize(queryModel);
            }
        }

        private void cbxManufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!productReportData.IsBusy)
            {
                ComboBox comboBox = (ComboBox)sender;
                var manufacturer = comboBox.SelectedItem as ManufacturerModel;

                queryModel.SetFilter("ManufacturerId", manufacturer?.Id);
                queryModel.PageNo = 1;

                productReportData.Initialize(queryModel);
            }
        }
    }
}
