﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.UserControls;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();

            Session.Carts?.ForEach((cart) =>
            {
                tabControl.Items.Add(new TabItem
                {
                    Header = DateTime.Now.ToString("t"),
                    Content = new CartUC(cart)
                });
            });

            if (Session.Carts == null)
            {
                Session.Carts = new List<CartModel>();
                btnAddSaleTab_Click(null, null);
            }

            GlobalEvent.Checkout += (parameter) =>
            {
                int index = tabControl.SelectedIndex;
                tabControl.Items.RemoveAt(index);
                Session.Carts.RemoveAt(index);

                btnAddSaleTab_Click(null, null);
            };
        }

        private void btnAddSaleTab_Click(object sender, RoutedEventArgs e)
        {
            CartModel cart = new CartModel();
            tabControl.Items.Insert(0, new TabItem
            {
                Header = DateTime.Now.ToString("t"),
                Content = new CartUC(cart)
            });

            Session.Carts.Insert(0, cart);

            tabControl.SelectedIndex = 0;
        }

        private void btnCloseTab_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.Items.Count == 1) return;

            int index = tabControl.SelectedIndex;
            tabControl.Items.RemoveAt(index);
            Session.Carts.RemoveAt(index);
        }
    }
}
