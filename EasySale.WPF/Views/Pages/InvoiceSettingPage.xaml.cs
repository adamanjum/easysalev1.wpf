﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using MongoDB.Driver;
using System.Threading;
using EasySale.WPF.DAL;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddUserPage.xaml
    /// </summary>
    public partial class InvoiceSettingPage : Page
    {
        BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        InvoiceHelper invoice = new InvoiceHelper();
        public InvoiceSetting InvoiceSettings { get; set; }

        CartModel cart = new CartModel
        {
            CartItems = new List<CartItemModel>
            {
                new CartItemModel
                {
                    Name = "Very very looooong product name",
                    Qty = 5,
                    Price = 60,
                    Note = "Cart item note"
                },
                new CartItemModel
                {
                    Name = "test products",
                    Qty = 5,
                    Price = 60,
                    Note = "Cart item note"
                },
            },
            Note= "Test not",
            Date = DateTime.Now,
            Paying = 1000,
            CustomerName = "Adam"
        };

        public InvoiceSettingPage()
        {
            InitializeComponent();
            InvoiceSettings = new InvoiceSetting();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            loader.Visibility = Visibility.Visible;
            bw.RunWorkerAsync();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            loader.Visibility = Visibility.Collapsed;
            InvoiceSettings = (InvoiceSetting)e.Result;
            if(InvoiceSettings != null)
            {
                DataContext = this;
                flowInvoive.Document = invoice.GetFlowDocumentAsync(cart, Session.Store, InvoiceSettings);
            }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
            => e.Result = InvoiceSettingDAL.GetByStore(Session.Store.Id);


        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InvoiceSettings.Id == ObjectId.Empty)
                {
                    InvoiceSettings.Addedby = Session.User.Id;
                    InvoiceSettings.CreatedAt = DateTime.Now;
                    InvoiceSettings.StoreId = Session.Store.Id;
                    InvoiceSettingDAL.Insert(InvoiceSettings);
                }
                else
                {
                    InvoiceSettingDAL.Update(InvoiceSettings.Id, InvoiceSettings);
                }
                Session.InvoiceSetting = InvoiceSettings;

                InformationalPopup.Show("Invoice Settings", "Invoice settings has been saved successfully");

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("Invoice Setting", ex.Message);
            }
        }

        private void updateSamplePreview(InvoiceSetting invoiceSettings)
            => flowInvoive.Document = invoice.GetFlowDocumentAsync(cart, Session.Store, invoiceSettings);

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
            => updateSamplePreview(InvoiceSettings);


        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
            => updateSamplePreview(InvoiceSettings);

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
            => updateSamplePreview(InvoiceSettings);
    }
}
