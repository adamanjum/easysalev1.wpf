﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using EasySale.WPF.Views.Popups;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EasySale.WPF.DAL;
using System.ComponentModel;

namespace EasySale.WPF.Views.Pages
{
    /// <summary>
    /// Interaction logic for AddUserPage.xaml
    /// </summary>
    public partial class AddUserPage : Page
    {
        private PathHelper path = null;
        BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };

        public AddUserPage()
        {
            InitializeComponent();
            User = new UserModel();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            DataContext = this;
            bw.RunWorkerAsync();
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var roles = (List<RoleModel>)e.Result;
            cbxRole.ItemsSource = roles;

            var currentRole = roles.Find(role => role.Id.Equals(User.RoleId));
            cbxRole.SelectedItem = currentRole;
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = RoleDAL.Get();
        }

        public AddUserPage(UserModel user) : this()
        {
            User = user;
            if (user.ImgPath != null)
            {
                try
                {
                    string path = ImageHelper.GetDirectoryPath("User") + user.ImgPath;
                    var image = new BitmapImage(new Uri(path));
                    img.Fill = new ImageBrush(image);
                }
                catch (Exception ex)
                {
                    InformationalPopup.Show("Profile Image", ex.Message);
                }
            }
        }

        public UserModel User { get; set; }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            path = ImageHelper.SelectImage();
            if (path != null)
            {
                img.Fill = new ImageBrush(new BitmapImage(new Uri(path.Path)));
            }
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (path != null)
                {
                    ImageHelper.Save("User", path.Path, path.FileName);
                    User.ImgPath = path.FileName;
                }

                if (string.IsNullOrWhiteSpace(User.FirstName))
                    throw new Exception("First Name is required");

                if (string.IsNullOrWhiteSpace(User.LastName))
                    throw new Exception("Last Name is required");

                if (!string.IsNullOrWhiteSpace(User.Email) && !RegularExpressions.ValidateEmail(User.Email))
                    throw new Exception("Invalid email address..");

                if (User.Id == ObjectId.Empty && (string.IsNullOrWhiteSpace(pwd.Password) || string.IsNullOrWhiteSpace(confirmPwd.Password)))
                    throw new Exception("Passwod and confirm password are required");

                if (pwd.Password != confirmPwd.Password)
                    throw new Exception("Passwod and confirm password are required should be match");

                User.Password = pwd.Password == string.Empty ? User.Password : User.Password;

                MongoCRUD db = new MongoCRUD();
                if (User.Id == ObjectId.Empty)
                {
                    User.Addedby = Session.User.Id;
                    User.CreatedAt = DateTime.Now;
                    await db.InsertRecordAsync<UserModel>("User", User);
                }
                else
                {
                    await db.UpsertRecordAsync("User", User.Id, User);
                }

                InformationalPopup.Show("User", "Customer has been saved successfully");
                NavigationService.Navigate(new UsersListPage());

            }
            catch (Exception ex)
            {
                InformationalPopup.Show("User", ex.Message);
            }
        }

    }
}
