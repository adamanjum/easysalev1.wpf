﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using EasySale.WPF.ViewModels;
using EasySale.WPF.Views.Pages;
using Microsoft.Xaml.Behaviors;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EasySale.WPF.Behaviors
{
    public class AddPurchaseBehavior: Behavior<AddPurchasesPage>
    {
        NewPurchaseVM viewmodel;
        protected override void OnAttached()
        {
            AssociatedObject.Loaded += AssociatedObjectLoaded;
            viewmodel = AssociatedObject.DataContext as NewPurchaseVM;
        }

        private void AssociatedObjectLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            AssociatedObject.autoCompleteTxt.OnTextChanged += OnTextChanged;
            AssociatedObject.autoCompleteTxt.OnSelected += OnSelected;
        }

        private void OnSelected(object seletedProduct)
        {
            viewmodel.AddToPurchaseItems(seletedProduct as ProductModel);
            //AssociatedObject.datagrid.Focus();
            //Keyboard.Focus(AssociatedObject.datagrid);
            AssociatedObject.datagrid.SelectedIndex = 0;
            AssociatedObject.datagrid.SelectCell(AssociatedObject.datagrid.SelectedItem, AssociatedObject.datagrid.Columns["Qty"]);
        }

        private async void  OnTextChanged(string text)
        {
            List<ProductModel> products = await viewmodel.SearchProducts(text);

            products.ForEach(product =>
            {
                product.Category = viewmodel.Categories?.Find(category => category.Id.Equals(product.CategoryId));
                product.Manufacturer = viewmodel.Manufacturers?.Find(manufacturer => manufacturer.Id.Equals(product.ManufacturerId));
            });

            AssociatedObject.autoCompleteTxt.Items = products;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.Loaded -= AssociatedObjectLoaded;
            AssociatedObject.autoCompleteTxt.OnTextChanged -= OnTextChanged;
            AssociatedObject.autoCompleteTxt.OnSelected -= OnSelected;
        }
    }
}
