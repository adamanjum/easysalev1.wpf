﻿using EasySale.WPF.Models;
using EasySale.WPF.ViewModels;
using EasySale.WPF.Views.Pages;
using Microsoft.Xaml.Behaviors;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Behaviors
{
    public class RegisterReportBehavior: Behavior<RegisterReportPage>
    {
        RegisterReportVM viewmodel;
        protected override void OnAttached()
        {
            AssociatedObject.Loaded += AssociatedObjectLoaded;
            viewmodel = AssociatedObject.DataContext as RegisterReportVM;
            viewmodel.DataProvider.onInitialized += DataProviderOnInitialized;
        }

        private void AssociatedObjectLoaded(object sender, System.Windows.RoutedEventArgs e)
        {
            AssociatedObject.paginationUC.OnPageChanged += OnPageChanged;
            AssociatedObject.paginationUC.OnRangeChanged += OnRangeChanged;
        }


        private void OnRangeChanged(int range)
        {
          viewmodel.PageSize = range;
        }

        private void OnPageChanged(int pageNo)
        {
            viewmodel.PageNo = pageNo;
        }

        private void DataProviderOnInitialized(QueryResultModel result)
        {
            AssociatedObject.paginationUC.PageNo = viewmodel.PageNo;
            AssociatedObject.paginationUC.PageRange = viewmodel.PageSize;
            AssociatedObject.paginationUC.TotalRecords = result.Total;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.Loaded -= AssociatedObjectLoaded;
            AssociatedObject.paginationUC.OnPageChanged -= OnPageChanged;
            AssociatedObject.paginationUC.OnRangeChanged -= OnRangeChanged;
            viewmodel.DataProvider.onInitialized -= DataProviderOnInitialized;

        }
    }
}
