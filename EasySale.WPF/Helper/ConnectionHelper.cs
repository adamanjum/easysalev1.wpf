﻿using MongoDB.Driver;

namespace EasySale.WPF.Helper
{
    public static class ConnectionHelper
    {
        private static IMongoDatabase database;
        public static IMongoDatabase GetConnection(string databaseName = "EasySale")
        {
            if(database == null)
            {
                var client = new MongoClient();
                database = client.GetDatabase(databaseName);
            }
            return database;
        }
    }
}
