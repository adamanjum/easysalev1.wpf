﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EasySale.WPF.Helper
{
    public class RequiredValidater : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string valueToValidate = value as string;
            if(string.IsNullOrWhiteSpace(valueToValidate))
            {
                return new ValidationResult(false, "Required");
            }
            return new ValidationResult(true, null);
        }
    }

    public class PriceValidater : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string valueToValidate = value as string;
            try
            {
                decimal price = Convert.ToDecimal(valueToValidate);
            }
            catch
            {
                return new ValidationResult(false, "Inalid value");
            }
            return new ValidationResult(true, null);
        }
    }

    public class EmailValidater : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string valueToValidate = value as string;
            if(string.Empty == valueToValidate)
                return new ValidationResult(true, null);

            if (!RegularExpressions.ValidateEmail(valueToValidate))
                return new ValidationResult(false, "Inalid email address");

            return new ValidationResult(true, null);
        }
    }
}
