﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.RegisterReport
{
    public class RegisterReportFilter
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        public event Action<QueryResultModel> onInitialized;
        public RegisterReportFilter()
        {
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
        }


        public void Initialize ()
        {
            bw.RunWorkerAsync();
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var users = UserDAL.GetAllAsync(new QueryModel());

            Task.WaitAll(users);

            QueryResultModel result = new QueryResultModel { Records = new Dictionary<string, object>() };
            result.Records.Add("users", users.Result);

            e.Result = result;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }
    }
}
