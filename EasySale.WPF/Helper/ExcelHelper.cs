﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public static class ExcelHelper
    {
        public static DataTable ReadExcel(string filePath)
        {
            string connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={filePath};Extended Properties=Excel 12.0";
            DataTable dtexcel = new DataTable();
            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                try
                {
                    con.Open();
                    OleDbDataAdapter oleAdpt = new OleDbDataAdapter("select * from [Sheet1$]", con);
                    oleAdpt.Fill(dtexcel);
                }
                catch(Exception  ex)
                {
                    Console.WriteLine(ex);
                }
                finally
                {
                    if (con.State != ConnectionState.Closed)
                        con.Close();
                }
            }
            return dtexcel;
        }
    }
}
