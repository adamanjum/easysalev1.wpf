﻿using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public delegate void CommunicationHandler(object parameter);
    public static class GlobalEvent
    {
        public static event Func<object, object> SupplierSaved;
        public static void OnSupplierSaved(object parameter)
            => SupplierSaved?.Invoke(parameter);

        public static event CommunicationHandler Logout;
        public static void OnLogout(object parameter)
            => Logout?.Invoke(parameter);

        public static event CommunicationHandler Checkout;
        public static void OnCheckout(object parameter)
            => Checkout?.Invoke(parameter);

        public static event CommunicationHandler RegisterClosed;
        public static void OnRegisterClosed(object parameter)
            => RegisterClosed?.Invoke(parameter);

        public static event CommunicationHandler AddToCart;
        public static void OnAddToCart(object parameter)
            => AddToCart?.Invoke(parameter);

        public static event CommunicationHandler RegisterOpened;
        public static void OnRegisterOpened(object parameter)
            => RegisterOpened?.Invoke(parameter);

        public static event CommunicationHandler MinimizeWindow;
        public static void OnMinimizeWindow(object parameter)
            => MinimizeWindow?.Invoke(parameter);

        public static event Action<CustomerModel> AddNewCustomer;
        public static void OnAddNewCustomer(CustomerModel customer)
            => AddNewCustomer?.Invoke(customer);

        public static event Action<string, object> OpenDialoge;
        public static void OnOpenDialoge(string headerText, object bodyContent)
            => OpenDialoge?.Invoke(headerText, bodyContent);

        public static event Action<string, object, object> OpenDialogeWithCustomFooter;
        public static void OnOpenDialoge(string headerText, object bodyContent, object footerContent)
            => OpenDialogeWithCustomFooter?.Invoke(headerText, bodyContent, footerContent);
    }
}
