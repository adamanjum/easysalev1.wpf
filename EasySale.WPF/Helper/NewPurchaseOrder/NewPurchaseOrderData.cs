﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.NewPurchaseOrder
{
    public class NewPurchaseOrderData : BackgroundWorkerBase
    {
        internal override void BackgroundworkerDoWork(object sender, DoWorkEventArgs e)
        {
            var query = (QueryModel)e.Argument;
            var records = ProductDAL.GetAllAsync(query);

            Task.WaitAll(records);

            QueryResultModel result = new QueryResultModel();

            result.Records.Add("ProductRecords", records.Result);

            e.Result = result;
        }
    }
}
