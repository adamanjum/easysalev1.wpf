﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.Cart
{
    public class CartIntitialData
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        public event Action<QueryResultModel> onInitialized;
        public QueryResultModel Result { get; private set; } = new QueryResultModel();

        public CartIntitialData()
        {
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
        }


        public void Initialize()
        {
            bw.RunWorkerAsync();
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync();
            var manufacturers = ManufacturerDAL.GetAsync();
            var customers = CustomerDAL.GetAsync();

            Task.WaitAll(categories, manufacturers, customers);

            if(Result.Records.ContainsKey("categories"))
                Result.Records["categories"] = categories.Result;
            else
                Result.Records.Add("categories", categories.Result);

            if (Result.Records.ContainsKey("manufacturers"))
                Result.Records["manufacturers"] = manufacturers.Result;
            else
                Result.Records.Add("manufacturers", manufacturers.Result);

            if (Result.Records.ContainsKey("customers"))
                Result.Records["customers"] = customers.Result;
            else
                Result.Records.Add("customers", customers.Result);

            e.Result = Result;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }
    }
}
