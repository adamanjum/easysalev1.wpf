﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.ProductList
{
    public class ProductListData : BackgroundWorkerBase
    {
        internal override void BackgroundworkerDoWork(object sender, DoWorkEventArgs e)
        {
            var query = (QueryModel)e.Argument;
            var records = ProductDAL.GetAllAsync(query);
            var count = ProductDAL.GetCountAsync(query);

            Task.WaitAll(records, count);

            QueryResultModel result = new QueryResultModel();

            result.Records.Add("ProductRecords", records.Result);
            result.Total = count.Result;

            e.Result = result;
        }
    }
}
