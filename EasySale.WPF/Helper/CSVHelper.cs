﻿using EasySale.WPF.Models;
using FileHelpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public static class CSVHelper
    {
        public static void ExportCSV<T>(string fileName, List<T> data, string[] columnsToExclude, Func<string, string> formatHeader) where T : class
        {
            var dialog = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Title = "Select a Directory",
                Filter = "csv|*.csv",
                FileName = fileName
            };

            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;

                var engine = new FileHelperEngine<T>();
                var headers = engine.GetFileHeader().Split(',').ToList();

                if (columnsToExclude != null)
                {
                    columnsToExclude.ToList().ForEach((column) =>
                    {
                        headers.Remove(column);
                        engine.Options.RemoveField(column);
                    });
                }

                string headerText = string.Empty;
                headers.ForEach((header) => headerText += header + ",");
                engine.HeaderText = formatHeader(headerText);
                engine.WriteFile(path, data);
            }
        }

        public static List<ProductCSVModel> MapProductCSV(List<ProductModel> products, List<CategoryModel> categories, List<ManufacturerModel> manufacturers)
        {
            List<ProductCSVModel> productCSVs = new List<ProductCSVModel>();

            products.ForEach((product) =>
            {
                ProductCSVModel productCSV = new ProductCSVModel
                {
                    Id = product.Id.ToString(),
                    Code = product.Code,
                    Name = product.Name,
                    Price = product.Price,
                    Cost = product.Cost,
                    Description = product.Description,
                    MinQty = product.MinQty,
                    StockQty = product.Stock != null ? product.Stock.Qty : 0
                };

                var category = categories.FirstOrDefault(x => x.Id.Equals(product.CategoryId));
                var manufacturer = manufacturers.FirstOrDefault(x => x.Id.Equals(product.ManufacturerId));

                if (category != null)
                    productCSV.Category = category.Name;

                if (manufacturer != null)
                    productCSV.Manufacturer = manufacturer.Name;

                productCSVs.Add(productCSV);
            });

            return productCSVs;
        }
    }
}
