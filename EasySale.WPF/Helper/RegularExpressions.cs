﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public class RegularExpressions
    {
        public static bool ValidatePrice(string price)
        {
            Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$");
            return regex.IsMatch(price);
        }

        public static bool ValidateQty(string qty)
        {
            Regex regex = new Regex("^[0-9]+$");
            return regex.IsMatch(qty);
        }

        public static bool ValidateEmail(string email)
        {
            try
            {
                MailAddress mailAddress = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
