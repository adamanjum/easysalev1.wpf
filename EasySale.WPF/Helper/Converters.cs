﻿using EasySale.WPF.Comman;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using EasySale.WPF.Models;
using EasySale.WPF.DAL;
using System.Windows.Controls;

namespace EasySale.WPF.Helper
{
    public class StringToBool : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string data = value as string;
            if (string.IsNullOrWhiteSpace(data))
                return false;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SelectedItemToBool : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToCollapsed : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value as string == string.Empty)
                return "Collapsed";
            return "Visible";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ZeroToCollapsed : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || (int)value > 0)
                return "Collapsed";
            return "Visible";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ProductTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int type = (int)value;

            if (type == (int)ProductType.Combo)
                return Enum.GetName(typeof(ProductType), ProductType.Combo);
            if (type == (int)ProductType.Service)
                return Enum.GetName(typeof(ProductType), ProductType.Service);

            return Enum.GetName(typeof(ProductType), ProductType.Standard);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ImgPathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string path = value as string;
            string dir = parameter as string;
            if (string.IsNullOrWhiteSpace(path))
                return new Uri("Views/Icons/avatar-placeholder.png", UriKind.Relative);
            return ImageHelper.GetDirectoryPath(dir) + path;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GenderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? gender = value as int?;
            if (gender == (int)Gender.Male)
                return Enum.GetName(typeof(Gender), Gender.Male);
            return Enum.GetName(typeof(Gender), Gender.Female);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PurchaseStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? status = value as int?;
            if (status == (int)PurchaseStatus.Received)
                return Enum.GetName(typeof(PurchaseStatus), PurchaseStatus.Received);
            return "Not Received";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SaleStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? status = value as int?;
            if (status == (int)SaleStatus.Completed)
                return Enum.GetName(typeof(SaleStatus), SaleStatus.Completed);
            return Enum.GetName(typeof(SaleStatus), SaleStatus.Hold);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PaymentModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? status = value as int?;
            if (status == (int)PaymentMode.Card)
                return Enum.GetName(typeof(PaymentMode), PaymentMode.Card);
            return Enum.GetName(typeof(PaymentMode), PaymentMode.Cash);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InverseValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        { 
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ResourceToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var role = (RoleModel)value;
            string resourse = (string)parameter;
            bool isExist = role.Resourses.Any(x => x.Name == resourse);
            return isExist ? "Visible" : "Collapsed";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToNA : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value == null || value as string == string.Empty) ? "N/A" : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HeaderCheckboxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selectedItems = value as List<ProductModel>;

            // int totalItems = (int)parameter;
            if (selectedItems == null || selectedItems.Count == 0)
                return false;
            else
                return true;
            //if (selectedItems < totalItems)
            //    return null;

            //return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SentenceCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = value as string;

            return Utils.ToSentenceCase(text);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IndexConverter : IValueConverter
    {
        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            var lbi = (ListBoxItem)value;
            var listBox = (ListBox)parameter;
            var index = listBox.ItemContainerGenerator.IndexFromContainer(lbi);
            // One based. Remove +1 for Zero based array.
            return index + 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MinStockConverter : IValueConverter
    {
        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            ProductModel product = (ProductModel)value;
            if (product.Stock == null)
                return false;

            if (product.MinQty == null && product.Stock.Qty <= 0)
                return true;

            if (product.MinQty != null && product.Stock.Qty <= product.MinQty)
                return true;

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class QtyMultiplierConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int type = (int)value;

            if (type == (int)QtyMultiplier.Cost)
                return Enum.GetName(typeof(QtyMultiplier), QtyMultiplier.Cost);


            return Enum.GetName(typeof(QtyMultiplier), QtyMultiplier.Price);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)(QtyMultiplier)Enum.Parse(typeof(QtyMultiplier), (string)value);
        }
    }

    public class DiscountTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int type = (int)value;

            if (type == (int)DiscountType.Flate)
                return Enum.GetName(typeof(DiscountType), DiscountType.Flate);


            return Enum.GetName(typeof(DiscountType), DiscountType.Percentage);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)(DiscountType)Enum.Parse(typeof(DiscountType), (string)value);
        }
    }
}
