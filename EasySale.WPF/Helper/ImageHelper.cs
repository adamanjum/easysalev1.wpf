﻿using FileHelpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public class ImageHelper
    {
        public static string GetDirectoryPath(string directory)
        {
            return Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\" + directory + @"\";
        }


        public static PathHelper SelectImage()
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Title = "Select an image";
            of.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            bool? rst = of.ShowDialog();
            if (rst == true)
            {
                string iName = of.SafeFileName;
                string filePath = of.FileName;

                return new PathHelper
                {
                    FileName = iName,
                    Path = filePath
                };
            }
            return null;
        }

        public static AttachmentHelper SelectAttachments()
        {
            OpenFileDialog of = new OpenFileDialog
            {
                Multiselect = true,
                Title = "Select Attachments",
                Filter = "Image Files(*.jpg; *.jpeg; *.txt; *.docx)|*.jpg; *.jpeg; *.gif; *.bmp"
            };

            bool? rst = of.ShowDialog();
            if (rst == true)
            {
                string[] iName = of.SafeFileNames;
                string[] filePath = of.FileNames;

                return new AttachmentHelper
                {
                    FileNames = iName,
                    Paths = filePath
                };
            }
            return null;
        }

        public static PathHelper SelectExcel()
        {
            OpenFileDialog of = new OpenFileDialog
            {
                Multiselect = true,
                Title = "Select Attachments",
                Filter = "CSV File(*.csv)|*.csv"
            };

            bool? rst = of.ShowDialog();
            if (rst == true)
            {
                string iName = of.SafeFileName;
                string filePath = of.FileName;

                return new PathHelper
                {
                    FileName = iName,
                    Path = filePath
                };
            }
            return null;
        }

        public static string Save(string directory, string filePath, string fileName)
        {
            string appPath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\" + directory + @"\";

            if (!Directory.Exists(appPath))
                Directory.CreateDirectory(appPath);

            if (!File.Exists(appPath + fileName))
                File.Copy(filePath, appPath + fileName);

            return appPath + fileName;
        }

        public static string[] SaveAttachments(string directory, string[] filePaths, string[] fileNames)
        {
            string appPath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\" + directory + @"\";

            if (!Directory.Exists(appPath))
                Directory.CreateDirectory(appPath);

            List<string> paths = new List<string>();

            for (int i = 0; i < fileNames.Length; i++)
            {
                if (!File.Exists(appPath + fileNames[i]))
                    File.Copy(filePaths[i], appPath + fileNames[i]);
                paths.Add(appPath + fileNames[i]);
            }

            return paths.ToArray();
        }

        public static void saveFile(string fileName)
        {
            var dialog = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Title = "Select a Directory",
                Filter = "csv|*.csv",
                FileName = fileName
            };

            if (dialog.ShowDialog() == true)
            {
                string appPath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + @"\Assets";
                string path = dialog.FileName;
                try
                {
                    File.Copy($"{appPath}/ProductSample.csv", path);
                }
                finally
                {
                    /// todo later 
                }
            }
        }
    }

    public class PathHelper
    {
        public string FileName { get; set; }
        public string NewPath { get; set; }
        public string Path { get; set; }
    }

    public class AttachmentHelper
    {
        public string[] FileNames { get; set; }
        public string[] Paths { get; set; }
    }
}
