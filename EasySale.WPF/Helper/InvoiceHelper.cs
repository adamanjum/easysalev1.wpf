﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.IO;
using System.Threading;
using EasySale.WPF.Models;
using System.Printing;
using EasySale.WPF.Comman;
using EasySale.WPF.Views.Popups;

namespace EasySale.WPF.Helper
{
    public class InvoiceHelper
    {
        public FlowDocument GetFlowDocumentAsync(CartModel order, StoreModel storeSetting, InvoiceSetting invoiceSetting)
        {
            double pageWidth = 288;
            string fontFamily = "Verdana";
            int fontSize = 10;

            FlowDocument flowDocument = new FlowDocument
            {
                PageWidth = pageWidth,
                PagePadding = new Thickness(15),
                FontFamily = new FontFamily(fontFamily),
                FontSize = fontSize,
            };

            double printAreaX = pageWidth - (flowDocument.PagePadding.Left + flowDocument.PagePadding.Right);

            Paragraph p = new Paragraph();

            // ............ Logo ................
            if (invoiceSetting.ShowLogo)
            {
                try
                {
                    p.Inlines.Add(new Image
                    {
                        Height = 70,
                        Margin = new Thickness(-10, 0, 0, 3),
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Width = printAreaX,
                        Source = new BitmapImage(new Uri(ImageHelper.GetDirectoryPath("Store") + storeSetting.Logo))
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            // ............ store name ................
            p.Inlines.Add(new TextBlock
            {
                Text = storeSetting.Name,
                Width = printAreaX,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                FontSize = 16,
                FontWeight = FontWeights.Bold,
                TextWrapping = TextWrapping.WrapWithOverflow
            });


            // .............. store address ............
            if (invoiceSetting.ShowAddress)
            {
                p.Inlines.Add(new TextBlock
                {
                    Text = storeSetting.Address,
                    Width = printAreaX,
                    TextAlignment = TextAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    TextWrapping = TextWrapping.WrapWithOverflow
                });
            }

            // .............. store City ............
            if (invoiceSetting.ShowCity)
            {
                p.Inlines.Add(new TextBlock
                {
                    Text = "Lahore, Pakistan",
                    Width = printAreaX,
                    TextAlignment = TextAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center
                });
            }

            // .............. store Phone ............
            p.Inlines.Add(new TextBlock
            {
                Text = storeSetting.Phone,
                Width = printAreaX,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            });


            // .............. store Date ............
            p.Inlines.Add(new TextBlock
            {
                Text = order.Date.ToString("dd-MM-yyyy HH:mm:ss"),
                Width = printAreaX,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            });

            flowDocument.Blocks.Add(p);

            //---Info Section------
            Grid infoGrid = new Grid
            {
                Width = printAreaX,
                FlowDirection = FlowDirection.LeftToRight
            };

            if (invoiceSetting.ShowCustomerName || invoiceSetting.ShowCustomerContact)
            {
                infoGrid.Children.Add(new TextBlock
                {
                    Text = "Customer",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    TextAlignment = TextAlignment.Left,
                    Width = 120,
                    Height = 20
                });

                if (invoiceSetting.ShowCustomerName)
                {
                    infoGrid.Children.Add(new TextBlock
                    {
                        Text = string.IsNullOrWhiteSpace(order.CustomerName) ? "Walking Customer" : order.CustomerName,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Height = 20,
                        Width = 120,
                        Margin = new Thickness(0, 25, 0, 0)
                    });
                }

                if (!string.IsNullOrWhiteSpace(order.CustomerName) && invoiceSetting.ShowCustomerContact)
                {
                    infoGrid.Children.Add(new TextBlock
                    {
                        Text = "03314874251",
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Height = 20,
                        Width = 120,
                        Margin = new Thickness(0, 55, 0, 0)
                    });
                }
            }

            if (invoiceSetting.ShowUserId || invoiceSetting.ShowUserName)
            {
                infoGrid.Children.Add(new TextBlock
                {
                    Text = "Sale's person",
                    HorizontalAlignment = HorizontalAlignment.Right,
                    TextAlignment = TextAlignment.Right,
                    Width = 120,
                    Height = 20,
                    Margin = new Thickness(0, 0, 0, 0)
                });

                if(invoiceSetting.ShowUserName)
                {
                    infoGrid.Children.Add(new TextBlock
                    {
                        Text = Session.User.FirstName + " " + Session.User.LastName,
                        HorizontalAlignment = HorizontalAlignment.Right,
                        TextAlignment = TextAlignment.Right,
                        Height = 20,
                        Width = 120,
                        Margin = new Thickness(0, 30, 0, 0)
                    });
                }
            }


            p.Inlines.Add(infoGrid);
            flowDocument.Blocks.Add(p);

            //---Order Details

            StackPanel headingRow = new StackPanel
            {
                Width = printAreaX
            };

            headingRow.Children.Add(new TextBlock
            {
                Text = "Item Name",
                FontWeight = FontWeights.Bold,
                HorizontalAlignment = HorizontalAlignment.Left
            }); ;

            StackPanel headingRow1 = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };

            headingRow1.Children.Add(new TextBlock
            {
                Text = "Qty",
                Width = 50,
                FontWeight = FontWeights.Bold,
                HorizontalAlignment = HorizontalAlignment.Left
            });

            headingRow1.Children.Add(new TextBlock
            {
                Text = "Price",
                Width = 70,
                FontWeight = FontWeights.Bold,
                HorizontalAlignment = HorizontalAlignment.Left
            });

            headingRow1.Children.Add(new TextBlock
            {
                Text = "Discount",
                Width = 60,
                FontWeight = FontWeights.Bold,
                HorizontalAlignment = HorizontalAlignment.Left
            });

            headingRow1.Children.Add(new TextBlock
            {
                Text = "Total",
                Width = 70,
                FontWeight = FontWeights.Bold,
                TextAlignment = TextAlignment.Right,
                HorizontalAlignment = HorizontalAlignment.Right
            });

            headingRow.Children.Add(headingRow1);

            Border headerBorder = new Border
            {
                BorderBrush = Brushes.Black,
                BorderThickness = new Thickness(1),
                Child = headingRow,
                Padding = new Thickness(2),
                CornerRadius = new CornerRadius(3)
            };

            p.Inlines.Add(headerBorder);
            List<CartItemModel> items = order.CartItems;
            foreach (var item in items)
            {
                StackPanel productDetailsStack = new StackPanel
                {
                    Width = printAreaX,
                    // line height
                    Margin = new Thickness(0, 5, 0, 0)
                };

                productDetailsStack.Children.Add(new TextBlock
                {
                    Text = Utils.ToSentenceCase(item.Name),
                    Width = printAreaX,
                    HorizontalAlignment = HorizontalAlignment.Left
                });

                Grid lineDetailsGrid = new Grid();
                StackPanel lineStack = new StackPanel
                {
                    Orientation = Orientation.Horizontal
                };

                lineStack.Children.Add(new TextBlock
                {
                    Text = item.Qty.ToString(),
                    Width = 50,
                    HorizontalAlignment = HorizontalAlignment.Left
                });

                lineStack.Children.Add(new TextBlock
                {
                    Text = item.Price.ToString(),
                    Width = 70,
                    HorizontalAlignment = HorizontalAlignment.Left
                });

                lineStack.Children.Add(new TextBlock
                {
                    Text = item.DiscountAmount.ToString(),
                    Width = 60,
                    HorizontalAlignment = HorizontalAlignment.Left
                });

                lineStack.Children.Add(new TextBlock
                {
                    Text = item.DiscountedLineTotal.ToString(),
                    Width = 70,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    TextAlignment = TextAlignment.Right
                });

                productDetailsStack.Children.Add(lineStack);

                if (!string.IsNullOrWhiteSpace(item.Note) && invoiceSetting.ShowItemNote)
                {
                    lineDetailsGrid.Children.Add(new TextBlock()
                    {
                        Text = "\"" + item.Note + "\"",
                        Width = pageWidth - 20,
                        FontStyle = FontStyles.Italic,
                        TextWrapping = TextWrapping.Wrap,
                        FontSize = 10,
                        Margin = new Thickness(10, 0, 10, 0)
                    });
                }

                lineDetailsGrid.Children.Add(new Separator
                {
                    VerticalAlignment = VerticalAlignment.Bottom,
                    Margin = new Thickness(0, 10, 0, 0)
                });

                productDetailsStack.Children.Add(lineDetailsGrid);
                p.Inlines.Add(productDetailsStack);
            }

            Grid billSummery = new Grid
            {
                Width = pageWidth - flowDocument.PagePadding.Left - flowDocument.PagePadding.Right,
                Margin = new Thickness(0, -20, 0, 0)
            };

            billSummery.Children.Add(new TextBlock
            {
                Text = "Sub Total",
                HorizontalAlignment = HorizontalAlignment.Left,
                TextAlignment = TextAlignment.Left,
                Width = 120,
                Height = 20
            });

            billSummery.Children.Add(new TextBlock
            {
                Text = "Discount",
                HorizontalAlignment = HorizontalAlignment.Left,
                Height = 20,
                Width = 120,
                Margin = new Thickness(0, 30, 0, 0)
            });

            billSummery.Children.Add(new TextBlock
            {
                Text = "Total",
                HorizontalAlignment = HorizontalAlignment.Left,
                Height = 20,
                Width = 120,
                FontWeight = FontWeights.Bold,
                Margin = new Thickness(0, 55, 0, 0),
                FontSize = 14,
            });


            billSummery.Children.Add(new TextBlock
            {
                Text = order.SubTotalWitoutDiscount.ToString("C", CultureHelper.CustomCulture()),
                HorizontalAlignment = HorizontalAlignment.Right,
                TextAlignment = TextAlignment.Right,
                Width = 120,
                Height = 20
            });

            billSummery.Children.Add(new TextBlock
            {
                Text = order.Discount.ToString("C", CultureHelper.CustomCulture()),
                HorizontalAlignment = HorizontalAlignment.Right,
                TextAlignment = TextAlignment.Right,
                Height = 20,
                Width = 120,
                FontSize = fontSize,
                Margin = new Thickness(0, 30, 0, 0)
            });

            billSummery.Children.Add(new TextBlock
            {
                Text = order.Total.ToString("C", CultureHelper.CustomCulture()),
                HorizontalAlignment = HorizontalAlignment.Right,
                TextAlignment = TextAlignment.Right,
                Height = 20,
                Width = 120,
                FontSize = 14,
                Margin = new Thickness(0, 55, 0, 0),
                FontWeight = FontWeights.Bold
            });

            p.Inlines.Add(billSummery);

            p.Inlines.Add(Line(pageWidth, new Thickness(0)));

            if(invoiceSetting.ShowCustomerBillSummary)
            {
                Grid cutomerSummery = new Grid
                {
                    Width = pageWidth - flowDocument.PagePadding.Left - flowDocument.PagePadding.Right,
                    Margin = new Thickness(0, -20, 0, 0),
                };

                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = "Paid",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    TextAlignment = TextAlignment.Left,
                    Width = 120,
                    Height = 20
                });

                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = "Balance",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Height = 20,
                    Width = 120,
                    Margin = new Thickness(0, 30, 0, 0)
                });

                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = "Payment Mode",
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Height = 20,
                    Width = 120,
                    FontWeight = FontWeights.Bold,
                    Margin = new Thickness(0, 55, 0, 0)
                });


                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = order.Paying.ToString("C", CultureHelper.CustomCulture()),
                    HorizontalAlignment = HorizontalAlignment.Right,
                    TextAlignment = TextAlignment.Right,
                    Width = 120,
                    Height = 20
                });

                double customerBalance = order.Paying - order.Total;

                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = customerBalance.ToString("C", CultureHelper.CustomCulture()),
                    HorizontalAlignment = HorizontalAlignment.Right,
                    TextAlignment = TextAlignment.Right,
                    Height = 20,
                    Width = 120,
                    FontSize = 12,
                    Margin = new Thickness(0, 30, 0, 0)
                });

                cutomerSummery.Children.Add(new TextBlock
                {
                    Text = Enum.GetName(typeof(PaymentMode), order.PaymentMode),
                    HorizontalAlignment = HorizontalAlignment.Right,
                    TextAlignment = TextAlignment.Right,
                    Height = 20,
                    Width = 120,
                    Margin = new Thickness(0, 55, 0, 0),
                    FontWeight = FontWeights.Bold
                });

                p.Inlines.Add(cutomerSummery);

                p.Inlines.Add(Line(pageWidth, new Thickness(0)));
            }

            p.Inlines.Add(new TextBlock
            {
                Text = "Transection #",
                Width = pageWidth,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Margin = new Thickness(0, 5, 0, 0)
            });

            p.Inlines.Add(new TextBlock
            {
                Text = order.InvoiceNo?.ToString(),
                Width = pageWidth,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
            });

            if(!string.IsNullOrWhiteSpace(order.Note) && invoiceSetting.ShowOrderNote)
            {
                p.Inlines.Add(new TextBlock
                {
                    Text = "Order Note",
                    Width = pageWidth,
                    Margin = new Thickness(0, 15, 0, 0),
                    FontWeight = FontWeights.Bold,
                });

                p.Inlines.Add(new TextBlock()
                {
                    Text = order.Note,
                    Width = pageWidth - flowDocument.PagePadding.Left - flowDocument.PagePadding.Right,
                    TextWrapping = TextWrapping.Wrap
                });
            }

            if (invoiceSetting.ShowTermCondition)
            {
                p.Inlines.Add(new TextBlock
                {
                    Text = "Terms & Conditions",
                    Width = pageWidth,
                    Margin = new Thickness(0, 15, 0, 0),
                    FontWeight = FontWeights.Bold,
                    FontSize = 8
                });

                p.Inlines.Add(new TextBlock()
                {
                    Text = invoiceSetting.TermConditionText,
                    Width = pageWidth - flowDocument.PagePadding.Left - flowDocument.PagePadding.Right,
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8
                });
            }

            if (invoiceSetting.ShowThankYou)
            {
                p.Inlines.Add(new TextBlock
                {
                    Text = invoiceSetting.ThankYouText,
                    Width = pageWidth,
                    TextAlignment = TextAlignment.Center,
                    TextDecorations = TextDecorations.Underline,
                    FontStyle = FontStyles.Italic,
                    FontFamily = new FontFamily("times new roman"),
                    Margin = new Thickness(0, 15, 0, 20),
                    FontWeight = FontWeights.Bold,
                    FontSize = 10
                });
            }

            p.Inlines.Add(Line(pageWidth, new Thickness(0)));

            p.Inlines.Add(new TextBlock
            {
                FontSize = 10,
                Text = "Powered by EasySale | 0315-4288408 | 0331-4874251",
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center
            });

            return flowDocument;
        }

        public void Print(CartModel order, StoreModel storeSetting, InvoiceSetting invoiceSetting)
        {
            try
            {
                PrintServer ps = new PrintServer(@"\\" + Environment.MachineName);
                PrintQueue pq = ps.GetPrintQueue(Session.PrinterSettings.DefaultInvoicePrinter);

                PrintDialog print = new PrintDialog { PrintQueue = pq };
                print.PrintDocument(((IDocumentPaginatorSource)GetFlowDocumentAsync(order, storeSetting, invoiceSetting)).DocumentPaginator, "Order No." + order.Id);
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(() => InformationalPopup.Show("Invoice", ex.Message));
            }
        }

        public static Line Line(double pageWidth, Thickness margin)
        {
            Line line = new Line()
            {
                X1 = 0,
                X2 = pageWidth,
                Stroke = Brushes.Black,
                StrokeThickness = 3,
                StrokeDashArray = new DoubleCollection(2),
                StrokeDashOffset = 2,
                Margin = margin,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            return line;
        }

        public void DoPrintJob(CartModel order, StoreModel storeSetting, InvoiceSetting invoiceSetting)
        {
            Thread printThread = new Thread(() => Print(order, storeSetting, invoiceSetting)) { IsBackground = true };
            printThread.SetApartmentState(ApartmentState.STA);
            printThread.Start();
        }
    }
}
