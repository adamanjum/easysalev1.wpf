﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.PurchaseOrder
{
    public class PurchaseOrderFilter : BackgroundWorkerBase
    {
        internal override void BackgroundworkerDoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync();
            var manufacturers = ManufacturerDAL.GetAsync();

            Task.WaitAll(categories, manufacturers);

            QueryResultModel result = new QueryResultModel { Records = new Dictionary<string, object>() };

            result.Records.Add("categories", categories.Result);
            result.Records.Add("manufacturers", manufacturers.Result);

            e.Result = result;
        }
    }
}
