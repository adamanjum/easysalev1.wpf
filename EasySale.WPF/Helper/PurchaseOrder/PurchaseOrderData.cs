﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.PurchaseOrder
{
    public class PurchaseOrderData : BackgroundWorkerBase
    {
        internal override void BackgroundworkerDoWork(object sender, DoWorkEventArgs e)
        {
            var query = (QueryModel)e.Argument;
            var records = PurchaseOrderDAL.GetAllAsync(query);
            var count = PurchaseOrderDAL.GetCountAsync(query);

            Task.WaitAll(records, count);

            QueryResultModel result = new QueryResultModel();

            result.Records.Add("PurchaseOrdersRecords", records.Result);
            result.Total = count.Result;

            e.Result = result;
        }
    }
}
