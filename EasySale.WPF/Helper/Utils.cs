﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public static class Utils
    {
        public static string ToSentenceCase(string sentence)
        {
            // start by converting entire string to lower case
            string lowerCase = sentence.ToLower();
            // matches the first sentence of a string, as well as subsequent sentences
            var r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);
            // MatchEvaluator delegate defines replacement of setence starts to uppercase
            return r.Replace(lowerCase, s => s.Value.ToUpper());
        }

        public static string GetUID(string prefix = "")
        {

            DateTime historicalDate = new DateTime(1970, 1, 1, 0, 0, 0);
            TimeSpan spanTillNow = DateTime.UtcNow.Subtract(historicalDate);
            return String.Format("{0}{1:0}", prefix, spanTillNow.TotalSeconds);
        }
    }
}
