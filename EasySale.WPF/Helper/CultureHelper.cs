﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper
{
    public static class CultureHelper
    {
        public static CultureInfo CustomCulture(string code = "ur-PK")
        {
            return CultureInfo.CreateSpecificCulture(code);
        }
    }
}
