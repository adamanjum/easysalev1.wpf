﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.ProductReport
{
    public class ProductReportData
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        public event Action<QueryResultModel> onInitialized;
        public event Action onInitializing;
        public bool IsBusy { get => bw.IsBusy; }
        public ProductReportData()
        {
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
        }


        public void Initialize(QueryModel query)
        {
            onInitializing?.Invoke();
            bw.RunWorkerAsync(query);
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var query = (QueryModel)e.Argument;
            var records = SaleDAL.GetProductReportsAsync(query);
            var count = SaleDAL.GetProductReportsCountAsync(query);

            Task.WaitAll(records, count);

            QueryResultModel result = new QueryResultModel();

            result.Records.Add("ProductRecords", records.Result);
            result.Total = count.Result?.Total ?? 0;

            e.Result = result;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }
    }
}
