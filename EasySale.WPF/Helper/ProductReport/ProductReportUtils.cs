﻿using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.ProductReport
{
    public class ProductReportUtils
    {
        public static List<ProductReportCSVModel> MapProductCSV(List<ProductReportModel> reports)
        {
            List<ProductReportCSVModel> productCSVs = new List<ProductReportCSVModel>();

            reports.ForEach((report) =>
            {
                ProductReportCSVModel productCSV = new ProductReportCSVModel
                {
                    Code = report.Code,
                    Name = report.ProductName,
                    QuantitySold = report.QtySold,
                    TotalPrice = report.TotalPrice,
                    RemainingStock = report.RemainingStock,
                    Category = report.Category,
                    Manufacturer = report.Manufacturer
                };

                productCSVs.Add(productCSV);
            });

            return productCSVs;
        }
    }
}
