﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.StockAdjustmentReport
{
    public class StockAdjustmentFilter
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        public event Action<QueryResultModel> onInitialized;
        public StockAdjustmentFilter()
        {
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
        }


        public void Initialize ()
        {
            bw.RunWorkerAsync();
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync();
            var manufacturers = ManufacturerDAL.GetAsync();

            Task.WaitAll(categories, manufacturers);

            QueryResultModel result = new QueryResultModel { Records = new Dictionary<string, object>() };

            result.Records.Add("categories", categories.Result);
            result.Records.Add("manufacturers", manufacturers.Result);

            e.Result = result;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }
    }
}
