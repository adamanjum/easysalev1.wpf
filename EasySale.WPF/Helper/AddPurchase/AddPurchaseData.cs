﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.AddPurchase
{
    public class AddPurchaseData : BackgroundWorkerBase
    {
        internal override void BackgroundworkerDoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync();
            var manufacturers = ManufacturerDAL.GetAsync();
            var suppliers = SupplierDAL.GetAsync();

            Task.WaitAll(categories, manufacturers, suppliers);

            QueryResultModel result = new QueryResultModel();

            result.Records.Add("Categories", categories.Result);
            result.Records.Add("Manufacturers", manufacturers.Result);
            result.Records.Add("Suppliers", suppliers.Result);

            e.Result = result;
        }
    }
}
