﻿using EasySale.WPF.DAL;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.ProductReport
{
    public class InitialData
    {
        private BackgroundWorker bw = new BackgroundWorker { WorkerReportsProgress = true };
        public event Action<QueryResultModel> onInitialized;
        public QueryResultModel Result { get; private set; } = new QueryResultModel();

        public InitialData()
        {
            bw.DoWork += Backgroundworker_DoWork;
            bw.RunWorkerCompleted += Backgroundworker_RunWorkerCompleted;
        }


        public void Initialize ()
        {
            bw.RunWorkerAsync();
        }

        private void Backgroundworker_DoWork(object sender, DoWorkEventArgs e)
        {
            var categories = CategoryDAL.GetAsync();
            var manufacturers = ManufacturerDAL.GetAsync();
            var suppliers = SupplierDAL.GetAsync();

            Task.WaitAll(categories, manufacturers, suppliers);
            
            Result.Records.Add("categories", categories.Result);
            Result.Records.Add("manufacturers", manufacturers.Result);
            Result.Records.Add("suppliers", suppliers.Result);

            e.Result = Result;
        }

        private void Backgroundworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            onInitialized?.Invoke((QueryResultModel)e.Result);
        }
    }
}
