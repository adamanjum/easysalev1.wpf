﻿using EasySale.WPF.Comman;
using EasySale.WPF.Models;
using EasySale.WPF.Models.AddPurchasePage;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.Helper.AddPurchase
{
    public class PurchaseUtils
    {
        public static List<PurchaseItems> FormatPurchaseItems(List<PurchaseItemsGridModel> gridItems)
        {
            List<PurchaseItems> purchaseItems = new List<PurchaseItems>();
            foreach (var item in gridItems)
            {
                purchaseItems.Add(new PurchaseItems
                {
                    ProductId = item.ProductId,
                    Name = item.Name,
                    Cost = item.Cost,
                    Qty = item.Qty,
                    CategoryId = item.Product.Category?.Id,
                    CategoryName = item.Product.Category?.Name,
                    ManufacturerId = item.Product.Manufacturer?.Id,
                    ManufacturerName = item.Product.Manufacturer?.Name,
                    SelectedDiscountType = item.SelectedDiscountType,
                    SelectedMultiplier = item.SelectedMultiplier
                });
            }

            return purchaseItems;
        }

        public static List<WriteModel<ProductModel>> FormatProductStockBulkUpdate(List<PurchaseItemsGridModel> purchaseItems)
        {
            List<WriteModel<ProductModel>> listWrites = new List<WriteModel<ProductModel>>();
            DateTime stockUpdateTime = new DateTime();
            foreach (var item in purchaseItems)
            {
                // where filter
                var filterDefinition = Builders<ProductModel>.Filter.Eq(p => p.Id, item.ProductId);

                var update = Builders<ProductModel>.Update;
                var updates = new List<UpdateDefinition<ProductModel>>();

                int stockQty = item.Product.Stock == null ? 0 : item.Product.Stock.Qty;
                stockQty += item.Qty;

                updates.Add(update.Set("Stock", new Stock { Qty = stockQty, UpdatedBy = Session.User.Id, UpdatedAt = stockUpdateTime }));


                    if (item.Cost.HasValue)
                        updates.Add(update.Set(x => x.Cost, item.Cost.Value));
                    else
                        updates.Add(update.Unset(x => x.Cost));

                listWrites.Add(new UpdateManyModel<ProductModel>(filterDefinition, update.Combine(updates)));
            }
            return listWrites;
        }
    }
}
