﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public static class RoleDAL
    {
        private static readonly string collectionName = "Role";

        public static void Create(RoleModel role)
        {
            var db = ConnectionHelper.GetConnection();
            db.GetCollection<RoleModel>(collectionName)
                .InsertOne(role);
        }

        public static RoleModel RoleById(ObjectId id)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<RoleModel>
                .Filter
                .Eq(x => x.Id, id);

            return db.GetCollection<RoleModel>(collectionName)
                .Find(filter)
                .FirstOrDefault();
        }

        public static List<RoleModel> Get()
        {
            var db = ConnectionHelper.GetConnection();
            var filter = Builders<RoleModel>.Filter.Empty;

            return db.GetCollection<RoleModel>(collectionName)
                .Find(filter)
                .ToList();
        }

        public static void CreateDefault()
        {
            var adminResources = new List<Resourse>();
            var adminGrants = new List<string>();

            RoleModel.GrantsName
                .ForEach((name) => adminGrants.Add(name));
            RoleModel.ResourseName
                .ForEach((name) => adminResources.Add(new Resourse { Name = name, Grants = adminGrants }));

            var adminRole = new RoleModel
            {
                Name = "Admin",
                Resourses = adminResources,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };


            var staffGrants = new List<string>();

            RoleModel.GrantsName
                .ForEach((name) => staffGrants.Add(name));

            var staffResources = new List<Resourse>
            { 
                new Resourse { Name = "POS", Grants = staffGrants }
            };

            var staffRole = new RoleModel
            {
                Name = "Staff",
                Resourses = adminResources,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };

            var db = ConnectionHelper.GetConnection();
            db.GetCollection<RoleModel>(collectionName)
                .InsertOne(adminRole);
        }
    }
}
