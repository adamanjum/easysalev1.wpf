﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class PurchaseOrderDAL
    {
        private static readonly string collectionName = "PurchaseOrder";

        public static Task InsertAsync(PurchaseOrderModel purchaseOrder)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<PurchaseOrderModel>(collectionName)
                .InsertOneAsync(purchaseOrder);
        }

        public static Task<List<PurchaseOrderModel>> GetAllAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = ConstructFilters(query);

            return db.GetCollection<PurchaseOrderModel>(collectionName)
                .Find(filter)
                .SortByDescending(x => x.CreatedAt)
                .Skip(query.Skip)
                .Limit(query.Limit)
                .ToListAsync();
        }

        public static Task<long> GetCountAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = ConstructFilters(query);

            return db.GetCollection<PurchaseOrderModel>(collectionName)
                .Find(filter)
                .CountDocumentsAsync();
        }

        public static Task DeleteByIdAsync(ObjectId id)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<PurchaseOrderModel>(collectionName)
                .DeleteOneAsync(x => x.Id == id);
        }

        private static FilterDefinition<PurchaseOrderModel> ConstructFilters(QueryModel query)
        {
            var builder = Builders<PurchaseOrderModel>.Filter;

            var filter = builder.Empty;

            if (query.Filters.ContainsKey("Search"))
                filter &= builder.Regex(x => x.OrderNo, (string)query.Filters["Search"]);

            return filter;
        }
    }
}
