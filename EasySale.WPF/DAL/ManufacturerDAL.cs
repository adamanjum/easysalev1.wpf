﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class ManufacturerDAL
    {
        private static readonly string collectionName = "Manufacturer";

        public static Task<List<ManufacturerModel>> GetAsync()
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ManufacturerModel>(collectionName)
                .Find(Builders<ManufacturerModel>.Filter.Empty)
                .ToListAsync();
        }

        public static Task Insert(ManufacturerModel manufacturer)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<ManufacturerModel>(collectionName)
                .InsertOneAsync(manufacturer);
        }

        public static Task<ManufacturerModel> GetByName(string name)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ManufacturerModel>(collectionName)
                .FindSync(x => x.Name.ToLower() == name.ToLower())
                .FirstOrDefaultAsync();
        }

        public static Task Update(ObjectId id, ManufacturerModel manufacturer)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ManufacturerModel>(collectionName)
                .ReplaceOneAsync(new BsonDocument("_id", id), manufacturer, new UpdateOptions { IsUpsert = false });
        }

        public static Task<List<ManufacturerModel>> Get(string keyword, int? skip, int? limit)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ManufacturerModel>.Filter.Empty;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                var search = Builders<ManufacturerModel>.Filter.Regex("Name", BsonRegularExpression.Create(new Regex(keyword, RegexOptions.IgnoreCase)));
                filter &= search;
            }

            return db.GetCollection<ManufacturerModel>(collectionName)
                .Find(filter)
                .SortByDescending(x => x.CreatedAt)
                .Skip(skip)
                .Limit(limit)
                .ToListAsync();
        }

        public static Task<long> Count(string keyword)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ManufacturerModel>.Filter.Empty;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                var search = Builders<ManufacturerModel>.Filter.Regex("Name", BsonRegularExpression.Create(new Regex(keyword, RegexOptions.IgnoreCase)));
                filter &= search;
            }

            return db.GetCollection<ManufacturerModel>(collectionName)
                .Find(filter)
                .CountDocumentsAsync();
        }

        public static Task Delete(ObjectId id)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ManufacturerModel>.Filter.Eq(x => x.Id, id);

            return db.GetCollection<ManufacturerModel>(collectionName)
                .DeleteOneAsync(filter);
        }
    }
}
