﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public static class ExpenseDAL
    {
        private static readonly string collectionName = "Expense";

        public static List<ExpenseModel> GetExpenses(DateTime? from, DateTime? to, ObjectId? AddedBy, int? skip, int? limit)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ExpenseModel>.Filter.Empty;

            if (from.HasValue)
                filter &= Builders<ExpenseModel>.Filter.Gte("CreatedAt", from.Value);

            if (to.HasValue)
                filter &= Builders<ExpenseModel>.Filter.Lte("CreatedAt", to.Value);

            if (AddedBy.HasValue)
                filter &= Builders<ExpenseModel>.Filter.Eq("AddedBy", AddedBy);

            return db.GetCollection<ExpenseModel>(collectionName)
                .Find(filter)
                .Skip(skip)
                .Limit(limit)
                .ToList();
        }
    }
}
