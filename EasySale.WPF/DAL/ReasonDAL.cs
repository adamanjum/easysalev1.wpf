﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class ReasonDAL
    {
        private static readonly string collectionName = "Reason";

        public static Task InsertAsync(ReasonModel reason)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<ReasonModel>(collectionName)
                .InsertOneAsync(reason);
        }

        public static Task<ReasonModel> GetOneAsync(Dictionary<string, object> query)
        {
            var filter = Builders<ReasonModel>.Filter.Empty;

            if (query.ContainsKey("Title"))
            {
                var Title = Builders<ReasonModel>.Filter.Where(x => x.Title == (string)query["Title"]);
                filter &= Title;
            }

            if (query.ContainsKey("Type"))
            {
                filter &= Builders<ReasonModel>.Filter.Eq("Type", query["Type"]);
            }

            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<ReasonModel>(collectionName)
                .Find<ReasonModel>(filter)
                .FirstOrDefaultAsync();
        }

        public static Task<List<ReasonModel>> GetAllAsync(Dictionary<string, object> query)
        {
            var filter = Builders<ReasonModel>.Filter.Empty;

            if (query.ContainsKey("Type"))
            {
                filter &= Builders<ReasonModel>.Filter.Eq("Type", query["Type"]);
            }

            var db = ConnectionHelper.GetConnection();
            Task<List<ReasonModel>> task = db.GetCollection<ReasonModel>(collectionName)
                            .Find<ReasonModel>(filter)
                            .ToListAsync();
            return task;
        }

        public static Task DeleteOneAsync(Dictionary<string, object> query)
        {
            var filter = Builders<ReasonModel>.Filter.Empty;

            if (query.ContainsKey("Id"))
            {
                filter &= Builders<ReasonModel>.Filter.Where(x => x.Id == (ObjectId)query["Id"]);
            }

            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<ReasonModel>(collectionName)
                .DeleteOneAsync(filter);
        }
    }
}
