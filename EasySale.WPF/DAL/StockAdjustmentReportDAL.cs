﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class StockAdjustmentReportDAL
    {
        private static readonly string collectionName = "StockAdjustmentReport";

        public static Task InsertAsync(StockAdjustmentModel model)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<StockAdjustmentModel>(collectionName)
                .InsertOneAsync(model);
        }

        public static Task<ReasonModel> GetOneAsync(Dictionary<string, object> query)
        {
            var filter = Builders<ReasonModel>.Filter.Empty;

            if (query.ContainsKey("Title"))
            {
                var Title = Builders<ReasonModel>.Filter.Where(x => x.Title == (string)query["Title"]);
                filter &= Title;
            }

            if (query.ContainsKey("Type"))
            {
                filter &= Builders<ReasonModel>.Filter.Eq("Type", query["Type"]);
            }

            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<ReasonModel>(collectionName)
                .Find<ReasonModel>(filter)
                .FirstOrDefaultAsync();
        }

        public static Task<List<StockAdjustmentModel>> GetAllAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = ConstructFilters(query);

            Task<List<StockAdjustmentModel>> task = db.GetCollection<StockAdjustmentModel>(collectionName)
                            .Find(filter)
                            .SortByDescending(x => x.ChangedAt)
                            .Skip(query.Skip)
                            .Limit(query.Limit)
                            .ToListAsync();
            return task;
        }

        public static Task<long> GetCountAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = ConstructFilters(query);

            Task<long> task = db.GetCollection<StockAdjustmentModel>(collectionName)
                            .Find(filter)
                            .CountDocumentsAsync();

            return task;
        }

        private static FilterDefinition<StockAdjustmentModel> ConstructFilters(QueryModel query)
        {
            var filter = Builders<StockAdjustmentModel>.Filter.Empty;

            if (query.Filters.ContainsKey("SearchText"))
            {
                var regex = new Regex(((string)query.Filters["SearchText"]).Insert(0, "^").Replace(" ", ".+").Trim(), RegexOptions.IgnoreCase);
                var search = Builders<StockAdjustmentModel>.Filter
                    .Regex(x => x.ProductName, BsonRegularExpression.Create(regex));
                filter &= search;

            }

            if (query.Filters.ContainsKey("CategoryId"))
            {
                filter &= Builders<StockAdjustmentModel>.Filter.Eq(x => x.CategoryId, query.Filters["CategoryId"]);
            }

            if (query.Filters.ContainsKey("ManufacturerId"))
            {
                filter &= Builders<StockAdjustmentModel>.Filter.Eq(x => x.ManufacturerId, query.Filters["ManufacturerId"]);
            }

            if (query.Filters.ContainsKey("StartDate"))
            {
                filter &= Builders<StockAdjustmentModel>.Filter.Gte(x => x.ChangedAt, query.Filters["StartDate"]);
            }

            return filter;
        }
    }
}
