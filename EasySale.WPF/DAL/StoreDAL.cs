﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class StoreDAL
    {
        private static readonly string collectionName = "Store";

        public static StoreModel GetStore()
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<StoreModel>.Filter.Empty;

            return db.GetCollection<StoreModel>(collectionName)
                .Find(filter).FirstOrDefault();
        }
    }
}
