﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EasySale.WPF.DAL
{
    class InvoiceSettingDAL
    {
        private static readonly string collectionName = "InvoiceSetting";

        public static void Insert(InvoiceSetting invoiceSetting)
        {
            var db = ConnectionHelper.GetConnection();
            db.GetCollection<InvoiceSetting>(collectionName)
                .InsertOne(invoiceSetting);
        }

        public static void Update(ObjectId id, InvoiceSetting invoiceSetting)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<InvoiceSetting>.Filter.Eq(x => x.Id, id);
            db.GetCollection<InvoiceSetting>(collectionName)
                .ReplaceOne(filter, invoiceSetting);
        }

        public static InvoiceSetting GetByStore(ObjectId storeId)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<InvoiceSetting>(collectionName)
                .Find(x => x.StoreId.Equals(storeId))
                .FirstOrDefault();
        }
    }
}
