﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class RegisterDAL
    {
        private static readonly string collectionName = "Register";

        public static RegisterModel GetOpenedRedisterByUser(ObjectId userId)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = Builders<RegisterModel>.Filter.Eq("UserId", userId)
                & Builders<RegisterModel>.Filter.Eq("CloseAt", BsonNull.Value);

            return db.GetCollection<RegisterModel>(collectionName)
                .Find(filter).FirstOrDefault();
        }

        public static void SaveRegister(RegisterModel register)
        {
            var db = ConnectionHelper.GetConnection();
            db.GetCollection<RegisterModel>(collectionName)
                .InsertOneAsync(register);
        }

        public async static void SaveRegisterAsync(RegisterModel register)
        {
            var db = ConnectionHelper.GetConnection();
            await db.GetCollection<RegisterModel>(collectionName)
                .InsertOneAsync(register);
        }

        public static Task<List<RegisterModel>> GetAllAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = ConstructFilters(query);

            return db.GetCollection<RegisterModel>(collectionName)
                .Find(filter)
                .SortByDescending(x => x.OpenAt)
                .Skip(query.Skip)
                .Limit(query.Limit)
                .ToListAsync();
        }

        public static Task<long> GetCountAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = ConstructFilters(query);

            Task<long> task = db.GetCollection<RegisterModel>(collectionName)
                .Find(filter)
                .CountDocumentsAsync();

            return task;
        }

        private static FilterDefinition<RegisterModel> ConstructFilters(QueryModel query)
        {
            var filter = Builders<RegisterModel>.Filter.Empty;

            if (query.Filters.ContainsKey("FromDate"))
            {
                filter &= Builders<RegisterModel>.Filter.Gte(x => x.OpenAt, query.Filters["FromDate"]);
            }

            if (query.Filters.ContainsKey("ToDate"))
            {
                filter &= Builders<RegisterModel>.Filter.Lte(x => x.OpenAt, query.Filters["ToDate"]);
            }

            if (query.Filters.ContainsKey("UserId"))
            {
                filter &= Builders<RegisterModel>.Filter.Eq(x => x.UserId, query.Filters["UserId"]);
            }

            return filter;
        }
    }
}
