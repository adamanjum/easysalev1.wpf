﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class UserDAL
    {
        private static readonly string collectionName = "User";

        public static UserModel AuthUser(string email, string password)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<UserModel>.Filter.Eq("Email", email)
                & Builders<UserModel>.Filter.Eq("Password", password);

            return db.GetCollection<UserModel>(collectionName)
                .Find(filter).FirstOrDefault();
        }

        public static Task<List<UserModel>> GetAllAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<UserModel>(collectionName)
                .Find(Builders<UserModel>.Filter.Empty)
                .ToListAsync();
        }
    }
}
