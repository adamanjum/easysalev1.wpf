﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class PurchaseDAL
    {
        private static readonly string collectionName = "Purchase";

        public static Task InsertAsync(PurchaseModel purchase)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<PurchaseModel>(collectionName)
                .InsertOneAsync(purchase);
        }

    }
}
