﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class CustomerDAL
    {
        private static readonly string collectionName = "Customer";

        public static Task<List<CustomerModel>> GetAsync()
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<CustomerModel>(collectionName)
                .Find(Builders<CustomerModel>.Filter.Empty)
                .ToListAsync();
        }
    }
}
