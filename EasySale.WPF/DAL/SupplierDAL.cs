﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class SupplierDAL
    {
        private static readonly string collectionName = "Supplier";

        public static Task<List<SupplierModel>> GetAsync()
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<SupplierModel>(collectionName)
                .Find(Builders<SupplierModel>.Filter.Empty)
                .ToListAsync();
        }
    }
}
