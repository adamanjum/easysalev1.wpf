﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class CategoryDAL
    {
        private static readonly string collectionName = "Category";

        public static Task Insert(CategoryModel category)
        {
            var db = ConnectionHelper.GetConnection();
            return db.GetCollection<CategoryModel>(collectionName)
                .InsertOneAsync(category);
        }

        public static Task<List<CategoryModel>> GetAsync()
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<CategoryModel>(collectionName)
                .Find(Builders<CategoryModel>.Filter.Empty)
                .ToListAsync();
        }

        public static Task<CategoryModel> GetByName(string name)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<CategoryModel>(collectionName)
                .FindSync(x => x.Name.ToLower() == name.ToLower())
                .FirstOrDefaultAsync();
        }

        public static Task Update(ObjectId id, CategoryModel category)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<CategoryModel>(collectionName)
                .ReplaceOneAsync(new BsonDocument("_id", id), category, new UpdateOptions { IsUpsert = false });
        }

        public static Task<List<CategoryModel>> GetAsync(string keyword, int? skip, int? limit)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<CategoryModel>.Filter.Empty;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                var search = Builders<CategoryModel>.Filter.Regex("Name", BsonRegularExpression.Create(new Regex(keyword, RegexOptions.IgnoreCase)));
                filter &= search;
            }

            return db.GetCollection<CategoryModel>(collectionName)
                .Find(filter)
                .SortByDescending(x => x.CreatedAt)
                .Skip(skip)
                .Limit(limit)
                .ToListAsync();
        }

        public static Task<List<CategoryModel>> GetAsync(ObjectId[] ids)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = Builders<CategoryModel>.Filter.In(x => x.Id, ids);

            return db.GetCollection<CategoryModel>(collectionName)
                .Find(filter)
                .ToListAsync();
        }
        public static Task<long> Count(string keyword)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<CategoryModel>.Filter.Empty;

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                var search = Builders<CategoryModel>.Filter.Regex("Name", BsonRegularExpression.Create(new Regex(keyword, RegexOptions.IgnoreCase)));
                filter &= search;
            }

            return db.GetCollection<CategoryModel>(collectionName)
                .Find(filter)
                .CountDocumentsAsync();
        }
    }
}
