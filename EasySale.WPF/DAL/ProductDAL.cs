﻿using EasySale.WPF.Comman;
using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class ProductDAL
    {
        private static readonly string collectionName = "Product";

        public static Task<List<ProductModel>> GetAllAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = ConstructFilters(query);

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .SortByDescending(x => x.CreatedAt)
                .Skip(query.Skip)
                .Limit(query.Limit)
                .ToListAsync();
        }

        public static Task<long> GetCountAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = ConstructFilters(query);

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .CountDocumentsAsync();
        }

        public static Task InsertBulk(List<ProductModel> products)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ProductModel>(collectionName)
                .InsertManyAsync(products);
        }

        public static Task DeleteOneAsync(ObjectId id)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ProductModel>(collectionName)
                .DeleteOneAsync(Builders<ProductModel>.Filter.Eq(x => x.Id, id));
        }

        public static Task<ProductModel> GetOneAsync(string name, string code)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ProductModel>.Filter.Empty;
            if (!string.IsNullOrWhiteSpace(name))
                filter &= Builders<ProductModel>.Filter.Where(x => x.Name.ToLower() == name.ToLower());

            if (!string.IsNullOrWhiteSpace(code))
                filter &= Builders<ProductModel>.Filter.Where(x => x.Code.ToLower() == code.ToLower());

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .FirstOrDefaultAsync();
        }

        public static Task<ProductModel> GetOne(string name, ObjectId? manufacturerId)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<ProductModel>.Filter.Empty;
            if (!string.IsNullOrWhiteSpace(name))
                filter &= Builders<ProductModel>.Filter.Where(x => x.Name.ToLower() == name.ToLower());

            if (manufacturerId.HasValue)
                filter &= Builders<ProductModel>.Filter.Where(x => x.ManufacturerId.Equals(manufacturerId));

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .FirstOrDefaultAsync();
        }

        public static Task<ProductModel> getAsync(Dictionary<string, object> query)
        {
            var db = ConnectionHelper.GetConnection();
            BsonDocument filter = new BsonDocument();

            if (query.Count > 1)
            {
                filter.AddRange(query);
            }

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .FirstOrDefaultAsync();
        }

        public static Task<UpdateResult> UpdateByIdAsync(ObjectId id, UpdateDefinition<ProductModel> update)
        {
            var db = ConnectionHelper.GetConnection();
            var filter = Builders<ProductModel>.Filter.Eq(x => x.Id, id);

            return db.GetCollection<ProductModel>(collectionName)
                .UpdateOneAsync(filter, update);
        }

        public static Task BulkUpdateAsync(List<WriteModel<ProductModel>> models)
        {
            var db = ConnectionHelper.GetConnection();

            return db.GetCollection<ProductModel>(collectionName)
                .BulkWriteAsync(models);
        }

        public static Task<List<ProductModel>> SearchAsync(string text, int limit = 30)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = new BsonDocument();

            if (!string.IsNullOrEmpty(text))
            {
                string search = Regex.Escape(text);

                filter.Add("Name", new BsonDocument
                {
                    {
                        "$regex",
                        BsonRegularExpression.Create(new Regex(search.Insert(0, "^").Replace("\\ ", ".+"), RegexOptions.IgnoreCase))
                    }
                });
            }

            return db.GetCollection<ProductModel>(collectionName)
                .Find(filter)
                .Limit(limit)
                .ToListAsync();
        }

        private static FilterDefinition<ProductModel> ConstructFilters(QueryModel query)
        {
            var builder = Builders<ProductModel>.Filter;

            var filter = builder.Empty;

            if (query.Filters.ContainsKey("CategoryId"))
                filter &= builder.Eq(x => x.CategoryId, query.Filters["CategoryId"]);

            if (query.Filters.ContainsKey("CategoryIds"))
                filter &= builder.In(x => x.CategoryId, query.Filters["CategoryIds"] as List<ObjectId>);

            if (query.Filters.ContainsKey("ManufacturerIds"))
                filter &= builder.In("ManufacturerId", query.Filters["ManufacturerIds"] as List<ObjectId>);

            if (query.Filters.ContainsKey("ManufacturerId"))
                filter &= builder.Eq(x => x.ManufacturerId, query.Filters["ManufacturerId"]);

            if (query.Filters.ContainsKey("Search"))
            {
                var search = builder.Regex(x => x.Name, (string)query.Filters["Search"]);
                filter &= search;
            }

            if (query.Filters.ContainsKey("IsLowStock"))
            {
                filter &= builder.Or(new FilterDefinition<ProductModel>[]
                {
                    builder.Eq(x => x.Stock, null),
                    builder.Eq(x => x.Stock.Qty, 0),
                    new BsonDocument("$expr",
                    new BsonDocument("$lte", new BsonArray { "$Stock.Qty", "$MinQty" })),
                });
            }

            if (query.Filters.ContainsKey("StockLevels"))
            {
                StockLevels level = (StockLevels)query.Filters["StockLevels"];
                if (level == StockLevels.Zero)
                {
                    filter &= builder.Or(new FilterDefinition<ProductModel>[]
                    {
                        builder.Eq(x => x.Stock, null),
                        builder.Eq(x => x.Stock.Qty, 0)
                    });
                }

                if (level == StockLevels.LowStock)
                {
                    filter &= builder.And(new FilterDefinition<ProductModel>[]
                    {
                        builder.Ne(x => x.Stock, null),
                        new BsonDocument("$expr",
                        new BsonDocument("$lte", new BsonArray { "$Stock.Qty", "$MinQty" }))
                    });
                }

                if (level == StockLevels.Surplus)
                {
                    filter &= builder.Ne(x => x.MaxQty, null);
                    filter &= new BsonDocument("$expr",
                        new BsonDocument("$gt", new BsonArray { "$Stock.Qty", "$MaxQty" }));
                }

                if (level == StockLevels.BetweenMinMaxThresholsd)
                {
                    //filter &= builder.And(new FilterDefinition<ProductModel>[]
                    //{
                    //    builder.Ne(x => x.MinQty, null),
                    //    new BsonDocument("$expr",
                    //    new BsonDocument("$gt", new BsonArray { "$Stock.Qty", "$MinQty" })),

                    //    builder.Ne(x => x.MaxQty, null),
                    //    new BsonDocument("$expr",
                    //    new BsonDocument("$lt", new BsonArray { "$Stock.Qty", "$MaxQty" }))
                    //});
                }
            }

            if (query.Filters.ContainsKey("Ids"))
            {
                filter &= builder.In(x => x.Id, (List<ObjectId>)query.Filters["Ids"]);
            }

            return filter;
        }
    }
}
