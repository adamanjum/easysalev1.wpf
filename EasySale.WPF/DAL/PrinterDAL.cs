﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public class PrinterDAL
    {
        private static readonly string collectionName = "Printer";

        public static PrinterModel GetPrinterSetting()
        {
            var db = ConnectionHelper.GetConnection();
            var filter = Builders<PrinterModel>.Filter.Empty;

            return db.GetCollection<PrinterModel>(collectionName)
                .Find(filter).FirstOrDefault();
        }
    }
}
