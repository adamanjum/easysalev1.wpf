﻿using EasySale.WPF.Helper;
using EasySale.WPF.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasySale.WPF.DAL
{
    public static class SaleDAL
    {
        private static readonly string collectionName = "Sale";

        public static List<CartModel> GetSales(int? status, DateTime? to, DateTime? from, ObjectId? userId)
        {
            var db = ConnectionHelper.GetConnection();

            var filter = Builders<CartModel>.Filter.Empty;

            if (status.HasValue)
                filter &= Builders<CartModel>.Filter.Eq("Status", status);

            if (from.HasValue)
                filter &= Builders<CartModel>.Filter.Gte("Date", from.Value);

            if (to.HasValue)
                filter &= Builders<CartModel>.Filter.Lte("Date", to.Value);

            if (userId.HasValue)
                filter &= Builders<CartModel>.Filter.Eq("UserId", userId);

            return db.GetCollection<CartModel>(collectionName)
                .Find(filter)
                .ToList();
        }

        public static Task<List<ProductReportModel>> GetProductReportsAsync(QueryModel query)
        {
            IMongoDatabase db = ConnectionHelper.GetConnection();
            BsonDocument filterMatch = GetProductReportFilter(query.Filters);
            BsonDocument dateRange = getProductReportDateRageFilter(query.Filters);

            List<BsonDocument> stages = new List<BsonDocument>
            {
                /// @match
                new BsonDocument("$match", dateRange),
                /// @unwind
                new BsonDocument("$unwind",
                new BsonDocument
                {
                    { "path", "$CartItems" },
                    { "preserveNullAndEmptyArrays", true }
                }),
                /// @project
                new BsonDocument("$project",
                new BsonDocument
                {
                    { "_id", "$CartItems.ProductId" },
                    { "ProductName", "$CartItems.Name" },
                    { "Qty", "$CartItems.Qty" },
                    { "Price", "$CartItems.Price" },
                    {
                        "total",
                        new BsonDocument("$multiply",
                        new BsonArray
                        {
                            "$CartItems.Price",
                            "$CartItems.Qty"
                        })
                    }
                }),
                /// @group
                new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", "$_id" },
                    {
                        "ProductName",
                        new BsonDocument("$first", "$ProductName")
                    },
                    {
                        "QtySold",
                        new BsonDocument("$sum", "$Qty")
                    },
                    {
                        "TotalPrice",
                        new BsonDocument("$sum", "$total")
                    }
                }),
                /// @lookup product
                new BsonDocument("$lookup",
                new BsonDocument
                {
                    { "from", "Product" },
                    { "localField", "_id" },
                    { "foreignField", "_id" },
                    { "as", "Product" }
                }),
                /// @unwind
                new BsonDocument("$unwind",
                new BsonDocument("path", "$Product")),
                /// @match
                new BsonDocument("$match", filterMatch)
            };

            if (query.Filters.ContainsKey("apply-pagination") && (bool)query.Filters["apply-pagination"])
            {
                stages.Add(new BsonDocument("$skip", query.Skip));
                stages.Add(new BsonDocument("$limit", query.Limit));
            }

            stages = stages.Concat(new List<BsonDocument>{
                new BsonDocument("$lookup",
                new BsonDocument
                {
                    { "from", "Category" },
                    { "localField", "Product.CategoryId" },
                    { "foreignField", "_id" },
                    { "as", "Category" }
                }),
                /// @unwind
                new BsonDocument("$unwind",
                new BsonDocument("path", "$Category")),
                /// @lookup manufacturer
                new BsonDocument("$lookup",
                new BsonDocument
                {
                    { "from", "Manufacturer" },
                    { "localField", "Product.ManufacturerId" },
                    { "foreignField", "_id" },
                    { "as", "Manufacturer" }
                }),
                /// @unwind
                new BsonDocument("$unwind",
                new BsonDocument
                {
                    { "path", "$Manufacturer" },
                    { "preserveNullAndEmptyArrays", true }
                }),
                /// @group
                new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", "$_id" },
                    {
                        "Code",
                        new BsonDocument("$first", "$Product.Code")
                    },
                    {
                        "ProductName",
                        new BsonDocument("$first", "$ProductName")
                    },
                    {
                        "QtySold",
                        new BsonDocument("$first", "$QtySold")
                    },
                    {
                        "TotalPrice",
                        new BsonDocument("$first", "$TotalPrice")
                    },
                    {
                        "RemainingStock",
                        new BsonDocument("$first", "$Product.Stock.Qty")
                    },
                    {
                        "Category",
                        new BsonDocument("$first", "$Category.Name")
                    },
                    {
                        "Manufacturer",
                        new BsonDocument("$first", "$Manufacturer.Name")
                    }
                })
            }).ToList();

            PipelineDefinition<BsonDocument, ProductReportModel> pipeline = stages.ToArray();

            //PipelineDefinition<BsonDocument, ProductReportModel> pipeline = new BsonDocument[]
            //{
            //    /// @match
            //    new BsonDocument("$match", dateRange),
            //    /// @unwind
            //    new BsonDocument("$unwind",
            //    new BsonDocument
            //    {
            //        { "path", "$CartItems" },
            //        { "preserveNullAndEmptyArrays", true }
            //    }),
            //    /// @project
            //    new BsonDocument("$project",
            //    new BsonDocument
            //    {
            //        { "_id", "$CartItems.ProductId" },
            //        { "ProductName", "$CartItems.Name" },
            //        { "Qty", "$CartItems.Qty" },
            //        { "Price", "$CartItems.Price" },
            //        {
            //            "total",
            //            new BsonDocument("$multiply",
            //            new BsonArray
            //            {
            //                "$CartItems.Price",
            //                "$CartItems.Qty"
            //            })
            //        }
            //    }),
            //    /// @group
            //    new BsonDocument("$group",
            //    new BsonDocument
            //    {
            //        { "_id", "$_id" },
            //        {
            //            "ProductName",
            //            new BsonDocument("$first", "$ProductName")
            //        },
            //        {
            //            "QtySold",
            //            new BsonDocument("$sum", "$Qty")
            //        },
            //        {
            //            "TotalPrice",
            //            new BsonDocument("$sum", "$total")
            //        }
            //    }),
            //    /// @lookup product
            //    new BsonDocument("$lookup",
            //    new BsonDocument
            //    {
            //        { "from", "Product" },
            //        { "localField", "_id" },
            //        { "foreignField", "_id" },
            //        { "as", "Product" }
            //    }),
            //    /// @unwind
            //    new BsonDocument("$unwind",
            //    new BsonDocument("path", "$Product")),
            //    /// @match
            //    new BsonDocument("$match", filterMatch),
            //    /// @skip
            //    new BsonDocument("$skip", query.Skip),
            //    /// @limit
            //    new BsonDocument("$limit", query.Limit),
            //    /// @ lookup category
            //    new BsonDocument("$lookup",
            //    new BsonDocument
            //    {
            //        { "from", "Category" },
            //        { "localField", "Product.CategoryId" },
            //        { "foreignField", "_id" },
            //        { "as", "Category" }
            //    }),
            //    /// @unwind
            //    new BsonDocument("$unwind",
            //    new BsonDocument("path", "$Category")),
            //    /// @lookup manufacturer
            //    new BsonDocument("$lookup",
            //    new BsonDocument
            //    {
            //        { "from", "Manufacturer" },
            //        { "localField", "Product.ManufacturerId" },
            //        { "foreignField", "_id" },
            //        { "as", "Manufacturer" }
            //    }),
            //    /// @unwind
            //    new BsonDocument("$unwind",
            //    new BsonDocument
            //    {
            //        { "path", "$Manufacturer" },
            //        { "preserveNullAndEmptyArrays", true }
            //    }),
            //    /// @group
            //    new BsonDocument("$group",
            //    new BsonDocument
            //    {
            //        { "_id", "$_id" },
            //        {
            //            "Code",
            //            new BsonDocument("$first", "$Product.Code")
            //        },
            //        {
            //            "ProductName",
            //            new BsonDocument("$first", "$ProductName")
            //        },
            //        {
            //            "QtySold",
            //            new BsonDocument("$first", "$QtySold")
            //        },
            //        {
            //            "TotalPrice",
            //            new BsonDocument("$first", "$TotalPrice")
            //        },
            //        {
            //            "RemainingStock",
            //            new BsonDocument("$first", "$Product.Stock.Qty")
            //        },
            //        {
            //            "Category",
            //            new BsonDocument("$first", "$Category.Name")
            //        },
            //        {
            //            "Manufacturer",
            //            new BsonDocument("$first", "$Manufacturer.Name")
            //        }
            //    })
            //};



            Task<List<ProductReportModel>> productReports = db.GetCollection<BsonDocument>(collectionName)
                .Aggregate(pipeline).ToListAsync();

            return productReports;
        }

        public static Task<CountModel> GetProductReportsCountAsync(QueryModel query)
        {
            var db = ConnectionHelper.GetConnection();
            var filterMatch = GetProductReportFilter(query.Filters);
            var dateRange = getProductReportDateRageFilter(query.Filters);

            PipelineDefinition<BsonDocument, CountModel> pipeline = new BsonDocument[]
            {
                /// @match
                new BsonDocument("$match", dateRange),
                new BsonDocument("$unwind",
                new BsonDocument
                {
                    { "path", "$CartItems" },
                    { "preserveNullAndEmptyArrays", true }
                }),
                new BsonDocument("$group",
                new BsonDocument("_id", "$CartItems.ProductId")),
                 /// @lookup product
                new BsonDocument("$lookup",
                new BsonDocument
                {
                    { "from", "Product" },
                    { "localField", "_id" },
                    { "foreignField", "_id" },
                    { "as", "Product" }
                }),
                /// @unwind
                new BsonDocument("$unwind",
                new BsonDocument("path", "$Product")),
                /// @match
                new BsonDocument("$match", filterMatch),
                new BsonDocument("$count", "Total")
            };

            Task<CountModel> count = db.GetCollection<BsonDocument>(collectionName)
                .Aggregate(pipeline).FirstOrDefaultAsync();

            return count;
        }


        /// @helper functions
        private static BsonDocument GetProductReportFilter(Dictionary<string, object> filters)
        {
            var filterMatch = new BsonDocument();

            if (filters.ContainsKey("CategoryId"))
                filterMatch.Add("Product.CategoryId", (ObjectId)filters["CategoryId"]);

            if (filters.ContainsKey("ManufacturerId"))
                filterMatch.Add("Product.ManufacturerId", (ObjectId)filters["ManufacturerId"]);

            if (filters.ContainsKey("SearchText"))
                filterMatch.Add("Product.Name", new BsonDocument
                {
                    {
                        "$regex",
                        BsonRegularExpression.Create(new Regex(((string)filters["SearchText"]).Insert(0, "^").Replace(" ", ".+"), RegexOptions.IgnoreCase))
                    }
                });


            Console.Write(filterMatch);

            return filterMatch;
        }

        private static BsonDocument getProductReportDateRageFilter(Dictionary<string, object> filters)
        {
            BsonDocument dateRange = new BsonDocument();
            BsonDocument startDate = new BsonDocument();
            BsonDocument endDate = new BsonDocument();

            BsonArray arr = new BsonArray();

            if (filters.ContainsKey("StartDate"))
            {
                startDate = new BsonDocument
                {
                    {
                        "Date",
                        new BsonDocument("$gte", ((DateTime)filters["StartDate"]).AddHours(5))
                    }
                };
            }

            if (filters.ContainsKey("EndDate"))
            {
                endDate = new BsonDocument
                {
                    {
                        "Date",
                        new BsonDocument("$lte", ((DateTime)filters["EndDate"]).AddHours(23).AddHours(5).AddMinutes(59).AddSeconds(59))
                    }
                };
            }

            if (startDate.Contains("Date"))
                arr.Add(startDate);

            if (endDate.Contains("Date"))
                arr.Add(endDate);

            if (arr.Count > 0)
                dateRange.Add("$and", arr);

            return dateRange;
        }
    }
}
